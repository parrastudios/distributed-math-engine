/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Debug.hpp>
#include <Exception.hpp>
#include <iostream> // Stream.hpp

using namespace std;

/** Custom exception handler class
 */
class CustomExceptionHandler : public ExceptionHandler {
  public:
	CustomExceptionHandler() {

	}

	~CustomExceptionHandler() {

	}

	virtual void process(const Exception& exception) {
		if (exception.get_code() == 2) {
			// do some task to solve exception 2

		} else if (exception.get_code() == 1) {
			// ..
		}

		cout << "Exception proccessed: " << exception << endl;
	}
};

/** 
 * Exception Test
 */

int main(int argc, char* argv[]) {

	CustomExceptionHandler handler;
	Exception ex("hola");

	ex.register_handler(handler);

	ex.raise();

	raise_exception(Exception("excep2", handler, 3, 4));
	
	// this should stop the data flow
	raise_critical_exception(Exception("critical_exception1"));
	
	return 0;
}
