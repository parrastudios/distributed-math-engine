/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Debug.hpp>
#include <Sleep.hpp>
#include <Time.hpp>

/** 
 * Time Test
 */

int main(int argc, char* argv[]) {
	Time time;
	uint64 r = 0;

	time.start();

	sleep(100);

	time.stop();

	sleep(100);

	r = time.restart();

	sleep(100);

	time.stop();

	sleep(100);

	time.resume();

	sleep(100);

	time.stop();

	//					near to 200,		near to 200,						undeterminated
	cout << "elapsed: " << r << " current: " << time.get() << " absolute: " << Time::get_absolute() << endl;

    return 0;
}
