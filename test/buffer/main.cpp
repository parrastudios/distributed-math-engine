/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Debug.hpp>
#include <Buffer.hpp>

using namespace std;

/**
 * Member Data
 */
typedef struct {
	uint32	a;	//< 4 bytes
	uint8	b;	//< 1 byte
	float32	c;	//< 4 bytes
	float64	d;	//< 8 bytes => 17 bytes
} TestData;

/** 
 * Buffer Test
 */

int main(int argc, char* argv[]) {
    TestData t = { 144450, 34, 5.8f, 0.0030432 };
	TestData t2 = {{0}};
    Packet packet;
    Buffer buffer;

    packet.set_type(13);
    packet.set_code(33);
    packet.append(&t, sizeof(TestData));

    packet.buffer_pack(buffer);

    cout << "C: " << endl;

    for (uint32 i = 0; i < buffer.get_size(); ++i) {
        cout << buffer.get_data()[i] << endl;
    }

	packet.buffer_unpack(buffer, &t2, sizeof(TestData));

	cout << t2.a << endl;
	cout << t2.b << endl;
	cout << t2.c << endl;
	cout << t2.d << endl;

    return 0;
}
