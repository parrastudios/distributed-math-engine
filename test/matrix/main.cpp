/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Debug.hpp>
#include <Matrix.hpp>

using namespace std;

/** 
 * Matrix Test
 */

int main(int argc, char* argv[]) {
    Matrixd A(3, 2, Matrixd::TYPE_RANDOM);
    Matrixd B(2, 4, Matrixd::TYPE_RANDOM);
    Matrixd C(4, 4, Matrixd::TYPE_RANDOM);
    Matrixd D(4, 4, Matrixd::TYPE_RANDOM);
    Matrixd I(4, 4, Matrixd::TYPE_IDENTITY);
    Matrixd M(20, 20, Matrixd::TYPE_RANDOM);

    cout << "Generando algunas matrices aleatorias:" << endl
         << "A(" << A.get_rows() << "x" << A.get_cols() << "):" << endl << A << endl
         << "B(" << B.get_rows() << "x" << B.get_cols() << "):" << endl << B << endl
         << "C(" << C.get_rows() << "x" << C.get_cols() << "):" << endl << C << endl
         << "D(" << D.get_rows() << "x" << D.get_cols() << "):" << endl << D << endl
         << "I(" << I.get_rows() << "x" << I.get_cols() << "):" << endl << I << endl;

    cout << "Operando con ellas:" << endl
         << "A*B:" << endl << A * B << endl
         << "3*B" << endl << 3.0 * B << endl
         << "C+D" << endl <<C + D << endl
         << "D-C" << endl << D - C << endl
         << "C/D -> C*inv(D):" << endl << C / D << endl;

    cout << "Aplicandole algunos metodos:" << endl
         << "minor(C, 3, 2):" << endl << C.minor_mat(2, 2) << endl
         << "transpose(A)" << endl << A.transpose() << endl
         << "inv(C):" << endl << C.invert() << endl
         << "lower(D):" << endl << D.lower() << endl
         << "upper(inv(C)):" << endl << C.invert().upper() << endl
         << "det(D): " << C.determinant() << endl;

    cout << "Y ahora, un test para probar la eficiencia del programa." << endl
         << "Calculo de inversa de una matriz 600x600" << endl;

	Matrixd l = M.invert();

    cout << l << endl;

	cout << endl << endl << M * l << endl;

    return 0;
}
