![](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Dec/23/distribuited-math-engine-logo-883910255-9_avatar.png?raw=true "Distributed Math Engine")
### __Distributed Math Engine__
__A new approach to improve the efficiency in mathematical calculus through its distribution on network__



- - -


__LICENSE__

Copyright (C) 2012 - 2013
Vicente Eduardo Ferrer Garcia (<vic798@gmail.com>)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>


- - -


__ACKNOWLEDGMENTS__

SFML (<http://www.sfml-dev.org/>) : By socket and thread support.

Boost (<http://www.boost.org/>) : By noncopyable pattern.


- - -

