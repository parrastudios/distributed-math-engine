/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Address.hpp>
#include <SocketImpl.hpp>

/**
 * Class definitions
 */
const Address Address::ADDR_NONE;
const Address Address::ADDR_LOCALHOST(127, 0, 0, 1);
const Address Address::ADDR_BROADCAST(255, 255, 255, 255);

/**
 * Methods
 */

namespace priv {

uint32 resolve(const String& address) {
    if (address == "255.255.255.255") {
        return INADDR_BROADCAST;
    } else {
        uint32 ip_addr = inet_addr(*address);
        if (ip_addr != INADDR_NONE)
            return ip_addr;

        // not a valid address, try to convert it as a host name
        hostent* host = gethostbyname(*address);
        if (host)
            return reinterpret_cast<in_addr*>(host->h_addr)->s_addr;

        // not a valid address nor a host name
        return 0;
    }
}

} // priv

Address::Address() : address_(0) {

}

Address::Address(const String& address) : address_(0) {
	address_ = priv::resolve(address);
}

Address::Address(const char* address) : address_(0) {
	address_ = priv::resolve(String(address));
}

Address::Address(uint8 byte0, uint8 byte1, uint8 byte2, uint8 byte3) :
         address_(htonl((byte0 << 24) | (byte1 << 16) | (byte2 << 8) | byte3)) {

}

Address::Address(uint32 address) : address_(htonl(address)) {

}

Address::Address(const Address& address) : address_(address.address_) {

}

String Address::to_string() const {
    in_addr address;
    address.s_addr = address_;

    return String(inet_ntoa(address));
}

uint32 Address::to_uint32() const {
    return ntohl(address_);
}

Address Address::get_address_local() {
    // the method here is to connect a UDP socket to anyone (here to localhost),
    // and get the local socket address with the getsockname function
    // UDP connection will not send anything to the network, so this function won't cause any overhead

    Address local_addr = ADDR_NONE;

    // create the socket
    Socket::Handle sock = socket(PF_INET, SOCK_DGRAM, 0);
    if (sock == priv::SocketImpl::invalid())
        return local_addr;

    // connect the socket to localhost on any port
    sockaddr_in address = priv::SocketImpl::create_addr(ntohl(INADDR_LOOPBACK), 0);
    if (connect(sock, reinterpret_cast<sockaddr*>(&address), sizeof(address)) == -1) {
        priv::SocketImpl::close(sock);
        return local_addr;
    }

    // get the local address of the socket connection
    priv::SocketImpl::addr_size_t size = sizeof(address);
    if (getsockname(sock, reinterpret_cast<sockaddr*>(&address), &size) == -1) {
        priv::SocketImpl::close(sock);
        return local_addr;
    }

    // Close the socket
    priv::SocketImpl::close(sock);

    // Finally build the IP address
    local_addr = Address(ntohl(address.sin_addr.s_addr));

    return local_addr;
}

bool operator==(const Address& left, const Address& right) {
    return left.to_uint32() == right.to_uint32();
}

bool operator!=(const Address& left, const Address& right) {
    return !(left == right);
}
