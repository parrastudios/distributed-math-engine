/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Server.hpp>

/**
 * Methods
 */

Server::Server(uint16 port) : port_(port), listener_(), listen_thread_(null), user_list_() {
	listen_thread_ = new Thread(&Server::listen_thread, this);
}

Server::~Server() {
	// close listen connection
	listener_.disconnect();

	// close all conections
	while (!user_list_.empty()) {
		delete user_list_.pop_back();
	}

	// detele the listen thread
	delete listen_thread_;
}

void Server::run() {
	// run listener thread
	listen_thread_->launch();

	// ...

	// wait until listener finishes
	listen_thread_->wait();
}

void Server::listen_thread() {
	// enable listener
	if (listener_.listen(port_) == Socket::STATUS_DONE) {
		User* new_user = null;

		do {
			if (!new_user) {
				// initialize the user
				new_user = new User(user_list_);
			} else {
				// new incoming user
				user_list_.push_back(new_user);

				// run the thread
				new_user->thread_->launch();

				// wait for other user
				new_user = new User(user_list_);
			}
		} while (listener_.accept(new_user->socket_) == Socket::STATUS_DONE);

		// remove the last unused user
		if (new_user) {
			delete new_user;
		}
	}
}
