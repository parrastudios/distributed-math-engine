/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Console.hpp>
#include <String.hpp>
#include <iostream> // Stream.hpp

/**
 * Methods
 */

Console::Console(std::istream& input, std::ostream& output) : is_open_(true), current_command_(CMD_ERROR),
															  input_stream_(input), output_stream_(output) {
	
}

Console::~Console() {
	close();
}

bool Console::is_open() {
	return is_open_;
}

uint32 Console::read_command() {
	String cmd_str;

	// check package
	input_stream_ >> cmd_str;
	
	if (cmd_str == "mat") {
		input_stream_ >> cmd_str;

		if (cmd_str == "inv") {
			return CMD_MATRIX_INVERSE;
		} else if (cmd_str == "triup") {
			return CMD_MATRIX_TRIANGULATEUP;
		}
	} else if (cmd_str == "exit") {
		return CMD_EXIT;
	}
	
	return CMD_ERROR;
}

void Console::close() {
	is_open_ = false;
}

