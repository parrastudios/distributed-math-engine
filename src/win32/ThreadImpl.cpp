/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Win32/ThreadImpl.hpp>
#include <Thread.hpp>
#include <process.h>

namespace priv {

ThreadImpl::ThreadImpl(Thread* owner) : handle_(null), id_(0) {
    handle_ = reinterpret_cast<HANDLE>(_beginthreadex(null, 0, &ThreadImpl::function, owner, 0, &id_));

    if (!handle_) {
        // error
    }
}

ThreadImpl::~ThreadImpl() {
    if (handle_)
        CloseHandle(handle_);
}

void ThreadImpl::wait() {
    if (handle_) {
        if (id_ != GetCurrentThreadId()) {
            WaitForSingleObject(handle_, INFINITE);
        }
    }
}

void ThreadImpl::terminate() {
    if (handle_) {
        TerminateThread(handle_, 0);
    }
}

unsigned int __stdcall ThreadImpl::function(void* data) {
    Thread* owner = static_cast<Thread*>(data);

    owner->run();

    _endthreadex(0);

    return 0;
}

}
