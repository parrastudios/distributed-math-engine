/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Win32/SocketImpl.hpp>

namespace priv {

/**
 * Class definitions
 */
bool Initializer::initialized_ = false;

/**
 * Methods
 */

Initializer::Initializer() {
	if (initialized_ == false) {
		WSADATA wsdata;
		int32 error;

		error = WSAStartup(MAKEWORD(2, 2), &wsdata);

		if (error || wsdata.wVersion != 0x0202) {
			WSACleanup();
			initialized_ = false;
		} else {
			initialized_ = true;
		}
	}
}

Initializer::~Initializer() {
	if (initialized_ == true) {
		WSACleanup();
		initialized_ = false;
	}
}

sockaddr_in SocketImpl::create_addr(uint32 address, uint16 port) {
    sockaddr_in addr;
    std::memset(addr.sin_zero, 0, sizeof(addr.sin_zero));
	addr.sin_addr.s_addr = htonl(address);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    return addr;
}

Socket::Handle SocketImpl::invalid() {
    return INVALID_SOCKET;
}

void SocketImpl::close(Socket::Handle handle) {
    closesocket(handle);
}

void SocketImpl::set_blocking(Socket::Handle handle, bool block) {
    u_long blocking = block ? 0 : 1;
    ioctlsocket(handle, FIONBIO, &blocking);
}

Socket::Status SocketImpl::get_error() {
	int32 last_error = WSAGetLastError();

    switch (last_error) {
        case WSAEWOULDBLOCK  : return Socket::STATUS_NOTREADY;
        case WSAECONNABORTED : return Socket::STATUS_DISCONNECTED;
        case WSAECONNRESET   : return Socket::STATUS_DISCONNECTED;
        case WSAETIMEDOUT    : return Socket::STATUS_DISCONNECTED;
        case WSAENETRESET    : return Socket::STATUS_DISCONNECTED;
        case WSAENOTCONN     : return Socket::STATUS_DISCONNECTED;
        default              : return Socket::STATUS_ERROR;
    }
}

} // priv
