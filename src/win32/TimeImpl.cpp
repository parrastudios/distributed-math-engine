/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Platform.hpp>
#include <Win32/TimeImpl.hpp>

namespace priv {

/**
 * Class definitions
 */
LARGE_INTEGER TimeImpl::frequency_ = { 0 };
BOOL TimeImpl::use_high_performance_timer_ = FALSE;

/**
 * Methods
 */

TimeImpl::TimeImpl() {
	HANDLE cur_thread = ::GetCurrentThread();
	DWORD_PTR prev_mask = ::SetThreadAffinityMask(cur_thread, 1);

	use_high_performance_timer_ = ::QueryPerformanceFrequency(&frequency_);
	
	// restore thread affinity
	::SetThreadAffinityMask(cur_thread, prev_mask);
}

TimeImpl::~TimeImpl() {

}

uint64 TimeImpl::get_absolute() {
	// http://msdn.microsoft.com/en-us/library/windows/desktop/ms644904(v=vs.85).aspx
	HANDLE cur_thread = ::GetCurrentThread();
	DWORD_PTR prev_mask = ::SetThreadAffinityMask(cur_thread, 1);

	if (use_high_performance_timer_) {
		LARGE_INTEGER cur_time;
    
		// get current time
		::QueryPerformanceCounter(&cur_time);
		
		// restore thread affinity
		::SetThreadAffinityMask(cur_thread, prev_mask);

		// return time in nanoseconds
		return static_cast<uint64>(cur_time.QuadPart * 1000000000 / frequency_.QuadPart);
	} else {
        // high performance counter not available (use GetTickCount less accurate)
#		if ARCH_FAMILY == ARCHITECTURE_64
			uint64 nanoseconds = static_cast<uint64>(::GetTickCount64() * 1000000);
#		else
			uint64 nanoseconds = static_cast<uint64>(::GetTickCount() * 1000000);
#		endif
		// restore thread affinity
		::SetThreadAffinityMask(cur_thread, prev_mask);

		return nanoseconds;
	}
}

} // priv
