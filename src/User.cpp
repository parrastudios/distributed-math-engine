/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <User.hpp>
#include <Lock.hpp>

/**
 * Methods
 */

User::User(List<User*>& list) : socket_(), available_(true), thread_(null), mutex_(),
                                protocol_(static_cast<Socket&>(socket_)), matrix_manager_(),
                                user_list_(user_list_), user_request_(null), pending_users_(0) {
	thread_ = new Thread(&User::run, this);
}

User::~User() {
	// close socket
	socket_.disconnect();

	// wait until thread finishes
	thread_->wait();

	delete thread_;
}

bool User::is_available() {
	return available_;
}

void User::run() {
	// main user thread (only handles incoming data by now)
	protocol_.handle_incoming_data(*this);
}


void User::incoming_request(Packet& packet) {
	List<User*> available_users = get_available_users();

	if (available_users.size() == 0) {
		return;
	}

	// handle incoming requests
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					// insert into matrix manager
					uint32 block_id = matrix_manager_.create(packet);
					uint32 block_offset = 0;

					// get the block
					Vector<uint32> block_vector = matrix_manager_.slice(block_id, available_users.size());

					// update pending users size
					pending_users_ = block_vector.size();

					if (pending_users_ > 0) {
						List<User*>::Iterator iterator = available_users.begin();

						do {
							// push data into manager
							(*iterator)->matrix_manager_.add(matrix_manager_.get_element(block_vector[block_offset]));

							// save pointer to the requesting user
							(*iterator)->user_request_ = this;

							// send request to client
							(*iterator)->protocol_.handle_outgoing_data(*(*iterator), PROTOCOL_TYPE_MATRIX, PROTOCOL_CODE_INVERSE);

							++iterator;
							++block_offset;
						} while (block_offset < pending_users_);
					}

					// clear current manager data
					matrix_manager_.clear();

					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default :
					return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default :
					return;
			}

			break;
		}

		default :
			return;
	}
}

void User::incoming_result(Packet& packet) {
	// handle incoming requests
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					// create matrix block
					uint32 id = matrix_manager_.create(packet);

					// lock user request mutex
					if (user_request_ != null) {
						Lock lock(user_request_->mutex_);

						// append data to user request
						user_request_->matrix_manager_.add(matrix_manager_.get_element(id));

						// decrease a pending user reference
						--user_request_->pending_users_;

						// send result to the original client (there is no data still being calculated)
						if (user_request_->pending_users_ == 0) {
							user_request_->protocol_.handle_outgoing_data(*user_request_, PROTOCOL_TYPE_MATRIX, PROTOCOL_CODE_INVERSE);
						}

						// set user to null
						user_request_ = null;
					}

					// remove matrix block
					// todo: memory leak here
					// matrix_manager_.remove(id);

					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default :
					return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default :
					return;
			}

			break;
		}

		default :
			return;
	}
}

void User::outgoing_request(Packet& packet) {
	// handle incoming requests
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					MatrixfBlock* block = matrix_manager_.get_element();

					if (block) {
						block->to_packet(packet);
					} else {
						packet.clear();
					}
					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default :
					return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default :
					return;
			}

			break;
		}

		default :
			return;
	}
}

void User::outgoing_result(Packet& packet) {
	// handle incoming requests
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					MatrixfBlock* block = matrix_manager_.get_element();

					if (block) {
						block->to_packet(packet);
					} else {
						packet.clear();
					}
					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default :
					return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default :
					return;
			}

			break;
		}

		default :
			return;
	}
}

void User::incoming_data(Packet& packet) {
	bool available = available_;

	available_ = !available_;

	if (available) {
		// problem request from a client
		incoming_request(packet);
	} else {
		// partial result form a client
		incoming_result(packet);
	}
}

void User::outgoing_data(Packet& packet) {
	if (available_) {
		// send a partial request to a client
		outgoing_request(packet);
	} else {
		// send a final result to a client
		outgoing_result(packet);
	}

	available_ = !available_;
}

List<User*> User::get_available_users() {
	if (user_list_.size() > 0) {
		List<User*> list(user_list_);

		// search and find available users
		for (List<User*>::Iterator iterator = list.begin();
			 iterator != list.end() && *iterator != null;
			 ++iterator) {

			if ((*iterator)->available_ == false) {
				list.erase(iterator);
			}
		}

		if (list.back()->available_ == false) {
			list.pop_back();
		}

		return list;
	}

	return List<User*>();
}
