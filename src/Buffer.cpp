/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Buffer.hpp>

/**
 * Methods
 */

Buffer::Buffer() : data_(null), size_(0) {

}

Buffer::Buffer(uint32 size) : data_(null), size_(0) {
    create(size);
}

Buffer::Buffer(const Buffer& buffer) : data_(null), size_(0) {
    create(buffer.size_);
    set_data(buffer.data_, buffer.size_, 0);
}

Buffer::~Buffer() {
    clear();
}

Buffer& Buffer::operator=(const Buffer& buffer) {
	if (&buffer != this) {
	    create(buffer.size_);
		set_data(buffer.data_, buffer.size_, 0);
	}
	
	return *this;
}

void Buffer::set_data(void* data, uint32 size, uint32 offset) {
    uint8* buffer = static_cast<uint8*>(data);

    for (uint32 i = 0; i < size; ++i) {
        data_[i + offset] = buffer[i];
    }
}

uint8* Buffer::get_data() {
    return data_;
}

uint32 Buffer::get_size() {
    return size_;
}

void Buffer::create(uint32 size) {
    clear();

    size_ = size;
    data_ = new uint8[size];
}

void Buffer::clear() {
    if (data_ != null) {
        delete[] data_;
        data_ = null;
        size_ = 0;
    }
}
