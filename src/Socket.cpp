/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Socket.hpp>
#include <SocketImpl.hpp>
#include <Exception.hpp>

/**
 * Methods
 */

Socket::Socket() : handle_(priv::SocketImpl::invalid()), blocking_(true) {

}

Socket::~Socket() {
    close();
}


Socket::Handle Socket::get_handle() const {
    return handle_;
}

void Socket::set_blocking(bool blocking) {
    if (handle_ != priv::SocketImpl::invalid())
        priv::SocketImpl::set_blocking(handle_, blocking);

    blocking_ = blocking;
}

bool Socket::get_blocking() const {
    return blocking_;
}

uint16 Socket::get_port_local() const {
    if (get_handle() != priv::SocketImpl::invalid()) {
        // retrieve informations about the local end of the socket
        sockaddr_in address;
        priv::SocketImpl::addr_size_t size = sizeof(address);
        if (getsockname(get_handle(), reinterpret_cast<sockaddr*>(&address), &size) != -1) {
            return ntohs(address.sin_port);
        }
    }

    return 0;
}

uint16 Socket::get_port_remote() const {
    if (get_handle() != priv::SocketImpl::invalid()) {
        // retrieve informations about the remote end of the socket
        sockaddr_in address;
        priv::SocketImpl::addr_size_t size = sizeof(address);
        if (getpeername(get_handle(), reinterpret_cast<sockaddr*>(&address), &size) != -1) {
            return ntohs(address.sin_port);
        }
    }

    return 0;
}

Address Socket::get_address_remote() const {
    if (get_handle() != priv::SocketImpl::invalid()) {
        // retrieve informations about the remote end of the socket
        sockaddr_in address;
        priv::SocketImpl::addr_size_t size = sizeof(address);
        if (getpeername(get_handle(), reinterpret_cast<sockaddr*>(&address), &size) != -1) {
            return Address(ntohl(address.sin_addr.s_addr));
        }
    }

    return Address::ADDR_NONE;
}

// timeout (in microseconds)
Socket::Status Socket::connect(const Address& remote_addr, uint16 remote_port, uint32 timeout) {
    create();

    // create the remote address
	sockaddr_in address = priv::SocketImpl::create_addr(remote_addr.to_uint32(), remote_port);

    if (timeout == 0) {
        // connect the socket
		if (::connect(get_handle(), reinterpret_cast<struct sockaddr*>(&address), sizeof(address)) == -1)
            return priv::SocketImpl::get_error();

        return STATUS_DONE;
    } else {
        bool blocking = get_blocking();

        // switch to non-blocking to enable our connection timeout
        if (blocking)
            set_blocking(false);

        // try to connect to the remote address
        if (::connect(get_handle(), reinterpret_cast<struct sockaddr*>(&address), sizeof(address)) >= 0) {
            // we got instantly connected
            return STATUS_DONE;
        }

        Status status = priv::SocketImpl::get_error();

        // if we were in non-blocking mode, return immediatly
        if (!blocking)
            return status;

        // otherwise, wait until something happens to our socket (success, timeout or error)
        if (status == STATUS_NOTREADY) {
            fd_set selector;
            FD_ZERO(&selector);
            FD_SET(get_handle(), &selector);

            timeval time;
            time.tv_sec = static_cast<long>(timeout / 1000000);
            time.tv_usec = static_cast<long>(timeout % 1000000);

            // wait for something to write on our socket (which means that the connection request has returned)
            if (select(static_cast<int>(get_handle() + 1), null, &selector, null, &time) > 0) {
                // at this point the connection may have been either accepted or refused
                // to know whether it's a success or a failure, we must check the address of the connected peer
                if (get_address_remote() != Address::ADDR_NONE) {
                    status = STATUS_DONE;
                } else {
                    status = priv::SocketImpl::get_error();
                }
            } else {
                status = priv::SocketImpl::get_error();
            }
        }

        // switch back to blocking mode
        set_blocking(true);

        return status;
    }
}

void Socket::disconnect() {
    // close the socket
    close();
}

Socket::Status Socket::send(const void* buffer, uint32 size) {
    if (!buffer || (size == 0)) {
        return STATUS_ERROR;
    }

    int32 sent = 0;
    int32 size_to_send = static_cast<int32>(size);

    // send a chunk of data
    for (int32 length = 0; length < size_to_send; length += sent) {
        sent = ::send(get_handle(), static_cast<const char*>(buffer) + length, size_to_send - length, 0);

        if (sent < 0)
            return priv::SocketImpl::get_error();
    }

    return STATUS_DONE;
}

Socket::Status Socket::receive(Buffer& buffer) {
	Status status = STATUS_ERROR;
	uint32 received_size = 0;
	
	// by default, but it should be more dynamic
	buffer.create(DEFAULT_BUFFER_SIZE);

	status = receive(static_cast<void*>(buffer.get_data()), buffer.get_size(), received_size);

	if (status != STATUS_DONE) {
		buffer.clear();
	}

	return status;
}

Socket::Status Socket::receive(void* buffer, uint32 size, uint32& received) {
    received = 0;

    if (!buffer) {
        return STATUS_ERROR;
    }

    // receive a chunk of bytes
    int32 size_received = recv(get_handle(), static_cast<char*>(buffer), static_cast<int32>(size), 0);

    // check the number of bytes received
    if (size_received > 0) {
        received = static_cast<uint32>(size_received);
        return STATUS_DONE;
    } else if (size_received == 0) {
        return STATUS_DISCONNECTED;
    } else {
        return priv::SocketImpl::get_error();
    }
}

Socket::Status Socket::listen(uint16 port) {
    create();

    // bind the socket to the specified port
    sockaddr_in address = priv::SocketImpl::create_addr(INADDR_ANY, port);
    if (bind(get_handle(), reinterpret_cast<sockaddr*>(&address), sizeof(address)) == -1) {
        return STATUS_ERROR;
    }

    // listen to the bound port
    if (::listen(get_handle(), 0) == -1) {
        return STATUS_ERROR;
    }

    return STATUS_DONE;
}

Socket::Status Socket::accept(Socket& socket) {
    // make sure that we're listening
    if (get_handle() == priv::SocketImpl::invalid()) {
        return STATUS_ERROR;
    }

    // accept a new connection
    sockaddr_in address;
    priv::SocketImpl::addr_size_t length = sizeof(address);
    Handle remote = ::accept(get_handle(), reinterpret_cast<sockaddr*>(&address), &length);

    // check for errors
    if (remote == priv::SocketImpl::invalid())
        return priv::SocketImpl::get_error();

    // initialize the new connected socket
    socket.close();
    socket.create(remote);

    return STATUS_DONE;
}

void Socket::create() {
    if (handle_ == priv::SocketImpl::invalid()) {
        Handle handle = socket(PF_INET, SOCK_STREAM, 0);
        create(handle);
    }
}

void Socket::create(Socket::Handle handle) {
    if (handle_ == priv::SocketImpl::invalid()) {
        int op_value = 1;

        handle_ = handle;

        set_blocking(blocking_);

        if (setsockopt(handle_, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<char*>(&op_value), sizeof(op_value)) == -1) {
            // throw Exception("Failed to set socket option TCP_NODELAY");
            return;
        }
    }
}

void Socket::close() {
    if (handle_ != priv::SocketImpl::invalid())
    {
        priv::SocketImpl::close(handle_);
        handle_ = priv::SocketImpl::invalid();
    }
}
