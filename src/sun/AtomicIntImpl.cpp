/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#include <Sun/AtomicIntImpl.hpp>

namespace priv {

AtomicIntImpl::AtomicIntImpl(int32 value) : value_(value) {

}

AtomicIntImpl::~AtomicIntImpl() {

}
	
bool AtomicIntImpl::is_null() const {
	return (value_ == 0);
}
	
void AtomicIntImpl::increment() {
	atomic_inc_32(&value_);
}
	
bool AtomicIntImpl::decrement() {
	return !atomic_dec_32_nv(&value_);
}
	
int32 AtomicIntImpl::get_value() const {
	return value_;
}
	
void AtomicIntImpl::set_value(int32 value) {
	value_ = value;
}

int32 AtomicIntImpl::exchange_add(int32 value) {
	const uint32_t nv = atomic_add_32_nv(&value_, value);
	return nv - val;
}

void AtomicIntImpl::add(int32 value) {
	atomic_add_32(&value_, value);
}

}
