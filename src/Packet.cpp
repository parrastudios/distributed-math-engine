/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Packet.hpp>

/**
 * Methods
 */

Packet::Packet() : preamble_(0), type_(0), code_(0), size_(0),
                   data_(null), read_pos_(0), valid_(false), parser_status_(PARSER_WAITING_PREAMBLE) {

}

Packet::Packet(uint8 type, uint8 code, uint32 size, const void* data) : preamble_(0), type_(type), code_(code), size_(size),
                                                      data_(null), read_pos_(0), valid_(false), parser_status_(PARSER_WAITING_PREAMBLE) {
    create();
    if (data != null) {
        set_data(static_cast<const uint8*>(data));
        valid_ = true;
    }
}

Packet::Packet(const Packet& packet) : preamble_(0), type_(0), code_(0), size_(0),
                                       data_(null), read_pos_(0), valid_(false), parser_status_(PARSER_WAITING_PREAMBLE) {
    *this = packet;
}

Packet& Packet::operator=(const Packet& packet) {
	if (&packet != this) {
		// clear packet
		clear();

		// assign header data
		type_ = packet.type_;
		code_ = packet.code_;
		size_ = packet.size_;
		parser_status_ = packet.parser_status_;

		// create data
		create();

		set_data(packet.data_);

		valid_ = true;
	}

    return *this;
}

Packet::~Packet() {
    clear();
}

uint8 Packet::get_type() {
    return type_;
}

uint8 Packet::get_code() {
	return code_;
}

void* Packet::get_data() {
    return static_cast<void*>(data_);
}

void Packet::set_data(const uint8* data, uint32 offset) {
    for (uint32 i = offset; i < size_; ++i) {
        data_[i] = data[i];
    }
}

uint32 Packet::get_size() {
    return size_;
}

void Packet::set_type(uint8 type) {
    type_ = type;
}

void Packet::set_code(uint8 code) {
    type_ = code;
}

void Packet::clear() {
    preamble_ = type_ = code_ = 0;
    size_ = read_pos_ = 0;
    parser_status_ = PARSER_WAITING_PREAMBLE;

    if (data_ != null) {
        delete[] data_;
        data_ = null;
    }

    valid_ = false;
}

void Packet::append(Buffer& buffer) {
	if (buffer.get_size() > 0) {
		append(static_cast<void*>(buffer.get_data()), buffer.get_size());
	}
}

void Packet::append(const void* data, uint32 size) {
    if (data_ == null && size_ == 0) {
        size_ = size;
        create();
    }

    if (data != null) {
        // only append data if packet is not being parsed
        if (parser_status_ == PARSER_WAITING_PREAMBLE) {
            if ((size_ - read_pos_) >= size) {
                set_data(static_cast<const uint8*>(data), read_pos_);
                read_pos_ += size;
            } else {
                // not enough memory for append
                uint8 * buffer = new uint8[size_ + size];

                // copy old data
                for (uint32 i = 0; i < size_; ++i) {
                    buffer[i] = data_[i];
                }

                delete[] data_;

                data_ = buffer;

                // copy the new data
                for (uint32 i = 0; i < size; ++i) {
                    data_[read_pos_ + i] = static_cast<const uint8*>(data)[i];
                }

                read_pos_ += size;

                size_ += size;

                preamble_update();
            }
        }
    }

	if (read_pos_ == size) {
		valid_ = true;
	}
}

void Packet::set(const void* data) {
    if (data != null) {
        if (data_ == null && size_ > 0) {
            create();
        }

        if (data_ != null) {
            set_data(static_cast<const uint8*>(data));
        }
    }
}

uint32 Packet::parse(void* buffer, uint32 size) {
    uint8* data = static_cast<uint8*>(buffer);

    // clear the packet
    clear();

    // parse it
    for (uint32 i = 0; i < size; ++i) {
        switch (parser_status_) {
            case PARSER_WAITING_PREAMBLE : {
                if (data[i] == HEADER_PREAMBLE_UINT8    ||
                    data[i] == HEADER_PREAMBLE_UINT16   ||
                    data[i] == HEADER_PREAMBLE_UINT32) {

                    preamble_ = data[i];
                    parser_status_ = PARSER_WAITING_TYPE;
                }

                break;
            }

            case PARSER_WAITING_TYPE : {
                type_ = data[i];
                parser_status_ = PARSER_WAITING_CODE;
                break;
            }

            case PARSER_WAITING_CODE : {
                code_ = data[i];
                parser_status_ = PARSER_WAITING_LENGTH;
                break;
            }

            case PARSER_WAITING_LENGTH : {
                if (preamble_ == HEADER_PREAMBLE_UINT8) {
                    size_ = *(static_cast<uint8*>(&data[i]));
                } else if (preamble_ == HEADER_PREAMBLE_UINT16) {
                    size_ = *(reinterpret_cast<uint16*>(&data[i]));
                    i += (sizeof(uint16) - 1);  // add the offset
                } else if (preamble_ == HEADER_PREAMBLE_UINT32) {
                    size_ = *(reinterpret_cast<uint32*>(&data[i]));
                    i += (sizeof(uint32) - 1);  // add the offset
                } else {
                    parser_status_ = PARSER_WAITING_PREAMBLE;
                    break;
                }
                parser_status_ = PARSER_WAITING_CHECKSUM;
                break;
            }

            /*
                    size_ =  data[i    ];

                    size_ = (data[i    ] << 8)  |
                             data[i + 1];


                    size_ = (data[i    ] << 24) |
                            (data[i + 1] << 16) |
                            (data[i + 2] << 8)  |
                             data[i + 3];

            */

            case PARSER_WAITING_CHECKSUM : {
                uint32 value = 0x00000000;

                if (preamble_ == HEADER_PREAMBLE_UINT8) {
                    value = *(static_cast<uint8*>(&data[i]));

                    if (size_ + value != 0x000000FFUL) {
                        parser_status_ = PARSER_WAITING_PREAMBLE;
                        break;
                    }
                } else if (preamble_ == HEADER_PREAMBLE_UINT16) {
                    value = *(reinterpret_cast<uint16*>(&data[i]));

                    if (size_ + value != 0x0000FFFFUL) {
                        parser_status_ = PARSER_WAITING_PREAMBLE;
                        break;
                    } else {
                        i += (sizeof(uint16) - 1);  // add the offset
                    }
                } else if (preamble_ == HEADER_PREAMBLE_UINT32) {
                    value = *(reinterpret_cast<uint32*>(&data[i]));

                    if (size_ + value != 0xFFFFFFFFUL) {
                        parser_status_ = PARSER_WAITING_PREAMBLE;
                        break;
                    } else {
                        i += (sizeof(uint32) - 1);  // add the offset
                    }
                }

                if (size_ == 0x00000000) {
                    parser_status_ = PARSER_WAITING_PREAMBLE;
                    valid_ = true;
                    return i; // return size read
                }

                if (size_ > 0x00000000) {
                    create();
                    read_pos_ = 0;
                    parser_status_ = PARSER_WAITING_DATA;
                    break;
                }

                break;
            }

            case PARSER_WAITING_DATA : {

                data_[read_pos_++] = data[i];

                if (read_pos_ == size_) {
                    parser_status_ = PARSER_WAITING_PREAMBLE;
                    valid_ = true;
                    return i; // return size read
                }

                break;
            }
        }
    }

    return 0; // no valid data parsed along the packet
}

void Packet::create() {
    if (data_ == null && size_ > 0) {
        data_ = new uint8[size_];
    }

    preamble_update();
}

void Packet::preamble_update() {
    if (size_ < HEADER_MAX_SIZE_UINT8) {
        preamble_ = HEADER_PREAMBLE_UINT8;
    } else if (size_ < HEADER_MAX_SIZE_UINT16) {
        preamble_ = HEADER_PREAMBLE_UINT16;
    } else if (size_ < HEADER_MAX_SIZE_UINT32) {
        preamble_ = HEADER_PREAMBLE_UINT32;
    }
}

template <typename T>
void Packet::write_size_buffer(Buffer& buffer, uint32& offset) {
    T size = static_cast<T>(size_);
    buffer.set_data(static_cast<void*>(&size), sizeof(T), offset);
    offset += sizeof(T);
    size = ~size;
    buffer.set_data(static_cast<void*>(&size), sizeof(T), offset);
    offset += sizeof(T);
}

template <typename T>
uint32 Packet::get_full_size() {
    // preamble + type + code  + size + checksum + data
    return (sizeof(uint8) * 3) + (sizeof(T) * 2) + size_;
}

void Packet::buffer_pack(Buffer& buffer) {
    uint32 current_offset = 0;
    uint32 full_size = 0;

    if (!valid_) {
        return;
    }

    switch (preamble_) {
        case HEADER_PREAMBLE_UINT8 : {
            full_size = get_full_size<uint8>();
            break;
        }

        case HEADER_PREAMBLE_UINT16 : {
            full_size = get_full_size<uint16>();
            break;
        }

        case HEADER_PREAMBLE_UINT32 : {
            full_size = get_full_size<uint32>();
            break;
        }

        default : {
            return;
        }
    }

    buffer.create(full_size);

    buffer.set_data(static_cast<void*>(&preamble_), sizeof(uint8), current_offset++);
    buffer.set_data(static_cast<void*>(&type_),     sizeof(uint8), current_offset++);
    buffer.set_data(static_cast<void*>(&code_),     sizeof(uint8), current_offset++);

	switch (preamble_) {
        case HEADER_PREAMBLE_UINT8 : {
            write_size_buffer<uint8>(buffer, current_offset);
            break;
        }

        case HEADER_PREAMBLE_UINT16 : {
            write_size_buffer<uint16>(buffer, current_offset);
            break;
        }

        case HEADER_PREAMBLE_UINT32 : {
            write_size_buffer<uint32>(buffer, current_offset);
            break;
        }

        default : {
            return;
        }
    }

    buffer.set_data(static_cast<void*>(data_), size_, current_offset);

    dynamic_assert((size_ + current_offset) == full_size);
}

void Packet::buffer_unpack(Buffer& buffer) {
	if (buffer.get_size() > 0) {
		dynamic_assert(parse(buffer.get_data(), buffer.get_size()) == buffer.get_size());
    }
}
