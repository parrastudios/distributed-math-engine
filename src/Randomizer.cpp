/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Randomizer.hpp>

/**
 * Class definitions
 */
const uint32 Randomizer::COEFFICIENT_Z_DEFAULT = 0x3F01A3B2UL;
const uint32 Randomizer::COEFFICIENT_W_DEFAULT = 0x19AC3C06UL;

/**
 * Methods
 */

Randomizer::Randomizer(uint32 z_coef, uint32 w_coef) : coef_z_(z_coef), coef_w_(w_coef) {
	if (!coef_z_) {
		coef_z_ = COEFFICIENT_Z_DEFAULT;
	}

	if (!coef_w_) {
		coef_w_ = COEFFICIENT_W_DEFAULT;
	}
}

uint32 Randomizer::generate() {
	coef_z_ = 0x9069 * (coef_z_ & 0x0000FFFF) + (coef_z_ >> 0x10);
	coef_w_ = 0x4650 * (coef_w_ & 0x0000FFFF) + (coef_w_ >> 0x10);

	return (coef_z_ << 0x10) + coef_w_;
}
