/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <TrackPoint.hpp>

/**
 * Methods
 */

TrackPoint::TrackPoint() : file_(null), line_(0) {
	mark();
}

TrackPoint::TrackPoint(const char* file, uint32 line) : file_(file), line_(line) {
	
}

TrackPoint::TrackPoint(const TrackPoint& track_point) : file_(null), line_(0) {
    *this = track_point;
}

TrackPoint::~TrackPoint() {

}

const char* TrackPoint::get_file() const {
	return file_;
}

uint32 TrackPoint::get_line() const {
	return line_;
}

const TrackPoint& TrackPoint::mark_impl(const char* file, uint32 line) {
	file_ = file;
	line_ = line;
	return *this;
}

bool TrackPoint::marked() const {
	return ((file_ != null) && (line_ > 0));
}

TrackPoint& TrackPoint::operator=(const TrackPoint& track_point) {
	if (&track_point != this) {
		mark_impl(track_point.file_, track_point.line_);
	}
    return *this;
}

std::ostream& TrackPoint::print(std::ostream& stream) const {
	if (marked()) {
		stream << "file: " << file_
			   << " line: " << line_;
	}

	return stream;
}

std::ostream& operator<<(std::ostream& stream, const TrackPoint& track_point) {
	return track_point.print(stream);
}
