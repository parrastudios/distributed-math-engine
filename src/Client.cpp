/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Client.hpp>
#include <Matrix.hpp>

/**
 * Methods
 */

Client::Client(const Address address, uint16 port) : available_(true), address_(address), port_(port), socket_(),
                                                     protocol_(socket_), incoming_thread_(null),
													 console_(std::cin, std::cout), matrix_manager_() {
	incoming_thread_ = new Thread(&Client::incoming_thread, this);
	// outgoing_thread_ = new Thread(&Client::outgoing_thread, this);
}

Client::~Client() {
	delete incoming_thread_;
	// delete outgoing_thread_;
}

void Client::incoming_request(Packet& packet) {
	// analize and process the problem
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			// matrix code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					// insert into matrix manager
					uint32 block_id = matrix_manager_.create(packet);

					// get the block
					MatrixfBlock* block = matrix_manager_.get_element(block_id);

					// get matrix array
					Matrixf* block_array = block->get_data();

					// run the task
					for (uint32 i = 0; i < block->get_size(); ++i) {
						block_array[i] = block_array[i].invert();
					}

					// send result
					protocol_.handle_outgoing_data(*this, PROTOCOL_TYPE_MATRIX, PROTOCOL_CODE_INVERSE);
					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default : return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {

			// numerical code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default : return;
			}

			break;
		}

		default : return;
	}
}

void Client::incoming_result(Packet& packet) {
	// the result was received (show / save)
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			// matrix code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					// insert into matrix manager
					uint32 block_id = matrix_manager_.create(packet);

					// get the block
					MatrixfBlock* block = matrix_manager_.get_element(block_id);

					// get matrix array
					Matrixf* block_array = block->get_data();

					// show the result
					for (uint32 i = 0; i < block->get_size(); ++i) {
						std::cout << block_array[i] << std::endl;
					}
					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default : return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {

			// numerical code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default : return;
			}

			break;
		}

		default : return;
	}
}

void Client::incoming_data(Packet& packet) {
	if (available_) {
		// problem request from a client
		incoming_request(packet);
	} else {
		// partial result form a client
		incoming_result(packet);
	}

	available_ = !available_;
}

void Client::outgoing_request(Packet& packet) {
	// packet type switch
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			// matrix code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					MatrixfBlock* block = matrix_manager_.get_element();

					block->to_packet(packet);

					// debug
#					if COMPILE_TYPE == DEBUG_MODE
					{
						Matrixf* block_array = block->get_data();

						for (uint32 i = 0; i < block->get_size(); ++i) {
							std::cout << block_array[i] << std::endl;
						}

						for (uint32 i = 0; i < block->get_size(); ++i) {
							std::cout << block_array[i].invert() << std::endl;
						}
					}
#					endif

					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default : return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {

			// numerical code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default : return;
			}

			break;
		}

		default : return;
	}
}

void Client::outgoing_result(Packet& packet) {
	// packet type switch
	switch (packet.get_type()) {
		case PROTOCOL_TYPE_MATRIX : {

			// matrix code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_INVERSE : {
					matrix_manager_.get_element()->to_packet(packet);
					break;
				}

				case PROTOCOL_CODE_TRIANGULATEUP : {

					break;
				}

				default : return;
			}

			break;
		}

		case PROTOCOL_TYPE_NUMERICAL : {

			// numerical code switch
			switch (packet.get_code()) {
				case PROTOCOL_CODE_PI : {

					break;
				}

				case PROTOCOL_CODE_PRIME : {

					break;
				}

				default : return;
			}

			break;
		}

		default : return;
	}
}

void Client::outgoing_data(Packet& packet) {
	if (available_) {
		// send a partial request to a client
		outgoing_request(packet);
	} else {
		// send a final result to a client
		outgoing_result(packet);
	}

	available_ = !available_;
}

void Client::incoming_thread() {
	protocol_.handle_incoming_data(*this);
}

void Client::run() {
	// enable connection
	if (socket_.connect(address_, port_) == Socket::STATUS_DONE) {
		// start console (a basic console)

		// launch protocol threads
		incoming_thread_->launch();
		// outgoing_thread_->launch();

		while (console_.is_open()) {
			switch (console_.read_command()) {
				case CMD_MATRIX_INVERSE : {
					String mat_name;
					uint32 mat_num, mat_size, mat_id;

					// name of matrix
					console_.get_arguments<String>(mat_name);

					// number of matrix
					console_.get_arguments<uint32>(mat_num);

					// size of each matrix
					console_.get_arguments<uint32>(mat_size);

					// create group of matrix
					mat_id = matrix_manager_.create(mat_name, mat_num, mat_size, Matrixf::TYPE_RANDOM); // rnd for test

					// send packet
					protocol_.handle_outgoing_data(*this, PROTOCOL_TYPE_MATRIX, PROTOCOL_CODE_INVERSE);
					break;
				}

				case CMD_MATRIX_TRIANGULATEUP : {
					// number of matrix
					// size of each matrix (must be equal i think)
					// source of matrix
					break;
				}

				case CMD_NUMERICAL_PI : {
					// get number of digits
					break;
				}

				case CMD_EXIT : {
					// close socket
					socket_.disconnect();

					// terminate console
					console_.close();
					break;
				}

				default :
					break;
			}
		}

		//		wait until command
		//		process command
		//		update outgoing data (request)
		//		wait until incoming data (result)
		//		parse and show / save result
		//		loop

		//		wait until incoming data
		//		parse incoming data
		//		process the problem
		//		send outgoing data (result)


		// merge both methods and run only two threads (one for incoming data, and another for console)


		// wait until console thread finishes
		// (console thread terminates safely the incoming data thread)

		incoming_thread_->wait();
		//outgoing_thread_->wait();
	}
}
