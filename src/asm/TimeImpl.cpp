/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Platform.hpp>
#include <Asm/TimeImpl.hpp>

namespace priv {

static inline uint64 cpu_get_clocks() {
	struct {
		uint32 lo_; uint32 hi_;
	} cycles_count;
	
#if (ASM_SYNTAX_TYPE == ASM_SYNTAX_INTEL)
	_asm {
			push eax
			push edx
			_emit 0fh
			_emit 031h
			mov cycles_count.hi_, edx
			mov cycles_count.lo_, eax
			pop edx
			pop eax
		}
#elif (ASM_SYNTAX_TYPE == ASM_SYNTAX_ATANDT)
	asm volatile (
					"CPUID\n\t"
					"RDTSC\n\t"
					"mov %%edx, %0\n\t"
					"mov %%eax, %1\n\t" : "=r" (cycles_count.hi_), "=r"
					(cycles_count.lo_) : : "%rax", "%rbx", "%rcx", "%rdx"
				);
#else
#	error Unsuported assembly syntax
#endif

	return (static_cast<uint64>(cycles_count.hi_) << 32) | cycles_count.lo_;
}

/**
 * Class definitions
 */
uint64 TimeImpl::absolute_start_timestamp_ = 0;

/**
 * Methods
 */

TimeImpl::TimeImpl() {
	absolute_start_timestamp_ = cpu_get_clocks();
}

TimeImpl::~TimeImpl() {

}

uint64 TimeImpl::get_absolute() {
	return cpu_get_clocks() - absolute_start_timestamp_;
}

} // priv
