/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Resource.hpp>

/**
 * Methods
 */

Resource::Resource() : id_(0), name_(""), ref_count_(1) {

}

Resource::Resource(uint32 id, String& name) : id_(id), name_(name), ref_count_(1) {

}

Resource::~Resource() {

}

uint32 Resource::get_id() {
	return id_;
}

void Resource::set_id(uint32 id) {
	id_ = id;
}

String& Resource::get_name() {
	return name_;
}

void Resource::set_name(String& name) {
	name_ = name;
}

uint32 Resource::get_ref_count() {
	return ref_count_;
}

void Resource::increment_ref_count() {
	ref_count_++;
}

void Resource::decrement_ref_count() {
	ref_count_--;
}
