/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Protocol.hpp>

/**
 * Methods
 */

Protocol::Protocol(Socket& socket) : socket_(socket) {
	// socket connection must be established
}

Protocol::~Protocol() {
	// close socket
	socket_.disconnect();
}

void Protocol::handle_incoming_data(ProtocolHandler& handler) {
	Buffer buffer;

	// allocate the buffer and receive data
	while (socket_.receive(buffer) == Socket::STATUS_DONE) {
		// check if there is data available
		if (buffer.get_size() > 0) {
			// create the packet
			Packet packet;

			// unpack the buffer to the packet structure
			packet.buffer_unpack(buffer);

			// handle it
			handler.incoming_data(packet);

			// clear buffer to handle new data
			buffer.clear();
		} else {
			// error: invalid packet
			break;
		}
	}

	// clear the buffer
	buffer.clear();
}

void Protocol::handle_outgoing_data(ProtocolHandler& handler, uint8 type, uint8 code) {
	Buffer buffer;
	Packet* packet = null;

	// create packet
	packet = new Packet(type, code);

	// handle packet
	handler.outgoing_data(*packet);

	// update buffer
	packet->buffer_pack(buffer);

	if (buffer.get_size() > 0) {
		if (socket_.send(static_cast<void*>(buffer.get_data()), buffer.get_size()) != Socket::STATUS_DONE) {
			// error: impossible to send data
		}
	}

	buffer.clear();

	delete packet;
}
