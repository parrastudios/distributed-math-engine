/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Application.hpp>

/**
 * Application entry point
 */
int main(int argc, char* argv[]) {
	Application application(argv, argc);

    try {
		application.run();
    } catch (Exception& exception) {
        return application.error_throw(exception);
    }

    return 0;
}

/*
#include <DifferentialEquation.hpp>

	const uint32 iterations = 25;

	float32 u_pattern_a_impl[] = { 1.5f };
	float32 u_pattern_b_impl[] = { 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f };

	float32 u_coef_i_impl[] = { 0.0f, 0.0f, 4.0f };
	float32 y_coef_i_impl[] = { 0.0f, 0.2f, 0.8f, -0.1f };
	float32 u_coef_ii_impl[] = { 0.0f, 2.0f, -1.1f, 0.05f };
	float32 y_coef_ii_impl[] = { 0.0f, 0.0f, -0.15f, 0.4f };
	float32 u_coef_iii_impl[] = { 0.0f, 2.0f, 0.1f, -0.8f };
	float32 y_coef_iii_impl[] = { 0.0f, 0.1f, 0.0f, -0.8f };

	Vectorf u_pattern_a(u_pattern_a_impl, 1);
	Vectorf u_pattern_b(u_pattern_b_impl, 6);

	Vectorf u_coef_i(u_coef_i_impl, 3);
	Vectorf y_coef_i(y_coef_i_impl, 4);
	Vectorf u_coef_ii(u_coef_ii_impl, 4);
	Vectorf y_coef_ii(y_coef_ii_impl, 4);
	Vectorf u_coef_iii(u_coef_iii_impl, 4);
	Vectorf y_coef_iii(y_coef_iii_impl, 4);

	DifferentialEquationf y_i_a(u_coef_i, y_coef_i, u_pattern_a, 2);
	DifferentialEquationf y_i_b(u_coef_i, y_coef_i, u_pattern_b, 2);
	DifferentialEquationf y_ii_a(u_coef_ii, y_coef_ii, u_pattern_a, 1);
	DifferentialEquationf y_ii_b(u_coef_ii, y_coef_ii, u_pattern_b, 1);
	DifferentialEquationf y_iii_a(u_coef_iii, y_coef_iii, u_pattern_a, 1);
	DifferentialEquationf y_iii_b(u_coef_iii, y_coef_iii, u_pattern_b, 1);

	// first equation

	// solve y (i) with a signal
	y_i_a.solve(iterations);

	// print
	std::cout << "First equation (i) with signal (a):" << std::endl << y_i_a << std::endl;

	// solve y (i) with b signal
	y_i_b.solve(iterations);

	// print
	std::cout << "First equation (i) with signal (b):" << std::endl << y_i_b << std::endl;

	// second equation

	// solve y (ii) with a signal
	y_ii_a.solve(iterations);

	// print
	std::cout << "Second equation (ii) with signal (a):" << std::endl << y_ii_a << std::endl;

	// solve y (ii) with b signal
	y_ii_b.solve(iterations);

	// print
	std::cout << "Second equation (ii) with signal (b):" << std::endl << y_ii_b << std::endl;

	// third equation

	// solve y (iii) with a signal
	y_iii_a.solve(iterations);

	// print
	std::cout << "Third equation (iii) with signal (a):" << std::endl << y_iii_a << std::endl;

	// solve y (iii) with b signal
	y_iii_b.solve(iterations);

	// print
	std::cout << "Third equation (iii) with signal (b):" << std::endl << y_iii_b << std::endl;
*/


/*
#include <Endianness.hpp>

{
	uint32 endianness = Endianness::instance().get();
}
*/

/*
	Socket sock;
    Socket::Status status = sock.connect(Address(173, 194, 34, 18), 80, 0);

    if (Socket::STATUS_DONE == status) {
        cout << "connected!" << endl;
        sock.disconnect();
    } else {
        cout << "mielda " << status << endl;
    }
*/
/*
Mutex myMut;
Matrixd A(3, 3, Matrixd::TYPE_RANDOM);

void func_t() {
    myMut.lock();
    A = (A.invert());
    cout << "From func_t thread (A): " << endl << A << endl << " ------------------------ " << endl;
    myMut.unlock();
}
*/


        /*
        Thread mythread(&func_t);

        cout << "Initialy the matrix is: " << endl << A << endl << " ------------------------ " << endl;


        mythread.launch();
        myMut.lock();
        A = (A.invert());
        cout << "From main thread (B): " << endl << A << endl << " ------------------------ " << endl;
        myMut.unlock();
        mythread.wait();
        */

  /*
        Socket sock, accepted;

        sock.listen(7777);

        sock.accept(accepted);

        sock.disconnect();

*/

/*
        Socket sock;
        Socket::Status status = sock.connect(Address(173, 194, 34, 18), 80, 10000);

        if (Socket::STATUS_DONE == status) {
            cout << "connected!" << endl;
            sock.disconnect();
        } else {
            cout << "mielda " << status << endl;
        }

*/
