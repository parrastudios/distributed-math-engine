/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Memory.hpp>

/**
 * Methods
 */

/** Checks for memory leaks
 * @return the number of leaks
 */
uint32 memory_check_leacks() {
	// todo
	return 0;
}

/** Checks for heap corruption
 * @return the number of memory corruptions found
 */
uint32 memory_check_corruption() {
	// todo
	return 0;
}

template <typename T>
T* MemoryDebugRecorder::operator->*(T* pointer) {
	process(pointer);
	return pointer;
}

void MemoryDebugRecorder::process(void* pointer) {
	// todo
}

namespace priv {

uint32 MemoryDebugCounter::count_ = 0;

MemoryDebugCounter::MemoryDebugCounter() {
	++count_;
}

MemoryDebugCounter::~MemoryDebugCounter() {
	if (--count_ == 0) {
		if (memory_check_leacks()) {
#			if (COMPILER_TYPE == COMPILER_GNUC)
#				if (COMPILER_VERSION >= compiler_version_build(3, 4, 0))
					dynamic_assert(getenv("GLIBCXX_FORCE_NEW") != null);
#				elif (COMPILER_VERSION >= compiler_version_build(3, 2, 0)
					dynamic_assert(getenv("GLIBCPP_FORCE_NEW") != null);
#				endif
#			endif
		}
	}
}

} // priv
