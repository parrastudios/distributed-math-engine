/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Time.hpp>
#include <TimeImpl.hpp>

/**
 * Methods
 */

Time::Time() : start_time_(0), stop_time_(0) {
	
}

Time::~Time() {

}

void Time::clear() {
	start_time_ = stop_time_ = 0;
}

uint64 Time::start() {
	stop_time_ = 0;
	start_time_ = get_absolute();

	return start_time_;
}

uint64 Time::restart() {
	if (start_time_ > 0) {
		uint64 last_start = start_time_;

		stop_time_ = 0;
		start_time_ = get_absolute();

		// return elapsed time
		return (start_time_ - last_start);
	} else {
		// start timer
		stop_time_ = 0;
		start_time_ = get_absolute();

		// not elapsed time
		return 0;
	}
}

void Time::resume() {
	if (start_time_ > 0 && stop_time_ > 0) {
		start_time_ = get_absolute() - (stop_time_ - start_time_);
		stop_time_ = 0;
	}
}

uint64 Time::stop() {
	// if already stopped
	if (stop_time_ > 0) {
		return stop_time_;
	}

	// if started
	if (start_time_ > 0) {
		stop_time_ = get_absolute();
		return stop_time_;
	}

	// if not started
	stop_time_ = 0;

	return 0;
}

uint64 Time::get() {
	if (start_time_ > 0) {
		if (stop_time_ > 0) {
			return stop_time_ - start_time_;
		} else {
			return get_absolute() - start_time_;
		}
	}

	return 0;
}

uint64 Time::get_absolute() {
	return priv::TimeImpl::get_absolute();
}
