/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Endianness.hpp>

/**
 * Methods
 */

uint32 Endianness::get() {
	static endianness_buffer buffer;
	static uint32 endian_type = ENDIAN_UNCHECKED;

	if (endian_type == ENDIAN_UNCHECKED) {
		switch (buffer.integer_) {
			case 0x00010203 : {
				endian_type = ENDIAN_BIG;
				break;
			}
			case 0x03020100 : {
				endian_type = ENDIAN_LITTLE;
				break;
			}
			case 0x02030001 : {
				endian_type = ENDIAN_BIG_WORD;
				break;
			}
			case 0x01000302 : {
				endian_type = ENDIAN_LITTLE_WORD;
				break;
			}
			default	: {
				endian_type = ENDIAN_UNKNOWN;
				break;
			}
		}
	}

	return endian_type;
}

uint16 Endianness::swap_uint16(uint16 value) {
	return (value << 8) | (value >> 8);
}

int16 Endianness::swap_int16(int16 value) {
	return (value << 8) | ((value >> 8) & 0xFF);
}

uint32 Endianness::swap_uint32(uint32 value) {
	value = ((value << 8) & 0xFF00FF00) | ((value >> 8) & 0xFF00FF); 
	return (value << 16) | (value >> 16);
}

int32 Endianness::swap_int32(int32 value) {
	value = ((value << 8) & 0xFF00FF00) | ((value >> 8) & 0xFF00FF); 
	return (value << 16) | ((value >> 16) & 0xFFFF);
}

int64 Endianness::swap_int64(int64 value) {
	value = ((value << 8) & 0xFF00FF00FF00FF00ULL) | ((value >> 8) & 0x00FF00FF00FF00FFULL);
	value = ((value << 16) & 0xFFFF0000FFFF0000ULL) | ((value >> 16) & 0x0000FFFF0000FFFFULL);
	return (value << 32) | ((value >> 32) & 0xFFFFFFFFULL);
}

uint64 Endianness::swap_uint64(uint64 value) {
	value = ((value << 8) & 0xFF00FF00FF00FF00ULL ) | ((value >> 8) & 0x00FF00FF00FF00FFULL);
	value = ((value << 16) & 0xFFFF0000FFFF0000ULL ) | ((value >> 16) & 0x0000FFFF0000FFFFULL);
	return (value << 32) | (value >> 32);
}

template <typename T>
void Endianness::swap_bytes(T* source) {
	uint32 size = sizeof(T);
	uint8* pointer = static_cast<uint8*>(source);
	uint32 lo, hi, temp;

	for (lo = 0, hi = size - 1; hi > lo; ++lo, --hi) {
		temp = pointer[lo];
		pointer[lo] = pointer[hi];
		pointer[hi] = temp;
	}
}
