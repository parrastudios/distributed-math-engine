/////////////////////////////////////////////////////////////////////////////
//  Distribuited Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#include <Unix/MutexImpl.hpp>

namespace priv {

MutexImpl::MutexImpl() {
    pthread_mutexattr_init(&attributes_);
    pthread_mutexattr_settype(&attributes_, PTHREAD_MUTEX_RECURSIVE);

    pthread_mutex_init(&mutex_, &attributes_);
}

MutexImpl::~MutexImpl() {
    pthread_mutex_destroy(&mutex_);
	pthread_mutexattr_destroy(&attributes_);
}

void MutexImpl::lock() {
    pthread_mutex_lock(&mutex_);
}

bool MutexImpl::trylock() {
	return (pthread_mutex_trylock(&mutex_) == 0);
}

void MutexImpl::unlock() {
    pthread_mutex_unlock(&mutex_);
}

}
