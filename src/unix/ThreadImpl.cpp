/////////////////////////////////////////////////////////////////////////////
//  Distribuited Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#include <Win32/ThreadImpl.hpp>
#include <Thread.hpp>
#include <process.h>

namespace priv {

ThreadImpl::ThreadImpl(Thread* owner) : active_(false) {
    active_ = pthread_create(&thread_, null, &ThreadImpl::function, owner) == 0;

    if (!active_) {
        // error
    }
}

void ThreadImpl::wait() {
    if (active_) {
		if (pthread_equal(pthread_self(), thread_) == 0) {
            pthread_join(thread_, null);
        }
    }
}

void ThreadImpl::terminate() {
	if (active_) {
        pthread_cancel(thread_);
	}
}

void* ThreadImpl::function(void* data) {
    Thread* owner = static_cast<Thread*>(data);

    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, null);

    owner->run();

    return null;
}

}
