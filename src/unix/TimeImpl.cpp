/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Unix/TimeImpl.hpp>

namespace priv {

/**
 * Class definitions
 */
struct timespec TimeImpl::time_ = { 0, 0 };

/**
 * Methods
 */

TimeImpl::TimeImpl() {

}

TimeImpl::~TimeImpl() {

}

uint64 TimeImpl::get_absolute() {
	clock_gettime(CLOCK_MONOTONIC, &time_);
	
	return (static_cast<uint64>(time_.tv_sec) * 1000000 + time_.tv_nsec);
}

} // priv
