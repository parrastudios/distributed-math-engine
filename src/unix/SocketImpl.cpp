/////////////////////////////////////////////////////////////////////////////
//  Distribuited Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#include <Win32/SocketImpl.hpp>

namespace priv {

sockaddr_in SocketImpl::create_addr(uint32 address, uint16 port) {
    sockaddr_in addr;
    std::memset(addr.sin_zero, 0, sizeof(addr.sin_zero));
    addr.sin_addr.s_addr = htonl(address);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    return addr;
}

Socket::Handle SocketImpl::invalid() {
    return -1;
}

void SocketImpl::close(Socket::Handle handle) {
    ::close(handle);
}

void SocketImpl::set_blocking(Socket::Handle handle, bool block) {
    int status = fcntl(handle, F_GETFL);
    if (block)
        fcntl(handle, F_SETFL, status & ~O_NONBLOCK);
    else
        fcntl(handle, F_SETFL, status | O_NONBLOCK);
}

Socket::Status SocketImpl::get_error() {
    if ((errno == EAGAIN) || (errno == EINPROGRESS))
        return Socket::STATUS_NOTREADY;

    switch (errno) {
        case EWOULDBLOCK  : return Socket::STATUS_NOTREADY;
        case ECONNABORTED : return Socket::STATUS_DISCONNECTED;
        case ECONNRESET   : return Socket::STATUS_DISCONNECTED;
        case ETIMEDOUT    : return Socket::STATUS_DISCONNECTED;
        case ENETRESET    : return Socket::STATUS_DISCONNECTED;
        case ENOTCONN     : return Socket::STATUS_DISCONNECTED;
        default           : return Socket::STATUS_ERROR;
    }
}

} // priv
