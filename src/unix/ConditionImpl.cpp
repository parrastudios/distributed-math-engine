/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#include <Unix/ConditionImpl.hpp>

namespace priv {

ConditionImpl::ConditionImpl() {
	pthread_condattr_init(attr_);
	pthread_cond_init(&cond_, &attr_);
}

ConditionImpl::~ConditionImpl() {
	pthread_cond_destroy(&cond_);
	pthread_condattr_destroy(&attr_);
}

void ConditionImpl::wait(Lock& lock, uint32 milliseconds) {
	if (milliseconds) {
		struct timespec timeout;

		timeout.tv_sec = 0UL;
		timeout.tv_nsec = milliseconds * 1000;

		pthread_cond_timedwait(&cond_, reinterpret_cast<pthread_mutex_t>(&lock.mutex_), &timeout);
	} else {
		pthread_cond_wait(&cond_, reinterpret_cast<pthread_mutex_t>(&lock.mutex_));
	}
}

template <typename Function>
void ConditionImpl::wait(Lock& lock, Function func, uint32 milliseconds) {
	while (!func()) {
		wait(lock, milliseconds);
	}
}
void ConditionImpl::notify() {
	pthread_cond_signal(&cond_);
}

void ConditionImpl::notify_all() {
	pthread_cond_broadcast(&cond_);
}

}
