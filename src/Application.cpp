/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Application.hpp>
#include <Debug.hpp>
#include <Client.hpp>
#include <Server.hpp>

/**
 * Definitions
 */
#if COMPILE_TYPE == DEBUG_MODE
#	define DEBUG_CLIENT			0x00
#	define DEBUG_SERVER			0x01
#	define APPLICATION_TEST		DEBUG_SERVER
#endif

/**
 * Class definitions
 */
const char  Application::tab_	= '\t';
const char* Application::name_	= APPLICATION_NAME;
const char* Application::ver_	= APPLICATION_VERSION;

/**
 * Methods
 */

Application::Application(char* args[], int32 size) : arg_list_(args), arg_size_(size), app_arg_pos_(-1), app_impl_(null) {
#if APPLICATION_TEST == DEBUG_CLIENT
	app_arg_pos_ = 1;
	arg_size_ = 4;

	arg_list_[0] = "dme";
	arg_list_[1] = "-c";
	arg_list_[2] = "127.0.0.1";
	arg_list_[3] = "666";
	
	client();
#elif APPLICATION_TEST == DEBUG_SERVER
	app_arg_pos_ = 1;
	arg_size_ = 3;

	arg_list_[0] = "dme";
	arg_list_[1] = "-s";
	arg_list_[2] = "666";

	server();
#else
	switch (arguments_parse()) {
        case APPLICATION_CLIENT : {
            client();
            break;
        }

        case APPLICATION_SERVER : {
            server();
            break;
        }

        default : {
			throw Exception("Invalid arguments");
        }
    }
#endif
}

Application::~Application() {
	if (app_impl_ != null) {
		delete app_impl_;
	}
}

void Application::run() {
	app_impl_->run();
}

Runnable& Application::client() {
	if (app_impl_ == null) {
		// get ip and port
		String address;
		uint16 port;

		std::cout << "Running " << name_ << " Client " << ver_ << std::endl;

		address = arg_list_[app_arg_pos_ + 1];
		port = atoi(arg_list_[app_arg_pos_ + 2]);

		app_impl_ = new Client(Address(address), port);
	}

	return (*app_impl_);
}

Runnable& Application::server() {
	if (app_impl_ == null) {
		// get ip and ...
		uint16 port;

		std::cout << "Running " << name_ << " Server " << ver_ << std::endl;

		port = atoi(arg_list_[app_arg_pos_ + 1]);

		app_impl_ = new Server(port);
	}

	return (*app_impl_);
}

std::ostream& Application::arguments_print_man(std::ostream& stream) {
    stream << "Syntax" << std::endl << tab_ << "<program_name> [options] [arguments]" << std::endl;
    stream << "Key"    << std::endl << tab_ << "-c, --client [ip]   [port]" << tab_ << "executes client application" << std::endl;
    stream							<< tab_ << "-s, --server [port] [...]"  << tab_ << "executes server application" << std::endl;
    return stream;
}

uint32 Application::arguments_parse() {
    for (int32 i = 1; i < (arg_size_ - 1); ++i) {
		if (arg_list_[i][0] == '-') {
            if (arg_list_[i][1] == 'c') {
				app_arg_pos_ = i;
                return APPLICATION_CLIENT;
            } else if (arg_list_[i][1] == 's') {
				app_arg_pos_ = i;
                return APPLICATION_SERVER;
            }
			// allow other future options
        } else {
            std::cout << "Invalid syntax: " << arg_list_[i] << std::endl;
        }
    }

    std::cout << "Invalid initialization - use the following syntax:" << std::endl;
	
	arguments_print_man(std::cout);

    return APPLICATION_ERROR;
}

uint32 Application::error_throw(Exception& exception) {
    std::cout << "Error: " << exception << std::endl;
    return APPLICATION_ERROR;
}
