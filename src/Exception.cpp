/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <Platform.hpp>
#include <Exception.hpp>

#if defined (PLATFORM_FAMILY_WINDOWS)
#	include <windows.h>
#elif defined(PLATFORM_FAMILY_UNIX) || defined(PLATFORM_FAMILY_BSD)
#	include <cerrno>
#	include <cstring>
#else
	// todo
#endif

/**
 * Class definitions
 */
const uint32 Exception::EXCEPTION_INVALID = 0x7FFFFFFF;
const uint32 Exception::EXCEPTION_UNKNOWN = 0x7FFFFFFE;
const uint32 Exception::EXCEPTION_SYSTEM = 0x7FFFFFFD;

/**
 * Methods
 */

Exception::Exception() : TrackPoint(), msg_(), id_(EXCEPTION_INVALID), code_(EXCEPTION_INVALID), handler_() {

}

Exception::Exception(const char* message, uint32 id, uint32 code) :
	TrackPoint(), msg_(message), id_(id), code_(code), handler_() {

	if (id_ == EXCEPTION_SYSTEM) {
		get_system_error();
	}
}

Exception::Exception(const char* message, ExceptionHandler& handler, uint32 id, uint32 code) :
	TrackPoint(), msg_(message), id_(id), code_(code), handler_(&handler, &ExceptionHandler::process) {
	
	if (id_ == EXCEPTION_SYSTEM) {
		get_system_error();
	}
}

Exception::Exception(const Exception& exception) : TrackPoint(), msg_(), id_(EXCEPTION_INVALID), code_(EXCEPTION_INVALID), handler_() {
    *this = exception;
}

Exception::~Exception() throw() {
	if (!handler_ == false) {
		handler_(*this);
	}
}

const String& Exception::get_message() const {
    return msg_;
}

const uint32 Exception::get_id() const {
	return id_;
}

const uint32 Exception::get_code() const {
	return code_;
}

const Exception& Exception::raise_impl(const TrackPoint& track) throw() {
	file_ = track.get_file();
	line_ = track.get_line();

	if (!handler_ == false) {
		handler_(*this);
	}

	return *this;
}

void Exception::register_handler(ExceptionHandler& handler) {
	handler_ = HandlerCallback(&handler, &ExceptionHandler::process);
}

const Exception& Exception::register_track(const TrackPoint& track) {
	file_ = track.get_file();
	line_ = track.get_line();
	return *this;
}

bool Exception::thrown() const {
	return ((!msg_.empty()) && (id_ != EXCEPTION_INVALID) && (code_ != EXCEPTION_INVALID) && marked());
}

Exception& Exception::operator=(const Exception& exception) {
	if (&exception != this) {
		file_		= exception.get_file();
		line_		= exception.get_line();
		msg_		= exception.msg_;
		id_			= exception.id_;
		code_		= exception.code_;
		handler_	= exception.handler_;
	}

	return *this;
}

void Exception::get_system_error() {
#if defined(PLATFORM_FAMILY_WINDOWS)
	LPSTR message_buffer = null;

	// get last error from win api
	code_ = ::GetLastError();

	if (::FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, null, code_, 0, // default language
						 reinterpret_cast<LPSTR>(&message_buffer), 0, null)) {

		msg_ = message_buffer;
		::LocalFree(message_buffer);
	} else {
		msg_ = "unknown system error";
		code_ = EXCEPTION_UNKNOWN;
	}
#elif defined(PLATFORM_FAMILY_UNIX) || defined(PLATFORM_FAMILY_BSD)
	code_ = errno;
	msg_ = strerror(code_);
#else
	// todo
	msg_ = "platform specific system error not implemented";
	code_ = EXCEPTION_UNKNOWN;
#endif
}

std::ostream& Exception::print(std::ostream& stream) const {
	if (thrown()) {
		stream << "exception generated : " << msg_ <<
				  "(id: " << id_ <<
				  " code: " << code_;
		if (marked()) {
			stream << " in file: " << file_
				   << " line: " << line_;
		}

		stream << ")";

	} else {
		stream << "exception not thrown";
	}

    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Exception& exception) {
	return exception.print(stream);
}
