/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

/**
 * Headers
 */
#include <MacOs/TimeImpl.hpp>

namespace priv {

/**
 * Class definitions
 */
struct mach_timebase_info TimeImpl::timebase_info_ = { 0, 0 };

/**
 * Methods
 */

TimeImpl::TimeImpl() {
	mach_timebase_info(&timebase_info_);
}

TimeImpl::~TimeImpl() {

}

uint64 TimeImpl::get_absolute() {
		return static_cast<uint64>(mach_absolute_time() * timebase_info_.numer / timebase_info_.denom);
}

} // priv
