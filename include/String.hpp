/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef STRING_HPP
#define STRING_HPP

#include <Vector.hpp>
//#include <Mutex.hpp>

#include <iostream> // #include <Stream.hpp>

template <typename T>
class StringBase;

template <typename T>
std::ostream& operator<<(std::ostream& stream, const StringBase<T>& string);

template <typename T>
std::istream& operator>>(std::istream& stream, const StringBase<T>& string);

template <typename T>
class StringBase : public Vector<T> {
  public:
	static const T null_char;

	StringBase();

	explicit StringBase(const T* string);

	StringBase(const StringBase<T>& string);

	virtual ~StringBase();

	StringBase<T> substr(uint32 length, uint32 offset = 0);

	bool readfield(StringBase<T>& result, T delimiter, uint32 position) const;

	StringBase<T> readfield(T delimiter, uint32 position) const;

	Vector<StringBase<T>>& split(Vector<StringBase<T>>& vector, T delimiter) const;
	
	Vector<StringBase<T>> split(T delimiter) const;

	void copy(const T* string);

	template <typename U>
	static StringBase<T> from(const U& value);

	template <>
	static StringBase<T> from<uint32>(const uint32& value) {
		
		return StringBase<T>();
	}

	StringBase<T>& operator= (const T* string);

	StringBase<T>  operator+ (const StringBase<T>& string);
	StringBase<T>& operator+=(const StringBase<T>& string);

	T* operator*();
	const T* operator*() const;

	bool operator==(const StringBase<T>& string);
	bool operator==(const T* string);
	bool operator!=(const StringBase<T>& string);
	bool operator!=(const T* string);
	bool operator< (const StringBase<T>& string);
	bool operator< (const T* string);
	bool operator> (const StringBase<T>& string);
	bool operator> (const T* string);
	bool operator<=(const StringBase<T>& string);
	bool operator<=(const T* string);
	bool operator>=(const StringBase<T>& string);
	bool operator>=(const T* string);

  protected:

    /** Overloaded ostream opearator
     */
	friend std::ostream& operator<< <>(std::ostream& stream, const StringBase<T>& string);

    /** Overloaded istream opearator
     */
	friend std::istream& operator>> <>(std::istream& stream, const StringBase<T>& string);

	/** Member data
	 */
//	Mutex	mutex_;
};

#include <String.inl>

typedef StringBase<char>			String;

#if defined(PLATFORM_FAMILY_WINDOWS) && (COMPILER_TYPE == COMPILER_MSVC)
#	ifdef _UNICODE
		typedef StringBase<wchar_t>	String16;
#	else
		typedef StringBase<TCHAR>	String16;
#	endif
#else
	typedef StringBase<uint16>		String16;
#endif

typedef StringBase<uint32>			String32;

#endif // STRING_HPP
