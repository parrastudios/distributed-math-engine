/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#if !defined MATRIXMANAGER_HPP || defined MATRIXMANAGER_INL
#	error MatrixManager.hpp included directly
#endif

#define MATRIXMANAGER_INL

template <typename T>
MatrixBlock<T>::MatrixBlock(uint32 id, String& name) : Resource(id, name), size_(0), data_(null) {

}

template <typename T>
MatrixBlock<T>::MatrixBlock(uint32 id, String& name, uint32 size) : Resource(id, name), size_(size), data_(null) {
	data_ = new Matrix<T>[size_];
}

template <typename T>
MatrixBlock<T>::MatrixBlock(uint32 id, String& name, uint32 size, Matrix<T>* data) :
							Resource(id, name), size_(size), data_(null) {
	for (uint32 i = 0; i < size_; ++i) {
		data_[i] = data[i];
	}
}

template <typename T>
MatrixBlock<T>::~MatrixBlock() {
	destroy();
}

template <typename T>
void MatrixBlock<T>::destroy() {
	if (size_ > 0 && data_) {
		delete[] data_;
	}
	size_ = 0;
}

template <typename T>
void MatrixBlock<T>::copy(MatrixBlock<T>& matrix_block) {
	destroy();

	size_ = matrix_block.size_;
	data_ = new Matrix<T>[size_];

	for (uint32 i = 0; i < size_; ++i) {
		data_[i].create(matrix_block.data_[i]);
	}
}

template <typename T>
void MatrixBlock<T>::create(uint32 count, uint32 size, enum Matrix<T>::Type type) {
	if (!data_) {
		size_ = count;
		data_ = new Matrix<T>[size_];

		for (uint32 i = 0; i < size_; ++i) {
			data_[i].create(size, size, type);
		}
	}
}

template <typename T>
Matrix<T>* MatrixBlock<T>::get_data() {
	return data_;
}

template <typename T>
uint32 MatrixBlock<T>::get_size() {
	return size_;
}

template <typename T>
void MatrixBlock<T>::to_packet(Packet& packet) {
	uint32 rows, cols;
	String::size_type name_size = name_.size();

	// save block name
	packet.append(static_cast<void*>(&name_size), sizeof(String::size_type));
	packet.append(static_cast<void*>(*name_), name_size * sizeof(String::type));

	// save the size of array
	packet.append(static_cast<void*>(&size_), sizeof(uint32));

	// save matrix array
	for (uint32 i = 0; i < size_; ++i) {
		rows = data_[i].get_rows();
		cols = data_[i].get_cols();
		packet.append(static_cast<void*>(&rows), sizeof(uint32));
		packet.append(static_cast<void*>(&cols), sizeof(uint32));
		packet.append(static_cast<void*>(data_[i].get_data()), rows * cols * sizeof(T));
	}
}

template <typename T>
void MatrixBlock<T>::from_packet(Packet& packet) {
	char* cstr_ptr;
	uint32 str_size, rows, cols;
	int8* read_ptr = static_cast<int8*>(packet.get_data());

	// read name size
	str_size = *(reinterpret_cast<uint32*>(read_ptr));
	read_ptr += sizeof(uint32);

	// read cstring name
	cstr_ptr = reinterpret_cast<char*>(read_ptr);
	name_ = cstr_ptr;
	read_ptr += str_size;

	// read the size of array
	size_ = *(reinterpret_cast<uint32*>(read_ptr));
	read_ptr += sizeof(uint32);

	// create matrix array
	data_ = new Matrix<T>[size_];

	// read matrix
	for (uint32 i = 0; i < size_; ++i) {
		rows = *(reinterpret_cast<uint32*>(read_ptr));
		read_ptr += sizeof(uint32);

		cols = *(reinterpret_cast<uint32*>(read_ptr));
		read_ptr += sizeof(uint32);

		data_[i].create(rows, cols, reinterpret_cast<T*>(read_ptr));
		read_ptr += (rows * cols * sizeof(T));
	}
}

template <typename T>
MatrixManager<T>::MatrixManager() : handles_() {

}

template <typename T>
MatrixManager<T>::~MatrixManager() {
	handles_.clear();
}

template <typename T>
uint32 MatrixManager<T>::create(String& name, uint32 count, uint32 size, enum Matrix<T>::Type type) {
	uint32 id = add(name);

	MatrixBlock<T>* resource = get_element(id);

	resource->create(count, size, type);

	return id;
}

template <typename T>
uint32 MatrixManager<T>::create(Packet& packet) {
	MatrixBlock<T>* resource = null;
	uint32 id = 0;

	// create the temp block instance
	MatrixBlock<T>* tmp_block = new MatrixBlock<T>(0, String(""));

	// read from packet
	tmp_block->from_packet(packet);

	// add to the manager
	id = add(tmp_block->get_name());

	// get the original resource
	resource = get_element(id);

	// copy data from temp to managed resource
	resource->copy(*tmp_block);

	// delete the temp resource
	// todo: memory leak here
	// delete tmp_block;
	tmp_block = null;

	// return resource id
	return id;
}

template <typename T>
Matrix<T>* MatrixManager<T>::get(uint32 id) {
	return (*ResourceManager<T>::resource_list_)[id].get_data();
}

template <typename T>
Vector<uint32> MatrixManager<T>::slice(uint32 id, uint32 size) {
	Vector<uint32> vector;

	if (size != 0) {
		if (size == 1) {
			// create vector
			vector = Vector<uint32>(1);

			// push unique id
			vector.push_back(id);
		} else {
			MatrixBlock<T>* block = (*resource_list_)[id];
			uint32 new_size = block->get_size() / size;
			uint32 new_offset = block->get_size() % size;

			// create vector
			vector = Vector<uint32>(size);

			// push create sub-blocks and push all ids
			for (uint32 i = 0; i < block->get_size(); i += new_size) {
				String result_str;
				uint32 new_id;

				result_str = block->get_name() + String::from<uint32>(i);

				new_id = add(result_str);

				if (new_id != ResourceManager<T>::INVALID_ID) {
					MatrixBlock<T>* new_block = (*resource_list_)[new_id];

					new_block->create(block->get_data()[i].get_cols(), new_size, Matrix<T>::TYPE_ZERO);

					for (uint32 j = 0; j < new_size; ++j) {
						new_block->get_data()[j] = block->get_data()[i + j];
					}

					vector.push_back(new_id);
				}
			}

			// add to the last
			if (new_offset > 0) {
				//uint32 last_index = block->get_size() - new_offset;

				//new_block->create(block->get_data()[i].get_cols(), new_size, Matrix<T>::TYPE_ZERO);

				//for (uint32 i = 0; i < new_offset; ++i) {

				//}
			}

			// remove the original block
			remove(id);
		}
	}

	return vector;
}
