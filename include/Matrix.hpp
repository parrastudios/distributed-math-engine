/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef MATRIX_HPP
#define MATRIX_HPP

/**
 * Headers
 */
#include <Types.hpp>
#include <Exception.hpp>
#include <Swap.hpp>
#include <Buffer.hpp>
#include <Randomizer.hpp>

/** Matrix
 *
 * A template to process matrices and operate with them.
 *
 * @see Exception
 *
 */

template <typename T>
class Matrix {
  public:

    /** Type of initialization
     */
    enum Type {
        TYPE_NULL,      //< uninitialized
        TYPE_ZERO,      //< zero matrix
        TYPE_IDENTITY,  //< identity matrix
        TYPE_RANDOM,    //< random generated matrix
        TYPE_STREAM     //< from standard input stream
    };

    /** Template definitions
     */
    static const uint32 randRange = 100;
    static const uint32 randRangeHalf = (randRange / 2);

    /** Default constructor
     */
    Matrix() : matrix_(null), rows_(0), cols_(0), rand_() {

    }

    /** Constructor
     * @param n the size of rows
     * @param m the size of columns
     * @param type the type of initialization
     */
    Matrix(uint32 n, uint32 m, enum Type type = TYPE_ZERO) : matrix_(null), rows_(0), cols_(0), rand_() {
		create(n, m, type);
    }

	Matrix(uint32 n, uint32 m, T* vector) : matrix_(null), rows_(n), cols_(m), rand_() {
		create(n, m, vector);
	}

    /** Constructor
     * @param matrix the matrix to be copied
     */
    Matrix(const Matrix<T>& matrix) : matrix_(null), rows_(0), cols_(0), rand_() {
        *this = matrix;
    }

    /** Destructor
     */
    virtual ~Matrix() {
        destroy();
    }

    /** The rows size
     * @return the current size of the rows
     */
    uint32 get_rows() const {
        return rows_;
    }

    /** The columns size
     * @return the current size of the columns
     */
    uint32 get_cols() const {
        return cols_;
    }

    /** The pointer to the matrix
     * @return a pointer to block of memory
     */
	T* get_data() const {
		return matrix_;
	}

    /** Check if the matrix is uninitialized
     * @return true if matrix is not allocated
     */
    bool is_null() const {
        return (matrix_ == null);
    }

    bool is_consistent() const {
        for (uint32 i = 0; i < rows_; ++i) {
            for (uint32 j = 0; j < cols_; ++j) {
                // todo ..
            }
        }

        return false;
    }

	void create(uint32 n, uint32 m, enum Type type = TYPE_ZERO) {
		destroy();

		rows_ = n; cols_ = m;

        if (n > 0 && m > 0) {
             matrix_ = new T[n * m];

            if (!matrix_) {
                throw Exception("Cannot allocate memory for matrix");
            }

            switch (type) {
                case TYPE_ZERO : {
                    for (uint32 i = 0; i < n * m; ++i) {
                        matrix_[i] = T(0);
                    }
                    break;
                }

                case TYPE_IDENTITY : {
                    if (n == m) {
                        for (uint32 i = 0; i < n; ++i) {
                            for (uint32 j = 0; j < m; ++j) {
                                if (i == j) {
                                    matrix_[i * m + j] = T(1);
                                } else {
                                    matrix_[i * m + j] = T(0);
                                }
                            }
                        }
                    } else {
                        for (uint32 i = 0; i < n * m; ++i) {
                            matrix_[i] = T(0);
                        }
                    }
                    break;
                }

                case TYPE_RANDOM : {
                    for (uint32 i = 0; i < n * m; i++) {
                        matrix_[i] = T(rand_.generate() % randRange) - randRangeHalf;
                    }
                    break;
                }

                case TYPE_STREAM : {
                    std::cin >> *this;
                    break;
                }

                default : break;
            }
        }
	}

	void create(uint32 n, uint32 m, T* vector) {
		destroy();

        if (n > 0 && m > 0 && vector != null) {
             matrix_ = new T[n * m];

            if (!matrix_) {
                throw Exception("Cannot allocate memory for matrix");
            }

			for (uint32 i = 0; i < n * m; ++i) {
				matrix_[i] = vector[i];
			}

			rows_ = n; cols_ = m;
		}
	}

	void create(Matrix<T>& matrix) {
		destroy();

		if (matrix.rows_ > 0 && matrix.cols_ > 0 && matrix.matrix_ != null) {
			rows_ = matrix.rows_;
			cols_ = matrix.cols_;
			matrix_ = new T[rows_ * cols_];

            if (!matrix_) {
                throw Exception("Cannot allocate memory for matrix");
            }

			for (uint32 i = 0; i < rows_ * cols_; ++i) {
				matrix_[i] = matrix.matrix_[i];
			}
		}
	}

    /** Change the size of the matrix
     * @param n the new size of rows
     * @param m the new size of columns
     * @param type the type of initialization of new cells
     */
    void resize(const uint32 rows, const uint32 cols, enum Type type = TYPE_ZERO) {
        T* mat_ptr = matrix_;

        matrix_ = new T[rows*cols];

        if (!matrix_) {
            throw Exception("Cannot allocate memory for matrix");
        }

        switch (type) {
            case TYPE_ZERO : {
                for (uint32 i = 0; i < rows * cols; ++i) {
                    matrix_[i] = T(0);
                }
                break;
            }

            case TYPE_IDENTITY : {
                if (cols == rows) {
                    for (uint32 i = 0; i < rows; ++i) {
                        for (uint32 j = 0; j < cols; ++j) {
                            if (i == j) {
                                matrix_[i * cols + j] = T(1);
                            } else {
                                matrix_[i * cols + j] = T(0);
                            }
                        }
                    }
                } else {
                    for (uint32 i = 0; i < rows * cols; ++i) {
                        matrix_[i] = T(0);
                    }
                }
                break;
            }

            case TYPE_RANDOM : {
                for (uint32 i = 0; i < rows * cols; i++) {
                    matrix_[i] = T(rand_.generate() % randRange) - randRangeHalf;
                }
                break;
            }

            case TYPE_STREAM : {
                std::cin >> *this;
                break;
            }

            default : break;
        }

        if (mat_ptr) {
            uint32 rows_size = (rows > rows_) ? rows_ : rows;
            uint32 cols_size = (cols > cols_) ? cols_ : cols;

            for (uint32 i = 0; i < rows_size; ++i) {
                for (uint32 j = 0; j < cols_size; ++j) {
                    matrix_[i * cols + j] = mat_ptr[i * cols_ + j];
                }
            }

            rows_ = rows;
            cols_ = cols;

            delete[] mat_ptr;
            mat_ptr = null;
        }
    }

    /** Generate a submatrix from the original matrix
     * @param rows the number of rows of the new submatrix
     * @param cols the number of columns of the new submatrix
     * @param offset_x rows offset
     * @param offset_y columns offset
     * @return a copy of the new sliced matrix
     */
    Matrix<T> slice(const uint32 rows, const uint32 cols, const uint32 offset_x = 0, const uint32 offset_y = 0) {
        if ((rows - offset_x) != (cols - offset_y)) {
            throw Exception("The sliced matrix must be squared");
        }
        return Matrix<T>(*this, offset_x, offset_y, rows, cols);
    }

    /** Factorize a general matrix into upper triangular form
     * @return a copy of the upper triangular matrix
     */
    Matrix<T> upper() const {
        Matrix<T> upper_mat(rows_, cols_);
        Matrix<T> lower_mat(rows_, cols_);

        triangulate(upper_mat, lower_mat);

        return upper_mat;
    }

    // todo:
    /*Matrix<T>& upper(Matrix<T>& matrix) const {
        triangulate(matrix, matrix);

        for (uint32 i = 1; i < rows_; ++i) {
            for (uint32 j = 0; j < i; ++j) {
                matrix.matrix_[i * cols_ + j] = T(0);
            }
        }

        return matrix;
    }*/

    /** Factorize a general matrix into lower triangular form
     * @return a copy of the lower triangular matrix
     */
    Matrix<T> lower() const {
        Matrix<T> upper_mat(rows_, cols_);
        Matrix<T> lower_mat(rows_, cols_);

        triangulate(upper_mat, lower_mat);

        return lower_mat;
    }

    // todo :
    Matrix<T> solve(const Matrix<T>& coefficients) const {
        if (rows_ != coefficients.rows_) {
            throw Exception("Incompatible coefficients");
        }

        // if (det() != 0) ?

        Matrix<T> upper_mat(rows_, cols_);
        Matrix<T> lower_mat(rows_, cols_);
        Matrix<T> result(1, cols_);

        triangulate(upper_mat, lower_mat);

        for (uint32 i = (rows_ - 1); i >= 0; --i) {
            for (uint32 j = (cols_ - 1); j > i; --j) {
                //upper_mat
            }

        }

        return result;
    }

    /** Provide a minor from the original matrix
     * @param row the row to be deleted
     * @param col the column to be deleted
     * @return a copy of the minor matrix generated
     */
    Matrix<T> minor_mat(const uint32 row, const uint32 col) {
        if (row < 0 || row > rows_ || col < 0 || col > cols_) {
            throw Exception("Invalid row or column");
        }

        Matrix result(rows_ - 1, cols_ - 1);

        for (uint32 i = 0; i < (rows_ - (row >= rows_)); ++i) {
            for (uint32 j = 0; j < (cols_ - (col >= cols_)); ++j) {
                result(i - (i > row), j - (j > col)) = matrix_[i * cols_ + j];
            }
        }

        return result;
    }

    /** Swap two rows
     * @param a a row to be swapped
     * @param b the other row to be swapped
     */
    void swap_rows(const uint32& a, const uint32& b) const {
        for (uint32 i = 0; i < cols_; ++i) {
			Swap<T>(matrix_[a * cols_ + i], matrix_[b * cols_ + i]);
        }
    }

    /** Swap two columns
     * @param a a column to be swapped
     * @param b the other column to be swapped
     */
    void swap_cols(const uint32& a, const uint32& b) const {
        for (uint32 i = 0; i < rows_; ++i) {
            Swap<T>(matrix_[a + i * rows_], matrix_[b + i * rows_]);
        }
    }

    /** Transpose the matrix
     */
    Matrix<T>& transpose() {
        Matrix<T> transposed(rows_, cols_, TYPE_NULL);

        for (uint32 i = 0; i < rows_; ++i) {
            for (uint32 j = 0; j < cols_; ++j) {
                transposed.matrix_[j * rows_ + i] = matrix_[i * cols_ + j];
            }
        }

        *this = transposed;

        return *this;
    }

    /** Calculate the determinant of the matrix
     * @return the determinant
     */
    T determinant() const {
        if (rows_ != cols_ || rows_ < 1 || cols_ < 1) {
            throw Exception("The matrix must be squared");
        }
        return det();
    }

    /** Calculate the rang of the matrix
     * @return the rang
     */
    T rang() const {
        // todo ..
    }

    /** Invert the current matrix if it's possible
     * by Gauss-Jordan elimination method
     * @return the resultant inverted matrix
     */
    Matrix<T> invert() const {
        if (det() == T(0)) {
            throw Exception("Determinant of matrix is zero");
        }

        Matrix<T> result(rows_, cols_, TYPE_IDENTITY);
        Matrix<T> this_(*this);

        for (uint32 c = 0; c < cols_; ++c) {
            uint32 r;

            // check if it is consistent
            for (r = c; r < rows_ && this_.matrix_[r * cols_ + c] == 0; ++r);

            if (r != c) {
                this_.swap_rows(c, r);
                result.swap_rows(c, r);
            }

            for (r = 0; r < rows_; ++r) {
                if (r != c) {
                    if (this_.matrix_[r * cols_ + c] != T(0)) {
                        T value = T(-this_.matrix_[r * cols_ + c] / this_.matrix_[c * cols_ + c]);

                        for (uint32 s = 0; s < cols_; ++s) {
                            this_.matrix_[r * cols_ + s] += value * this_.matrix_[c * cols_ + s];
                            result.matrix_[r * cols_ + s] += value * result.matrix_[c * cols_ + s];
                        }
                    }
                } else {
                    T value = this_.matrix_[c * cols_ + c];
                    for (uint32 s = 0; s < cols_; ++s) {
                        this_.matrix_[r * cols_ + s] /= value;
                        result.matrix_[r * cols_ + s] /= value;
                    }
                }
            }
        }

        return result;
    }


    // todo: jacobi, gauss seidel

    /** Overloaded ostream operator
     * @param stream the output buffer
     * @param matrix the matrix to be streamed
     * @return a reference to the output stream
     */
    friend std::ostream& operator<<(std::ostream& stream, const Matrix<T>& matrix) {
        for (uint32 i = 0; i < matrix.rows_; ++i) {
            for (uint32 j = 0; j < matrix.cols_; ++j) {
                stream << matrix.matrix_[i * matrix.cols_ + j] << "  ";
            }
            stream << std::endl;
        }
        return stream;
    }

    /** Overloaded istream operator
     * @param stream the iutput buffer
     * @param matrix the matrix to be streamed
     * @return a reference to the input stream
     */
    friend std::istream& operator>>(std::istream& stream, const Matrix<T>& matrix) {
        for (uint32 i = 0; i < matrix.rows_; ++i) {
            for (uint32 j = 0; j < matrix.cols_; ++j) {
                stream >> matrix.matrix_[i * matrix.cols_ + j]; // todo: improve that
            }
        }
        return stream;
    }

    /** Overloaded assignment operator
     * @param matrix the matrix to be copied
     * @return a reference to the current matrix object
     */
    Matrix<T>& operator=(const Matrix<T>& matrix) {
		if (&matrix != this) {
			// delete if matrix have different size
			if (rows_ != matrix.rows_ || cols_ != matrix.cols_) {
				destroy();
			}

			// copy attributes
			rows_ = matrix.rows_;
			cols_ = matrix.cols_;

			// only create if it is deleted
			if (!matrix_) {
				matrix_ = new T[rows_ * cols_];

				if (!matrix_) {
					throw Exception("Cannot allocate memory for matrix");
				}
			}

			// copy it
			for (uint32 i = 0; i < cols_ * rows_; ++i) {
				matrix_[i] = matrix.matrix_[i];
			}
		}

        return *this;
    }

    /** Overloaded plus operator
     * @param matrix the matrix to be added
     * @return a copy of the resultant matrix
     */
    Matrix<T> operator+(const Matrix<T>& matrix) const {
        if (rows_ != matrix.rows_ || cols_ != matrix.cols_) {
            throw Exception("Matrix size must be equal");
        }
        Matrix<T> result(matrix);
        for (uint32 i = 0; i < rows_ * cols_; ++i) {
            result.matrix_[i] += matrix_[i];
        }
        return result;
    }

    /** Overloaded minus operator
     * @param matrix the matrix to be subtracted
     * @return a copy of the resultant matrix
     */
    Matrix<T> operator-(const Matrix<T>& matrix) const {
        if (rows_ != matrix.rows_ || cols_ != matrix.cols_) {
            throw Exception("Matrix size must be equal");
        }
        Matrix<T> result(rows_, cols_, TYPE_NULL);
        for (uint32 i = 0; i < rows_ * cols_; ++i) {
            result.matrix_[i] = matrix_[i] - matrix.matrix_[i];
        }
        return result;
    }

    /** Overloaded multiply operator
     * @param value
     * @return a copy of the resultant matrix
     */
    Matrix<T> operator*(const T value) const {
        Matrix<T> result(*this);

        for (uint32 i = 0; i < rows_ * cols_; ++i) {
            result.matrix_[i] *= value;
        }

        return result;
    }

    /** Overloaded multiply operator
     * @param matrix the matrix to be multiplied
     * @return a copy of the resultant matrix
     */
    Matrix<T> operator*(const Matrix<T>& matrix) const {
        if (cols_ != matrix.rows_) {
            throw Exception("Invalid size");
        }

        Matrix<T> result(rows_, matrix.cols_, TYPE_NULL);

        for (uint32 i = 0; i < result.rows_; ++i) {
            for (uint32 j = 0; j < result.cols_; ++j) {
                T value = T(0);

                for (uint32 k = 0; k < cols_; ++k) {
                    value += matrix_[i * cols_ + k] * matrix.matrix_[k * matrix.cols_ + j];
                }

                result.matrix_[i * result.cols_ + j] = value;
            }
        }

        return result;
    }

    friend Matrix<T> operator*(const T value, const Matrix<T>& matrix) {
        return (matrix * value);
    }

    Matrix<T> operator/(const T value) const {
        if (value == T(0)) {
            throw Exception("Invalid zero division");
        }

        Matrix<T> result(*this);

        for (uint32 i = 0; i < rows_ * cols_; ++i) {
            result.matrix_[i] /= value;
        }

        return result;
    }

    Matrix<T> operator/(const Matrix<T>& matrix) const {
        if (matrix.determinant() == 0) {
            throw Exception("Matrix isn't invertible");
        }
        return (operator*(matrix.invert()));
    }

    // todo: operators += -= *= ..

	T& operator()(const uint32 i, const uint32 j) const {
		return matrix_[i * cols_ + j];
	}

	T& operator()(const uint32 i, const uint32 j) {
		return matrix_[i * cols_ + j];
	}

	void to_buffer(Buffer& buffer) {
		uint32 cur_pos = 0;
		uint32 buffer_size = (sizeof(uint32) * 2) + (sizeof(T) * rows_ * cols_);

		if (!(buffer.get_data() != null && buffer.get_size() == buffer_size)) {
			buffer.create(buffer_size);
		}

		buffer.set_data(static_cast<void*>(&rows_), sizeof(uint32), cur_pos++);
		buffer.set_data(static_cast<void*>(&cols_), sizeof(uint32), cur_pos++);

		for (uint32 i = 0; cur_pos < buffer.get_size(); ++i) {
			buffer.set_data(static_cast<void*>(&matrix_[i]), sizeof(T), cur_pos);
			cur_pos += sizeof(T);
		}
	}

  protected:
    Matrix(Matrix<T>& matrix, const uint32 x, const uint32 y, const uint32 w, const uint32 h) : matrix_(null), rows_(w - x), cols_(h - y) {
        matrix_ = new T[rows_ * cols_];

        if (!matrix_) {
            throw Exception("Cannot allocate memory for matrix");
        }

        for (uint32 i = x; i < w; ++i) {
            for (uint32 j = y; j < h; ++j) {
                matrix_[(i - x) * cols_ + (j - y)] = matrix.matrix_[i * matrix.cols_ + j];
            }
        }
    }

    /** Implement Doolittle's method to decompose the
     * matrix into a unit lower/upper triangular matrix
     * @param upper_mat the upper triangular matrix resulting
     * @param lower_mat the lower triangular matrix resulting
     * @return a reference to the current matrix object
     */
    void triangulate(Matrix<T>& upper_mat, Matrix<T>& lower_mat) const {
        if (rows_ != cols_ || rows_ < 1 || cols_ < 1) {
            throw Exception("The matrix must be squared");
        }

        for (uint32 i = 0; i < rows_; ++i) {
            for (uint32 j = i; j < rows_; ++j) {
                upper_mat.matrix_[i * cols_ + j] = matrix_[i * cols_ + j];
                for (uint32 k = 0; k < i; ++k) {
                    upper_mat.matrix_[i * cols_ + j] += ((-lower_mat.matrix_[i * cols_ + k]) * upper_mat.matrix_[k * cols_ + j]);
                }
            }

            for (uint32 j = i; j < rows_; ++j) {
                lower_mat.matrix_[j * cols_ + i] = matrix_[j * cols_ + i];
                for (uint32 k = 0; k < i; ++k) {
                    lower_mat.matrix_[j * cols_ + i] += ((-lower_mat.matrix_[j * cols_ + k]) * upper_mat.matrix_[k * cols_ + i]);
                }

                // prevent zero division
                if (upper_mat.matrix_[i * cols_ + i] != T(0)) {
                    lower_mat.matrix_[j * cols_ + i] /= upper_mat.matrix_[i * cols_ + i];
                } else {
                    lower_mat.matrix_[j * cols_ + i] = T(0);
                }
            }

        }
    }

    T det() const {
        Matrix<T> upper_mat = upper();
        T result = T(1);

        for (uint32 i = 0; i < rows_; ++i) {
            result *= upper_mat.matrix_[i * cols_ + i];
        }

        return result;
    }

    void destroy() {
        if (matrix_) {
            delete[] matrix_;
            matrix_ = null;
        }
    }

    // Member data
    T* matrix_;			//< memory block
    uint32 rows_;		//< number of rows
    uint32 cols_;		//< number of columns
	Randomizer rand_;	//< random number generator
};

typedef Matrix<float64>	Matrixd;
typedef Matrix<float32>	Matrixf;
typedef Matrix<int32>	Matrixi;
typedef Matrix<uint32>	Matrixui;

#endif // MATRIX_HPP
