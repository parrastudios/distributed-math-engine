/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef CALLBACK_HPP
#define CALLBACK_HPP

/**
 * Headers
 */
#include <Types.hpp>

template <typename class_type, typename return_type, typename param0 = void, typename param1 = void,
													 typename param2 = void, typename param3 = void>
class Callback {
  public:
	typedef return_type (class_type::*function)(param0, param1, param2, param3);

	Callback() : object_(null), func_(null) {

	}
   
	Callback(class_type* object, function func) : object_(object), func_(func) {
	
	}

	Callback(const Callback<class_type, return_type, param0, param1, param2, param3>& callback) {
		*this = callback;
	}

	virtual ~Callback() {

	}

	return_type operator()(param0 p0, param1 p1, param2 p2, param3 p3) {
		return execute(p0, p1, p2, p3);
	}

	return_type execute(param0 p0, param1 p1, param2 p2, param3 p3) {
		return (object_->*func_)(p0, p1, p2, p3);
	}

	bool operator!() const {
		return (func_ == null);
	}

	bool operator==(const Callback<class_type, return_type, param0, param1, param2, param3>& callback) const {
		return ((object_ == callback.object_) && (func_ == callback.func_));
	}

	bool operator!=(const Callback<class_type, return_type, param0, param1, param2, param3>& callback) const {
		return ((object_ != callback.object_) || (func_ != callback.func_));
	}

	Callback<class_type, return_type, param0, param1, param2, param3>& operator=(const Callback<class_type, return_type, param0, param1, param2, param3>& callback) {
		if (&callback != this) {
			object_ = callback.object_;
			func_ = callback.func_;
		}
		return *this;
	}

  protected:
	class_type* object_;
	function func_;
};

template <typename class_type, typename return_type, typename param0, typename param1,
													 typename param2>
class Callback<class_type, return_type, param0, param1, param2> {
  public:
	typedef return_type (class_type::*function)(param0, param1, param2);

	Callback() : object_(null), func_(null) {

	}

	Callback(class_type* object, function func) : object_(object), func_(func) {
	
	}

	Callback(const Callback<class_type, return_type, param0, param1, param2>& callback) {
		*this = callback;
	}

	virtual ~Callback() {

	}

	return_type operator()(param0 p0, param1 p1, param2 p2) {
		return execute(p0, p1, p2);
	}

	return_type execute(param0 p0, param1 p1, param2 p2) {
		return (object_->*func_)(p0, p1, p2);
	}

	bool operator!() const {
		return (func_ == null);
	}

	bool operator==(const Callback<class_type, return_type, param0, param1, param2>& callback) const {
		return ((object_ == callback.object_) && (func_ == callback.func_));
	}

	bool operator!=(const Callback<class_type, return_type, param0, param1, param2>& callback) const {
		return ((object_ != callback.object_) || (func_ != callback.func_));
	}

	Callback<class_type, return_type, param0, param1, param2>& operator=(const Callback<class_type, return_type, param0, param1, param2>& callback) {
		if (&callback != this) {
			object_ = callback.object_;
			func_ = callback.func_;
		}
		return *this;
	}

  protected:
	class_type* object_;
	function func_;
};

template <typename class_type, typename return_type, typename param0, typename param1>
class Callback<class_type, return_type, param0, param1> {
  public:
	typedef return_type (class_type::*function)(param0, param1);

	Callback() : object_(null), func_(null) {

	}

	Callback(class_type* object, function func) : object_(object), func_(func) {
	
	}

	Callback(const Callback<class_type, return_type, param0, param1>& callback) {
		*this = callback;
	}

	virtual ~Callback() {

	}

	return_type operator()(param0 p0, param1 p1) {
		return execute(p0, p1);
	}

	return_type execute(param0 p0, param1 p1) {
		return (object_->*func_)(p0, p1);
	}

	bool operator!() const {
		return (func_ == null);
	}

	bool operator==(const Callback<class_type, return_type, param0, param1>& callback) const {
		return ((object_ == callback.object_) && (func_ == callback.func_));
	}

	bool operator!=(const Callback<class_type, return_type, param0, param1>& callback) const {
		return ((object_ != callback.object_) || (func_ != callback.func_));
	}

	Callback<class_type, return_type, param0, param1>& operator=(const Callback<class_type, return_type, param0, param1>& callback) {
		if (&callback != this) {
			object_ = callback.object_;
			func_ = callback.func_;
		}
		return *this;
	}

  protected:
	class_type* object_;
	function func_;
};

template <typename class_type, typename return_type, typename param>
class Callback<class_type, return_type, param> {
  public:
	typedef return_type (class_type::*function)(param);

	Callback() : object_(null), func_(null) {

	}

	Callback(class_type* object, function func) : object_(object), func_(func) {
	
	}

	Callback(const Callback<class_type, return_type, param>& callback) {
		*this = callback;
	}

	virtual ~Callback() {

	}

	return_type operator()(param p) {
		return execute(p);
	}

	return_type execute(param p) {
		return (object_->*func_)(p);
	}

	bool operator!() const {
		return (func_ == null);
	}

	bool operator==(const Callback<class_type, return_type, param>& callback) const {
		return ((object_ == callback.object_) && (func_ == callback.func_));
	}

	bool operator!=(const Callback<class_type, return_type, param>& callback) const {
		return ((object_ != callback.object_) || (func_ != callback.func_));
	}

	Callback<class_type, return_type, param>& operator=(const Callback<class_type, return_type, param>& callback) {
		if (&callback != this) {
			object_ = callback.object_;
			func_ = callback.func_;
		}
		return *this;
	}

  protected:
	class_type* object_;
	function func_;
};

template <typename class_type, typename return_type>
class Callback<class_type, return_type> {
  public:
	typedef return_type (class_type::*function)(void);

	Callback() : object_(null), func_(null) {

	}

	Callback(class_type* object, function func) : object_(object), func_(func) {
	
	}

	Callback(const Callback<class_type, return_type>& callback) {
		*this = callback;
	}

	virtual ~Callback() {

	}

	return_type operator()() {
		return execute();
	}

	return_type execute() {
		return (object_->*func_)();
	}

	bool operator!() const {
		return (func_ == null);
	}

	bool operator==(const Callback<class_type, return_type>& callback) const {
		return ((object_ == callback.object_) && (func_ == callback.func_));
	}

	bool operator!=(const Callback<class_type, return_type>& callback) const {
		return ((object_ != callback.object_) || (func_ != callback.func_));
	}

	Callback<class_type, return_type>& operator=(const Callback<class_type, return_type>& callback) {
		if (&callback != this) {
			object_ = callback.object_;
			func_ = callback.func_;
		}
		return *this;
	}

  protected:
	class_type* object_;
	function func_;
};

#endif // CALLBACK_HPP

