/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <Types.hpp>

#include <iostream> // Stream.hpp

namespace priv {
    const uint32 QUEUE_SIZE_DEFAULT = 64;
    const uint32 QUEUE_EMPTY        =  0;
};

template <typename T, uint32 size = priv::QUEUE_SIZE_DEFAULT>
class Queue {
  public:
    Queue() : head_(0), count_(0) {

    }

    ~Queue() {
        //delete[] data_;
    }

    void push(T value) {
        if (!full()) {
            data_[((head_ + count_) % size)] = value;
            count_++;
        }
    }

    T pop() {
        if (!empty()) {
            uint32 element = head_;
            head_ = (head_ + 1) % size;
            count_--;
            return data_[element];
        }

        return T(0);
    }

    bool full() {
        return (count_ == size);
    }

    bool empty() {
        return (count_ == priv::QUEUE_EMPTY);
    }

    /** Overloaded ostream operator
     * @param stream the output buffer
     * @param queue the queue to be streamed
     * @return a reference to the output stream
     */
    friend std::ostream& operator<<(std::ostream& stream, const Queue<T, size>& queue) {
        if (!queue.empty()) {
            for (uint32 i = 0; i < queue.count_; ++i) {
                stream << queue.data_[(queue.head_ + i) % size] << "  ";
            }
        }
        stream << std::endl;
        return stream;
    }

    /** Overloaded istream operator
     * @param stream the iutput buffer
     * @param queue the queue to be streamed
     * @return a reference to the input stream
     */
    friend std::istream& operator>>(std::istream& stream, Queue<T, size>& queue) {
        T value = T(0);
        stream >> value;
        queue.push(value);
        while (stream.get() != '\n' && !queue.full()) {
            stream >> value;
            queue.push(value);
        }
        return stream;
    }

  protected:
    T       data_[size];
    int32   head_;
    uint32  count_;
};

#endif // QUEUE_HPP
