/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <Types.hpp>
#include <Swap.hpp>

template <typename T>
class Vector {
  public:
	typedef T* Iterator;
	typedef T type;
	typedef uint32 size_type;

    Vector() : data_(null), size_(0), capacity_(0) {

	}

	Vector(uint32 size) : data_(null), size_(0), capacity_(size) {
		if (capacity_ > 0) {
			data_ = new T[capacity_];
		}
	}

	Vector(uint32 size, const T& value) : data_(null), size_(0), capacity_(size) {
		if (capacity_ > 0) {
			data_ = new T[capacity_];
			for (uint32 i = 0; i < capacity_; ++i) {
				data_[i] = value;
			}
		}
	}

	Vector(T* vector, uint32 size) : data_(null), size_(size), capacity_(size) {
		if (vector && size > 0) {
			data_ = new T[capacity_];

			for (uint32 i = 0; i < size; ++i) {
				data_[i] = vector[i];
			}
		}
	}

	Vector(const Vector<T>& vector) : data_(null), size_(0), capacity_(0) {
		copy(vector);
	}

	virtual ~Vector() {
		clear();
	}

	void clear() {
		if (data_ != null) {
			delete[] data_;
			data_ = null;
		}

		size_ = 0;
		capacity_ = 0;
	}

	bool empty() const {
		return (size_ == 0);
	}

	bool full() const {
		return (size_ == capacity_);
	}

	uint32 size() const {
		return size_;
	}

	uint32 capacity() const {
		return capacity_;
	}

	T& front() {
		return data_[0];
	}

	T& back() {
		return data_[size_ - 1];
	}

	Iterator begin() {
		return data_;
	}

	Iterator end() {
		return data_ + size();
	}

	void swap(Vector<T>& vector) {
		if (size_ == vector.size_) {
			for (uint32 i = 0; i < size_; ++i) {
				Swap<T>(data_[i], vector.data_[i]);
			}
		}
	}

	void push_back(const T& value) {
		if (size_ >= capacity_) {
			resize();
		}

		data_[size_++] = value;
	}

	T& pop_back() {
		T value = T();

		if (size_ > 0) {
			value = data_[--size_];
			data_[size_] = T();
		}

		return value;
	}

	void resize(uint32 size = 0) {
		if (size <= capacity_) {
			return;
		} else if (size == 0) {
			if (capacity_ <= 1) {
				size = 2;
			} else {
				// todo: optimization

				// template <typename T> 
				// inline bool is_power_of_two(const T& value) { return ((value != 0) && ((value & (~(value) + 1)) == value)); }
				
				// template <typename T>
				// inline bool next_power_of_two(const T& value) { ... }

				//if (sizeof(T) <= sizeof(uint64) && is_power_of_two<T>(sizeof(T))) {
				//	size <<= 1;
				//} else {
					size = capacity_ + (capacity_ / 2);
				//}
			}
		}

		if (data_ != null) {
			if (size_ > 0) {
				T* temp_ptr = data_;

				data_ = new T[size];

				for (uint32 i = 0; i < size_; ++i) {
					data_[i] = temp_ptr[i];
				}

				delete[] temp_ptr;
			} else {
				delete[] data_;
				data_ = new T[size];
			}
		} else {
			data_ = new T[size];
		}

		capacity_ = size;
	}

    T operator[](uint32 index) const {
		return data_[index];
	}

    T& operator[](uint32 index) {
		return data_[index];
	}

	Vector<T>& append(const Vector<T>& vector) {
		// check if there is enough space
		if (vector.size_ > (capacity_ - size_)) {
			resize(vector.size_ + size_);
		}

		for (uint32 i = 0; i < vector.size_; ++i) {
			data_[size_ + i] = vector.data_[i];
		}

		size_ += vector.size_;

		return *this;
	}

	void erase(uint32 from, uint32 size = 1) {
		if (from < capacity_ && size >= 1) {
			// todo
		}
	}

	Vector<T>& operator=(const Vector<T>& vector) {
		// avoid self-assignement
		if (&vector != this) {
			clear();
			copy(vector);
		}

		return *this;
	}

  protected:

	void copy(const Vector<T>& vector) {
		size_ = vector.size_;
		capacity_ = vector.capacity_;
		data_ = new T[capacity_];

		for (uint32 i = 0; i < size_; ++i) {
			data_[i] = vector.data_[i];
		}
	}

	T*		data_;		//< pointer to vector data
    uint32	size_;		//< current size of vector
    uint32	capacity_;	//< total allocated storage for vector
};

/**
 * Swap template function for vectors
 */
template <typename T>
void Swap(const Vector<T>& a, const Vector<T>& b) {
	a.swap(b);
}

typedef Vector<float64>	Vectord;
typedef Vector<float32>	Vectorf;
typedef Vector<int32>	Vectori;
typedef Vector<uint32>	Vectorui;

#endif // VECTOR_HPP
