/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef DEBUG_HPP
#define DEBUG_HPP

/**
 * Headers
 */
#include <iostream> // Stream.hpp ?

/**
 * Define a portable debug macro
 */
#define DEBUG_MODE		0x00
#define RELEASE_MODE	0x01

#if defined(_DEBUG) || defined(DEBUG) || !defined(NDEBUG) || defined(__DEBUG) || defined(__DEBUG__)
#	define COMPILE_TYPE DEBUG_MODE
#else
#	define COMPILE_TYPE RELEASE_MODE
#endif

/**
 * Define the debug levels
 */
enum {
	DEBUG_LEVEL_DEFAULT	= 0x00,	// Debug messages
	DEBUG_LEVEL_INFO	= 0x01, // Output information debug 
	DEBUG_LEVEL_WARNING	= 0x02,	// Warning level
	DEBUG_LEVEL_ERROR	= 0x03	// Error
};

/**
 * Define a custom static assert macro
 */
#ifndef static_assert
#	if COMPILE_TYPE == DEBUG_MODE
#		define assert_paste_(a, b) a##b
#		define assert_paste(a, b) assert_paste_(a, b)
#		define assert_message(file) assert_paste(assert_paste(assertion_failed_, assert_paste(file, _)), __LINE__)

		// Compile time assert
#		ifdef __cplusplus
			template<bool> struct static_assert_template;
			template<> struct static_assert_template<true> {};

#			define assert_line(predicate, file) \
				static static_assert_template<static_cast<bool>(predicate) != false> assert_message(file)
#		else
#			define assert_line(predicate, file) \
				typedef char assert_message(file)[2 * !!(predicate) - 1]
#		endif

#		if defined __COUNTER__
#			define static_assert(predicate) assert_line(predicate, __COUNTER__)
#		elif defined __func__
#			define static_assert(predicate) assert_line(predicate, __func__)
#		elif defined __PRETTY_FUNCTION__
#			define static_assert(predicate) assert_line(predicate, __PRETTY_FUNCTION__)
#		elif defined __FUNCTION__
#			define static_assert(predicate) assert_line(predicate, __FUNCTION__)
#		else
#			define static_assert(predicate) assert_line(predicate, 0)
#		endif
#	else
#		define static_assert(predicate)
#	endif
#endif

/**
 * Define a custom dynamic assert macro
 */
#ifndef dynamic_assert
#	if COMPILE_TYPE == DEBUG_MODE
		namespace priv {
			static void assert_(const char* predicate, const char* file, int line) {
				std::cout << "assertion_failed_" << file << "_" << line << ": " << predicate << std::endl;
			}
		}
#		define dynamic_assert(predicate) (void)((predicate) || (priv::assert_(#predicate, __FILE__, __LINE__), 0))
#	else
#		define dynamic_assert(predicate)
#	endif
#endif

#endif // DEBUG_HPP
