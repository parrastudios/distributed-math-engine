/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef THREADLOCALPTR_HPP
#define THREADLOCALPTR_HPP

#include <ThreadLocal.hpp>

template <typename T>
class ThreadLocalPtr : ThreadLocal {
  public:
    ThreadLocalPtr(T* value = null) : ThreadLocal(value) {

    }

    T& operator*() const {
        return *static_cast<T*>(get_value());
    }

    T* operator->() const {
        return static_cast<T*>(get_value());
    }

    operator T*() const {
        return static_cast<T*>(get_value());
    }

    ThreadLocalPtr<T>& operator=(T* value) {
		if (get_value() != value) {
			set_value(value);
		}
        return *this;
    }

    ThreadLocalPtr<T>& operator=(const ThreadLocalPtr<T>& thread_local_ptr) {
		if (&thread_local_ptr != this) {
			set_value(thread_local_ptr.get_value());
		}
        return *this;
    }
};

#endif // THREADLOCALPTR_HPP
