/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef STACK_HPP
#define STACK_HPP

#include <iostream> // Stream.hpp

namespace priv {
    const uint32 STACK_SIZE_DEFAULT = 64;
    const int32  STACK_EMPTY        = -1;
};

template <typename T, uint32 size = priv::STACK_SIZE_DEFAULT>
class Stack {
  public:
    Stack() : top_(priv::STACK_EMPTY) {

    }

    ~Stack() {

    }

    void push(T value) {
        if (!full()) {
            data_[++top_] = value;
        }
    }

    T pop() {
        if (!empty()) {
            return data_[top_--];
        }

        return T(0);
    }

    bool full() {
        return ((top_ + 1) == size);
    }

    bool empty() {
        return (top_ == priv::STACK_EMPTY);
    }

    /** Overloaded ostream operator
     * @param stream the output buffer
     * @param stack the stack to be streamed
     * @return a reference to the output stream
     */
    friend std::ostream& operator<<(std::ostream& stream, Stack<T, size>& stack) {
        if (!stack.empty()) {
            for (uint32 i = 0; i < static_cast<uint32>(stack.top_ + 1); ++i) {
                stream << stack.data_[stack.top_ - i] << "  ";
            }
        }
        stream << std::endl;
        return stream;
    }

    /** Overloaded istream operator
     * @param stream the iutput buffer
     * @param stack the stack to be streamed
     * @return a reference to the input stream
     */
    friend std::istream& operator>>(std::istream& stream, Stack<T, size>& stack) {
        T value = T(0);
        stream >> value;
        stack.push(value);
        while (!stream.get() != '\n' && !stack.full()) {
            stream >> value;
            stack.push(value);
        }
        return stream;
    }

  protected:
    T       data_[size];
    int32   top_;
};

#endif // STACK_HPP
