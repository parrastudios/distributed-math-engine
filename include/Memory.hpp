/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef MEMORY_HPP
#define MEMORY_HPP

/**
 * Headers
 */
#include <Types.hpp>
#include <NonCopyable.hpp>
#include <TrackPoint.hpp>

/**
 * Standard memory heap headers (C/C++)
 */
#include <stdio.h>
#include <new>

/**
 *
 */

// todo

class MemoryDebugRecorder : public TrackPoint, private NonCopyable {
  public:

    /** Operator to write the context information to memory
	 * @param pointer the templated pointer to memory block
	 * @return 
     */
    template <typename T> T* operator->*(T* pointer);

  protected:
	/** Processes the allocated memory
	 * @param pointer the pointer returned by a new-expression
	 */
	void process(void* pointer);

	explicit_noncopyable(MemoryDebugRecorder);
};

namespace priv {

/** 
 * Counter class for on-exit leakage check
 */
class MemoryDebugCounter {
  public:
	MemoryDebugCounter();

	~MemoryDebugCounter();

  private:
	static uint32 count_;
};

static MemoryDebugCounter memory_debug_counter;

} // priv

#endif // MEMORY_HPP
