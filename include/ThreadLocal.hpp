/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef THREADLOCAL_HPP
#define THREADLOCAL_HPP

#include <Types.hpp>
#include <NonCopyable.hpp>

namespace priv {
    class ThreadLocalImpl;
}

class ThreadLocal : private NonCopyable {
  public:
    ThreadLocal(void* value = null);

    ~ThreadLocal();

    void set_value(void* value);

    void* get_value() const;

  private:

    /**
     * Define portable thread local variable handle
     */
    priv::ThreadLocalImpl* handle_;
};

#endif // THREADLOCAL_HPP
