/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef CONFIG_HPP
#define CONFIG_HPP

/**
 * Project definitions
 */
#define APPLICATION_NAME			"Distributed Math Engine"
#define APPLICATION_VERSION			"0.1.11"
#define APPLICATION_MAJOR_VERSION	0x00
#define APPLICATION_MINOR_VERSION	0x01
#define APPLICATION_MICRO_VERSION 	0x0B // Beta
#define APPLICATION_VERSION_HEX		((APPLICATION_MAJOR_VERSION << 16)	| \
									 (APPLICATION_MINOR_VERSION << 8)	| \
									 (APPLICATION_MICRO_VERSION))

#endif // CONFIG_HPP
