/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef DIFFERENTIAL_EQUATION_HPP
#define DIFFERENTIAL_EQUATION_HPP

/**
 * Headers
 */
#include <Vector.hpp>

/** Differential Equation
 *
 * A template to process differentian equations to solve discrete systems.
 *
 * @see Vector
 *
 */

template <typename T>
class DifferentialEquation {
  public:

    /** Default constructor
     */
	  DifferentialEquation(Vector<T>& u_coef, Vector<T>& y_coef) : y_(), u_pattern_(), u_coefficients_(u_coef),
																   y_coefficients_(y_coef), delay_(0), u_(), iterations_(0) {

    }

    /** Constructor
     * @param u_pattern the input signal pattern
     * @param delay the discrete system delay (time until is initialized)
     */
    DifferentialEquation(Vector<T>& u_coef, Vector<T>& y_coef, Vector<T>& u_pattern, uint32 delay) : y_(delay),
		u_pattern_(u_pattern), u_coefficients_(u_coef), y_coefficients_(y_coef), delay_(delay), u_(), iterations_(0) {

		// initialize y coeficients
		for (uint32 i = 0; i < delay_; ++i) {
			y_.push_back(T(0));
		}
    }

    /** Destructor
     */
    virtual ~DifferentialEquation() {

    }

    /** The discrete system delay
     * @return the current delay
     */
    uint32 get_delay() const {
        return delay_;
    }

    /** The discrete system u pattern
     * @return the current input signal pattern
     */
    const Vector<T>& get_signal() const {
        return u_pattern_;
    }

    /** The result of the discrete system
     * @return the result no matter if it is computed or not
     */
	const Vector<T>& get_result() const {
		return y_;
	}

	/** Set the u coefficients
     * @param u_coefficients The coefficients will be used to solve the discrete system
     */
	void set_u_coefficients(Vector<T>& u_coefficients) {
		u_coefficients_ = u_coefficients;
	}

	/** Set the y coefficients
     * @param y_coefficients The coefficients will be used to solve the discrete system
     */
	void set_y_coefficients(Vector<T>& y_coefficients) {
		y_coefficients_ = y_coefficients;
	}

	/** Set the u pattern
     * @param u_pattern The current input signal pattern
     */
	void set_signal_pattern(Vector<T>& u_pattern) {
		u_pattern_ = u_pattern;
	}
	
	/** Compute result of the discrete system
     */
	void solve(uint32 iterations) {
		iterations_ = iterations;

		// allocate u vector
		u_.resize(iterations_);

		// calculate u coefficients
		for (uint32 i = 0; i < iterations_; ++i) {
			u_.push_back(u_pattern_[i % u_pattern_.size()]);
		}

		// allocate y vector
		y_.resize(iterations_);

		for (uint32 i = delay_; i < iterations_; ++i) {
			T result = T(0);

			// calculate u coefficients
			for (uint32 j = 0; j < u_coefficients_.size(); ++j) {
				int32 coef_k = (i - j);

				if (u_coefficients_[j] != T(0) && coef_k >= 0) {
					result += u_coefficients_[j] * u_[coef_k];
				}
			}

			// calculate y coefficients
			for (uint32 j = 0; j < y_coefficients_.size(); ++j) {
				int32 coef_k = (i - j);

				if (y_coefficients_[j] != T(0) && coef_k >= 0) {
					result += y_coefficients_[j] * y_[coef_k];
				}
			}

			// set the result
			y_.push_back(result);
		}
	}

    /** Overloaded ostream operator
     * @param stream the output buffer
     * @param differential_equation the equation to be streamed
     * @return a reference to the output stream
     */
    friend std::ostream& operator<<(std::ostream& stream, const DifferentialEquation<T>& differential_equation) {
		// print header
		stream << "k\t|\tu(k)\t|\ty(k)" << std::endl;
		stream << "--------------------------------------------" << std::endl;

		// print data
        for (uint32 i = 0; i < differential_equation.iterations_; ++i) {
			stream << i << "\t|\t" << differential_equation.u_[i] << "\t|\t" << differential_equation.y_[i] << std::endl;
        }

        return stream;
    }

  protected:

    // Member data
	Vector<T>&	u_pattern_;
	Vector<T>&	u_coefficients_;
	Vector<T>&	y_coefficients_;
	Vector<T>	y_;
	uint32		delay_;
	Vector<T>	u_;
	uint32		iterations_;
};

typedef DifferentialEquation<float64>	DifferentialEquationd;
typedef DifferentialEquation<float32>	DifferentialEquationf;
typedef DifferentialEquation<int32>		DifferentialEquationi;
typedef DifferentialEquation<uint32>	DifferentialEquationui;

#endif // DIFFERENTIAL_EQUATION_HPP
