/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef LOCK_HPP
#define LOCK_HPP

/**
 * Headers
 */
#include <Mutex.hpp>

/** Lock
 *
 * A class to implement simple automatic scoped lock mutex
 *
 */
template <typename MutexPolicy = Mutex>
class ScopedLock : private NonCopyable {
  public:
    explicit ScopedLock(MutexPolicy& mutex);

    ~ScopedLock();

  protected:
    MutexPolicy& mutex_;

	explicit_noncopyable(ScopedLock);
};

/** SingleThreaded
 *
 * A template to abstract a single threaded policy
 *
 */
template <typename T, typename MutexPolicy = MutexSingleThreaded>
class SingleThreaded {
  public:
	class Lock {
	  public:
		Lock() {

		}

		explicit Lock(const SingleThreaded<T, MutexPolicy>& single_threaded_policy) {

		}

		explicit Lock(const SingleThreaded<T, MutexPolicy>* single_threaded_policy) {

		}
	};

	typedef T volatile_type;
};

/** ObjectLevelLockable
 *
 * A template to abstract a object level lockable policy
 *
 */
template <typename T, typename MutexPolicy = Mutex>
class ObjectLevelLockable {
  public:
	ObjectLevelLockable() : mutex_() {
	
	}

	ObjectLevelLockable(const ObjectLevelLockable<T, MutexPolicy>& object_level_policy) : mutex_() {
	
	}

	class Lock;
	friend class Lock;

	class Lock : private NonCopyable {
	  public:
		explicit Lock(const ObjectLevelLockable<T, MutexPolicy>& host) : host_(host) {
			host_.mutex_.lock();
		}

		explicit Lock(const ObjectLevelLockable<T, MutexPolicy>* host) : host_(*host) {
			host_.mutex_.lock();
		}

		~Lock() {
			host_.mutex_.unlock();
		}
	
	  protected:
		const ObjectLevelLockable& host_;

	  private:
		Lock();
		explicit_noncopyable(Lock);
	};

	typedef volatile T volatile_type;

  protected:
	mutable MutexPolicy mutex_;
};

/** ClassLevelLockable
 *
 * A template to abstract a class level lockable policy
 *
 */
template <typename T, typename MutexPolicy = Mutex>
class ClassLevelLockable {
  public:
	class Lock;
	friend class Lock;

	class Lock : private NonCopyable {
	  public:
		Lock() {
			dynamic_assert(initializer_.initialized());
			initializer_.static_mutex().lock();
		}

		explicit Lock(const ClassLevelLockable<T, MutexPolicy>& class_level_policy) {
			dynamic_assert(initializer_.initialized());
			initializer_.static_mutex().lock();
		}

		explicit Lock(const ClassLevelLockable<T, MutexPolicy>* class_level_policy) { 
			dynamic_assert(initializer_.initialized());
			initializer_.static_mutex().lock();
		}

		~Lock() {
			dynamic_assert(initializer_.initialized());
			initializer_.static_mutex().unlock();
		}

		explicit_noncopyable(Lock);
	};

	typedef volatile T volatile_type;


  protected:
	class Initializer {
	  public:
		Initializer() : initialized_(false), mutex_() {
			initialized_ = true;
		}

		~Initializer() {
			dynamic_assert(initialized_);
		}

		bool initialized() const {
			return initialized_;
		}

		const MutexPolicy& static_mutex() const {
			return mutex_;
		}

	  protected:
		bool initialized_;
		MutexPolicy mutex_;
	};

	static Initializer initializer_;
};

template <typename T, typename MutexPolicy>
typename ClassLevelLockable<T, MutexPolicy>::Initializer ClassLevelLockable<T, MutexPolicy>::initializer_;

// todo: all templates to Lock.inl
#include <Lock.inl>

/**
 * Sugar for basic templated scoped lock
 */
typedef ScopedLock<> Lock;

#endif // LOCK_HPP
