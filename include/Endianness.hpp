/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef ENDIANESS_HPP
#define ENDIANESS_HPP

/**
 * Headers
 */
#include <Types.hpp>
#include <Singleton.hpp>

/** Endianness
 *
 * A class to allow dynamic endianness checking.
 *
 */
class Endianness : public Singleton<Endianness> {
  public:
	static uint32 get();

	static uint16 swap_uint16(uint16 value);

	static int16 swap_int16(int16 value);

	static uint32 swap_uint32(uint32 value);

	static int32 swap_int32(int32 value);

	static int64 swap_int64(int64 value);

	static uint64 swap_uint64(uint64 value);

	template <typename T>
	static void swap_bytes(T* source);

  private:
	typedef union endianness_ {
		uint8	byte_[4];
		uint32	integer_;

		endianness_() {
			byte_[0] = 0x00; byte_[1] = 0x01; byte_[2] = 0x02; byte_[3] = 0x03;
		}
	} endianness_buffer;

	friend class Singleton<Endianness>;
};

#endif // ENDIANESS_HPP
