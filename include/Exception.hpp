/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef EXCEPTION_HPP
#define EXCEPTION_HPP

/**
 * Headers
 */
#include <String.hpp>
#include <Callback.hpp>
#include <TrackPoint.hpp>
#include <iostream> // Stream.hpp

/**
 * Forward declarations
 */
class ExceptionHandler;

/** Exception
 *
 * A basic class to handle exceptions
 *
 */

class Exception : public TrackPoint {
  public:

	/** Public class definitions
	 */
	static const uint32 EXCEPTION_INVALID;
	static const uint32 EXCEPTION_UNKNOWN;
	static const uint32 EXCEPTION_SYSTEM;

    /** Default constructor
     */
    Exception();

	/** Constructor
     * @param message the current exception message
	 * @param id the current exception identifier
	 * @param code the current exception code
	 */
	Exception(const char* message, uint32 id = EXCEPTION_UNKNOWN, uint32 code = EXCEPTION_UNKNOWN);

	/** Constructor
	 * @param message the current exception message
	 * @param handler callback to the exception manager
	 * @param id the current exception identifier
	 * @param code the current exception code
	 */
	Exception(const char* message, ExceptionHandler& handler, uint32 id = EXCEPTION_UNKNOWN, uint32 code = EXCEPTION_UNKNOWN);

    /** Copy constructor
     */
    Exception(const Exception& exception);

    /** Destructor
     */
    virtual ~Exception() throw();

    /** The current message
     * @return the internal message string
     */
    const String& get_message() const;
    
	/** The current id
     * @return the internal id number
     */
	const uint32 get_id() const;

	/** The current code
     * @return the internal code number
     */
	const uint32 get_code() const;

    /** Throw the exception
	 * @param track point where the exception was raised
	 * @return the instance of itself
     */
	const Exception& raise_impl(const TrackPoint& track) throw();

    /** Register a handler to manage the exceptions thrown
	 * @param handler the object that will handle exceptions
     */
	void register_handler(ExceptionHandler& handler);

    /** Register a new track point
	 * @param track point where the exception was raised
	 * @return the instance of itself
     */
	const Exception& register_track(const TrackPoint& track);

    /** Check if the exception has been thrown
	 * @return true if it is thrown, false otherwise
     */
	bool thrown() const;

    /** Overloaded assignment opearator
     * @param exception the exception to be copied
     * @return the reference to itself
     */
    Exception& operator=(const Exception& exception);

  protected:

	/** Gets the last error from the system
	 */
	void get_system_error();

    /** Wrapper for ostream opearator
     * @param stream the output stream
     * @return the reference to ostream object
     */
	std::ostream& print(std::ostream& stream) const;

    /** Overloaded ostream opearator
     */
	friend std::ostream& operator<<(std::ostream&, const Exception&);

	/** Callback type to exception handler
	 */
	typedef Callback<ExceptionHandler, void, const Exception&> HandlerCallback;

    /** Member data
	 */
    String				msg_;		//< message string
	uint32				id_;		//< exception id
	uint32				code_;		//< exception code
	HandlerCallback		handler_;	//< callback to the exception handler
};

/** Syntactic sugar for throw function
 */
#define raise() raise_impl(TrackPoint(__FILE__, __LINE__))

namespace priv {

inline const Exception& raise_exception_ctor(Exception& exception, const TrackPoint& track) throw() {
	return exception.register_track(track);
}

} // priv

#define raise_exception(exception) priv::raise_exception_ctor(exception, TrackPoint(__FILE__, __LINE__))
#define raise_critical_exception(exception) throw raise_exception(exception)

/** Overloaded ostream opearator for Exception
 * @param stream the output stream
 * @param exception the exception to be streamed
 * @return the reference to ostream object
 */
std::ostream& operator<<(std::ostream& stream, const Exception& exception);

/** ExceptionHandler
 *
 * A basic class interface to manage raised exceptions
 *
 */

class ExceptionHandler {
  public:
	
    /** The virtual method to manage the exceptions
     * @param exception the exception thrown
     */
	virtual void process(const Exception& exception) = 0;
};

#endif // EXCEPTION_HPP
