/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef TIME_HPP
#define TIME_HPP

/**
 * Headers
 */
#include <Types.hpp>

class Time {
  public:
	
	/** Default constructor
	 */
	Time();

	/** Default destructor
	 */
	~Time();

	/** Reestart the time counter
	 */
	void clear();

	/** Start the time counter
	 * @return the starting absolute time
	 */
	uint64 start();

	/** Restarts the time counter
	 * @return the elapsed time from previous started time
	 */
	uint64 restart();

	/** Resume the time counter (if it is paused previously)
	 */
	void resume();

	/** Stop the time counter
	 * @return time from start (0 if not started)
	 */
	uint64 stop();

	/** Get current time
	 * @return the nanoseconds elapsed
	 */
	uint64 get();

	/** Get the absolute time
	 * @return the nanoseconds elapsed from first call
	 */
	static uint64 get_absolute();

  protected:
	
    /** Member data
	 */
	uint64 start_time_;	//< start time in nanoseconds
	uint64 stop_time_;	//< stop time in nanoseconds
};

#endif // TIME_HPP
