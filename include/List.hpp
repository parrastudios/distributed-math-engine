/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef LIST_HPP
#define LIST_HPP

/**
 * Headers
 */
#include <Types.hpp>
#include <Swap.hpp>
#include <Mutex.hpp>
#include <iostream> // Stream.hpp

/**
 * Forward declarations
 */
template <typename T>
class List;

template <typename T>
class ListNode {
  public:
	inline ListNode() : data_(null), next_(null), prev_(null) {

	}

	inline explicit ListNode(const T& data) : data_(data), next_(null), prev_(null) {

	}

	inline T data() {
		return data_;
	}

	T data_;
	ListNode<T>* next_;
	ListNode<T>* prev_;

  protected:
	friend class List<T>;
};

template <typename T>
std::ostream& operator<<(std::ostream& stream, const List<T>& list);

template <typename T>
std::istream& operator>>(std::istream& stream, const List<T>& list);

template <typename T>
class List {
  public:
	inline List() : head_(null), tail_(null), size_(0) {

	}

	inline List(const List<T>& list) : head_(null), tail_(null), size_(0) {
		append(list);
	}

	inline virtual ~List() {
		clear();
	}

	inline void clear() {
		if (size_) {
			ListNode<T>* iterator;
			while (head_->next_) {
				iterator = head_->next_;
				delete head_;
				head_ = iterator;
			}
			reset();
		}
	}

	inline virtual void append(const T& value) {
		ListNode<T>* node = new ListNode<T>(value);

		if (node) {
			if (size_ == 0) {
				head_ = tail_ = node;
			} else {
				tail_->next_ = node;
				node->prev_ = tail_;
				tail_ = node;
			}

			++size_;
		}
	}

	inline virtual void append(const List<T>& list) {
		if (list.size_ > 0) {
			ListNode<T>* iterator = list.head_;
			do {
				append(iterator->data_);
				iterator = iterator->next_;
			} while (iterator);
		}
	}

	inline T& operator[](const uint32 i) {
		return get(i)->data_;
	}

	inline const T& operator()(const uint32 i) const {
		return get(i)->data_;
	}

	inline const T& back() const {
		return tail_->data_;
	}

	inline T& back() {
		return tail_->data_;
	}

	inline const T& front() const {
		return head_->data_;
	}

	inline T& front() {
		return head_->data_;
	}

	inline bool empty() const {
		return (size_ == 0);
	}

	inline uint32 size() const {
		return size_;
	}

	inline void resize(uint32 size, T value = T()) {
	    if (size_ == size) {
            return;
	    }

	    if (size > size_) {
            size -= size_;
			while (size) {
				append(value);
				--size;
			}
	    } else {
            while (size_ > size) {
                pop_back();
            }
	    }
	}

	inline uint32 count(const T& value) const {
		const ListNode<T>* iterator = head_;
		uint32 counter = 0;

		while (iterator) {
			if (iterator->data_ == value) {
				++counter;
			}
			iterator = iterator->next_;
		}
		return counter;
	}

	inline bool contains(const T& value) const {
		const ListNode<T>* iterator = head_;

		while (iterator) {
			if (iterator->data_ == value) {
				return true;
			}
			iterator = iterator->next_;
		}
		return false;
	}

	inline bool starts(const T& value) const {
		return (size_ && head_->data_ == value);
	}

	inline bool ends(const T& value) const {
		return (size_ && tail_->data_ == value);
	}

	inline uint32 index_of(const T& value, uint32 offset = 0) const {
		if (!size_ || offset >= size_) {
			return size_;
		}

		const ListNode<T>* iterator;
		uint32 counter;

		if (size_ / 2 < offset) {
			uint32 found = size_;

			iterator = tail_;
			counter = size_ - 1;
			while (offset != counter) {
				if (iterator->data_ == value) {
					found = counter;
				}
				iterator = iterator->prev_;
				--counter;
			}
			return found;
		} else {
			iterator = head_;
			counter = offset;
			while (iterator->next_) {
				if (iterator->data_ == value) {
					return counter;
				}
				++counter;
				iterator = iterator->next_;
			}
			return (size_ - 1);
		}
	}

	inline uint32 last_index(const T& value, uint32 offset = 0) const {
		if (!size_ || offset >= size_) {
			return size_;
		}

		const ListNode<T>* node = tail_;
		uint32 counter = size_ - 1;

		while (counter) {
			if (node->data_ == value) {
				return counter;
			}
			node = node->prev_;
			--counter;
		}

		return (size_ - 1);
	}

	inline void insert(uint32 position, const T& value) {
		if (!size_ || position >= size_) {
			append(value);
		} else if (!position) {
            push_front(value);
		} else {
			ListNode<T>* pnode = get(position - 1); // position node
			ListNode<T>* inode = new ListNode<T>(value); // insert node

			inode->next_ = pnode->next_;
			if (pnode->next_) {
                pnode->next_->prev_ = inode;
			}
			pnode->next_ = inode;
			inode->prev_ = pnode;

			++size_;
		}
	}

	inline void erase(uint32 position) {
		if (size_ && position < size_) {
			remove(get(position));
		}
	}

	inline void erase(uint32 from, uint32 to) {
	    if (to >= size_) {
            to = (size_ - 1);
	    }

	    if (size_ && from < to) {
	        const ListNode<T>* iterator;
	        const ListNode<T>* tmp;
	        uint32 length = to - from;

            if (from > (size_ - to)) {
                iterator = get(to);

                while (length && iterator) {
                    tmp = iterator->prev_;
                    remove(iterator);
                    iterator = tmp;
                    length--;
                }
            } else {
                iterator = get(from);

                while (length && iterator) {
                    tmp = iterator->next_;
                    remove(iterator);
                    iterator = tmp;
                    length--;
                }
            }
	    }
	}

    inline void sort() {

    }

	inline void unique() {
        // todo: two ways, sort it previously (merge sort) or with a hash map
	}

    // faster implementation : O(n/2)
	inline void reverse() {
	    if (size_ > 1) {
            ListNode<T>* left = head_;
            ListNode<T>* right = tail_;
            uint32 count = (size_ / 2);

            while (count) {
                Swap<node_type*>(left->next_, left->prev_);
                Swap<node_type*>(right->next_, right->prev_);

                left = left->prev_;
                right = right->next_;
                --count;
            }

            // don't forget the middle node
            if (size_ % 2) {
                Swap<node_type*>(left->next_, left->prev_);
            }

            Swap<node_type*>(head_, tail_);
	    }
	}

	inline void push_back(const T& value) {
		append(value);
	}

	inline void push_front(const T& value) {
		if (size_ == 0) {
			append(value);
		} else {
			ListNode<T>* node = new ListNode<T>(value);

			if (node) {
				head_->prev_ = node;
				node->next_ = head_;
				head_ = node;
				++size_;
			}
		}
	}

	inline T pop_back() {
		T element = T(0);
		if (size_ > 0) {
			element = tail_->data();
			remove(tail_);
		}
		return element;
	}

	inline T pop_front() {
		T element = T(0);
		if (size_ > 0) {
			element = head_->data();
			remove(head_);
		}
		return element;
	}

	inline void remove(const T& value) {
		const ListNode<T>* iterator = head_;
		const ListNode<T>* next;

		while (iterator) {
		    next = iterator->next_;

		    if (iterator->data_ == value) {
                remove(iterator);
		    }

		    iterator = next;
		}
	}

    inline void repleace(const uint32 position, const T& value) {
        if (size_ && position < size_) {
            node_type* node = get(position);

            if (node) {
                node->data_ = value;
            }
        }
    }

	inline void swap(uint32 i, uint32 j) {
		const ListNode<T>* inode = get(i);
		const ListNode<T>* jnode = get(j);

		Swap<node_type>(get(i), get(j));
	}

	inline List<T>& operator=(const List<T>& list) {
		if (&list != this) {
			clear();
			append(list);
		}
		return *this;
	}

	inline std::ostream& print(std::ostream& stream) const {
		const ListNode<T>* iterator = head_;

		while (iterator) {
			stream << iterator->data_;
			iterator = iterator->next_;
		}

		return stream;
	}

  protected:
	class BaseIterator {
	  public:
		BaseIterator(ListNode<T>* const node) : node_(node) {

		}

		BaseIterator(const BaseIterator& iterator) : node_(iterator.node_) {

		}

		BaseIterator& operator=(const BaseIterator& iterator) {
			if (&iterator != this) {
				node_ = iterator.node_;
			}
			return *this;
		}

		virtual ~BaseIterator() {

		}

		bool operator==(const BaseIterator& iterator) const {
			return (node_ == iterator.node_);
		}

		bool operator!=(const BaseIterator& iterator) const {
			return (node_ != iterator.node_);
		}

		BaseIterator& operator++() {
			if (node_->next_) {
				node_ = node_->next_;
			}
			return *this;
		}

		BaseIterator& operator--() {
			if (node_->prev_) {
				node_ = node_->prev_;
			}
			return *this;
		}

		BaseIterator operator++(int32) {
			BaseIterator prev(node_);
			if (node_->next_) {
				node_ = node_->next_;
			}
			return prev;
		}

		BaseIterator operator--(int32) {
			BaseIterator prev(node_);
			if (node_->prev_) {
				node_ = node_->prev_;
			}
			return prev;

		}

        BaseIterator operator+(int32 j) const {
            BaseIterator r = *this;
            if (j > 0){
                while(j-- && r.node_->next) r.node_ = r.node_->next_;
            }
            else{
                while (j++ && r.node_->previous) r.node_ = r.node_->prev_;
            }
            return r;
        }

		BaseIterator operator-(int32 j) const {
			return operator+(-j);
		}

		BaseIterator& operator+=(int32 j) {
			return *this = *this + j;
		}

		BaseIterator& operator-=(int32 j) {
			return *this = *this - j;
		}

		bool begin() {
			return !node_->prev_;
		}

		bool end() {
			return !node_->next_;
		}

		bool is_null() {
			return !node_;
		}

		friend class List<T>;
	  protected:
		BaseIterator() : node_(null) {

		}

		ListNode<T>* node_;
	};

  public:
	class Iterator : public BaseIterator {
	  public:
		Iterator(ListNode<T>* const node) : BaseIterator(node) {

		}

		Iterator(const BaseIterator& iterator) : BaseIterator(iterator.node_) {

		}

		virtual ~Iterator() {

		}

		virtual T& operator*() const {
			return BaseIterator::node_->data_;
		}

		virtual T* operator->() const {
			return &BaseIterator::node_->data_;
		}

		friend class List;
	};

	friend class Iterator;

	class ConstIterator : public BaseIterator {
	  public:
		ConstIterator(const BaseIterator &iterator) : BaseIterator(const_cast<ListNode<T>*>(iterator.node_)) {

		}

		ConstIterator(const ListNode<T>* const node) : BaseIterator(const_cast<ListNode<T>*>(node)) {

		}

		virtual ~ConstIterator() {

		}

		virtual const T& operator*() const {
			return BaseIterator::node_->data_;
		}

		virtual const T* operator->() const {
			return &BaseIterator::node_->data_;
		}
	};

	friend class ConstIterator;

	Iterator begin() {
		Iterator iterator(head_);
		return iterator;
	}

	Iterator end() {
		Iterator iterator(tail_);
		return iterator;
	}

	ConstIterator begin_const() {
		ConstIterator iterator(head_);
		return iterator;
	}

	ConstIterator end_const() {
		ConstIterator iterator(tail_);
		return iterator;
	}

	// todo and test
	Iterator erase(Iterator iterator) {
		ListNode<T>* node;

		if (!iterator.end()) {
			node = iterator.node_->next_;
		} else if (!iterator.begin()) {
			node = iterator.node_->prev_;
		} else {
			node = null;
		}

		remove(iterator.node_);

		return Iterator(node);
	}

	Iterator erase(Iterator begin, Iterator end) {
		return Iterator();
	}

	Iterator insert(Iterator before, const T& value) {
		return Iterator();
	}

	template <typename Function>
	Function for_each(Function function) {
		if (head_) {
			for (ListNode<T>* iterator = head_; iterator != null; iterator = iterator->next_) {
				function(iterator->data_);
			}
		}

		return function;
	}

	template <typename Function>
	Function for_each(Iterator first, Iterator last, Function function) {
		if (head_) {
			do {
				function(*first);
				++first;
			} while (first != last);

			function(*last);
		}

		return function;
	}

	bool find(const T& value) {
		if (head_) {
			for (ListNode<T>* iterator = head_; iterator != null; iterator = iterator->next_) {
				if (*iterator == value) {
					return true;
				}
			}
		}
		return false;
	}

	Iterator find_if(Iterator first, Iterator last, bool (*function)()) {
		while (first != last) {
			if (function(*first) == true) {
				return first;
			}

			++first;
		}
		return last;
	}

  protected:
    typedef ListNode<T> node_type;

	friend std::ostream& operator<< <>(std::ostream& stream, const List<T>& list);
	friend std::istream& operator>> <>(std::istream& stream, const List<T>& list);

    inline void remove(const ListNode<T>* node) {
        if (node) {
            if (node == head_) {
                head_ = head_->next_;
            }
            if (node == tail_) {
                tail_ = tail_->prev_;
            }

            if (node->prev_) {
                node->prev_->next_ = node->next_;
            }
            if (node->next_) {
                node->next_->prev_ = node->prev_;
            }

            delete node;

            --size_;
        }
    }

	inline void reset() {
		head_ = tail_ = null;
		size_ = 0;
	}

	inline ListNode<T>* get(uint32 position) const {
		ListNode<T>* iterator;

		if (position > ((size_ - 1) / 2)) {
		    iterator = tail_;
			position = size_ - position - 1;
			while (position) {
				iterator = iterator->prev_;
				--position;
			}
		} else {
		    iterator = head_;
			while (position) {
				iterator = iterator->next_;
				--position;
			}
		}
		return iterator;
	}

	ListNode<T>*	head_;
	ListNode<T>*	tail_;
	uint32			size_;
	//MutexPolicy		mutex_;
};

template <typename T>
std::ostream& operator<<(std::ostream& stream, const List<T>& list) {
    return list.print(stream);
}

template <typename T>
std::istream& operator>>(std::istream& stream, const List<T>& list) {
	// todo
    return stream;
}

// todo:	all inline functions should be in inl file including overloaded operators
//			convert it to thread safe with locking policies

#endif // LIST_HPP
