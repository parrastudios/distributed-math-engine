/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef USER_HPP
#define USER_HPP

#include <List.hpp>
#include <Protocol.hpp>
#include <Runnable.hpp>
#include <Thread.hpp>
#include <Mutex.hpp>
#include <MatrixManager.hpp>

class User : public ProtocolHandler, Runnable {
  public:
	User(List<User*>& list);

	virtual ~User();

	bool is_available();

	virtual void run();

	virtual void incoming_data(Packet& packet);

	virtual void outgoing_data(Packet &packet);

  protected:
	friend class Server;

	List<User*> get_available_users();

	void incoming_request(Packet& packet);

	void incoming_result(Packet& packet);

	void outgoing_request(Packet& packet);

	void outgoing_result(Packet& packet);

	Socket				socket_;
	bool				available_;
	Thread*				thread_;
	Mutex				mutex_;
	Protocol			protocol_;
	MatrixfManager		matrix_manager_;
	List<User*>&		user_list_;
	User*				user_request_;
	uint32				pending_users_;
};

#endif // USER_HPP
