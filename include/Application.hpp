/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <Config.hpp>
#include <Types.hpp>
#include <Exception.hpp>
#include <Runnable.hpp>
#include <NonCopyable.hpp>

#include <ostream> // Stream.hpp

/**
 * Application running modes
 */
enum {
	APPLICATION_CLIENT		= 0x00,
    APPLICATION_SERVER		= 0x01,

    APPLICATION_ERROR		= 0xFFFFFFFFUL
};

/** Application
 *
 * The entry point to the software
 */
class Application : public Runnable, private NonCopyable {
  public:
    Application(char* args[], int32 size);

    virtual ~Application();

	virtual void run();

    std::ostream& arguments_print_man(std::ostream& stream);

    uint32 arguments_parse();

    uint32 error_throw(Exception& exception);
  protected:
    Runnable& client();

    Runnable& server();

	char** arg_list_;
	int32  arg_size_;
	int32  app_arg_pos_;

  private:
    static const char	tab_;
	static const char*	name_;
	static const char*	ver_;

	Runnable*			app_impl_;

	explicit_noncopyable(Application);
};

#endif // APPLICATION_HPP
