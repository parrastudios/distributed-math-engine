/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef RANDOMIZER_HPP
#define RANDOMIZER_HPP

#include <Types.hpp>

/** Randomizer
 *
 * Pseudo-random number generator
 * http://en.wikipedia.org/wiki/Random_number_generation
 *
 * @assert initializer coefficients are not zero
 *
 */

class Randomizer {
  public:
    Randomizer(uint32 z_coef = 0, uint32 w_coef = 0);

	uint32 generate();

  protected:
	uint32 coef_z_;
	uint32 coef_w_;

  private:
	static const uint32 COEFFICIENT_Z_DEFAULT;
	static const uint32 COEFFICIENT_W_DEFAULT;
};

#endif // RANDOMIZER_HPP
