
// todo

namespace priv {

template <typename T>
struct AutoPtrRef {};

template <typename T>
class AutoPtr {
  public:
	typedef T type;

    explicit AutoPtr(T* pointer = null);
	
	AutoPtr(AutoPtr& autptr);
	
	template <typename U>
	AutoPtr(AutoPtr<U>& autoptr);

    AutoPtr& operator=(AutoPtr& autoptr);
    
	template <typename U>
	AutoPtr& operator=(AutoPtr<Y>& autoptr);
    
	AutoPtr& operator=(AutoPtrRef<T> ref);

    ~AutoPtr();

    T& operator*() const;

    T* operator->() const;
    
	T* get() const;
    
	T* release();
    
	void reset(T* pointer = null);

    AutoPtr(AutoPtrRef<T> ref);

    template <typename U>
	operator AutoPtrRef<U>();
    
	template <typename U>
	operator AutoPtr<U>();
};

template <typename T, typename LockingPolicy = SingleThreaded >
class SmartPointer : public LockingPolicy {
 // todo



	template <typename U, typename Policy>
	SmartPointer(const SmartPtr<U, Policy>& other) : pointer_(other.pointer_), LockingPolicy(other) {
		
	}
};


/** LockingPointer
 *
 * Locks a volatile object and casts away volatility so that
 * the object can be safely used in a single-threaded region of code
 *
 */
template <typename T, template <class> class ConstPolicy = DontPropagateConst>
class LockingPointer : private NonCopyable {
  public:
	typedef typename ConstPolicy<T>::type ConstOrNotType;

	LockingPtr(volatile ConstOrNotType& object, Mutex& mutex) : object_ptr_(const_cast<T*>(&object)), mutex_(&mutex) {
		mutex.lock();
	}
	
	~LockingPtr() {
		mutex_.unlock();
	}

	ConstOrNotType& operator*() {
		return *object_;
	}

	ConstOrNotType* operator->() {
		return object_;
	}

  protected:
	LockingPtr();

	ConstOrNotType*	object_;	//< reference to object
	Mutex*			mutex_;		//< mutex used to control thread access to object

	explicit_noncopyable(LockingPtr);
};

} // priv



/** Syntactic sugar for pointers
 */
typedef priv::AutoPointer		auto_ptr;
typedef priv::SharedPointer		shared_ptr;
typedef priv::WeakPointer		weak_ptr;
typedef priv::SmartPointer		smart_ptr;
typedef priv::UniquePointer		unique_ptr;
typedef priv::LockingPointer	locking_ptr;