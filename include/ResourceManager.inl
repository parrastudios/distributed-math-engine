/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#if !defined RESOURCEMANAGER_HPP || defined RESOURCEMANAGER_INL
#	error ResourceManager.hpp included directly
#endif

#define RESOURCEMANAGER_INL

template <typename T>
const uint32 ResourceManager<T>::INVALID_ID = 0xFFFFFFFF;

template <typename T>
ResourceManager<T>::ResourceManager() : ids_stack_(Stack<uint32>()), resource_list_(null) {
	resource_list_ = new List<T*>;
}

template <typename T>
ResourceManager<T>::~ResourceManager() {
	clear();
	delete resource_list_;
	resource_list_ = null;
}

template <typename T>
List<T*>* ResourceManager<T>::get_list() {
	return resource_list_;
}

template <typename T>
T* ResourceManager<T>::get_element() {
	return get_element(resource_list_->size() - 1);
}

template <typename T>
T* ResourceManager<T>::get_element(uint32 id) {
	if (id < resource_list_->size()) {
		return (*resource_list_)[id];
	}

	return null;
}

template <typename T>
T* ResourceManager<T>::get_element(String& name) {
	if (name.empty() || resource_list_ == null || resource_list_->empty())
		return null;

	for (class List<T*>::Iterator iterator = resource_list_->begin(); iterator != resource_list_->end(); iterator++) {
		if ((*iterator) != null && (*iterator)->get_name() == name) {
			return (*iterator);
		}
	}

	return null;
}

template <typename T>
void ResourceManager<T>::clear() {
	for (class List<T*>::Iterator iterator = resource_list_->begin();
		 iterator != resource_list_->end(); iterator++) {
		delete *iterator;
	}

	while (!ids_stack_.empty()) {
		ids_stack_.pop();
	}

	resource_list_->clear();
}

template <typename T>
void ResourceManager<T>::remove(uint32 id) {
	T* resource = null;

	if (resource_list_ == null || id >= resource_list_->size() || (*resource_list_)[id] == null)
		return;

	resource = (*resource_list_)[id];

	resource->decrement_ref_count();

	if (resource->get_ref_count() == 0) {
		ids_stack_.push(id);

		delete resource;

		(*resource_list_)[id] = null;
	}
}

template <typename T>
uint32 ResourceManager<T>::add(T* element) {
	uint32 id;
	bool ids_available;

	if (resource_list_ == null || element == null)
		return INVALID_ID;

	if (element == get_element(element->get_name())) {
		element->increment_ref_count();
		return element->get_id();
	}

	// check if there is a free handle available
	ids_available = !ids_stack_.empty();

	if (ids_available) {
		id = ids_stack_.pop();
		resource_list_->insert(id, element);
	} else {
		id = resource_list_->size();
		resource_list_->push_back(element);
	}

	return id;
}

template <typename T>
uint32 ResourceManager<T>::add(String& name) {
	uint32 id;
	bool ids_available;
	T* element = null;
	T* resource = null;

	if (resource_list_ == null || name.empty())
		return INVALID_ID;

	element = get_element(name);

	if (element != null) {
		element->increment_ref_count();
		return element->get_id();
	}

	// check if there is a free handle available
	ids_available = !ids_stack_.empty();

	if (ids_available) {
		id = ids_stack_.pop();
	} else {
		id = resource_list_->size();
	}

	resource = new T(id, name);

	if (ids_available) {
		resource_list_->insert(id, resource);
	} else {
		resource_list_->push_back(resource);
	}

	return id;
}

template <typename T>
T* ResourceManager<T>::operator[](uint32 id) {
	if (id < resource_list_.size()) {
		return (*resource_list_)[id];
	}

	return null;
}
