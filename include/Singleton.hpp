/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef SINGLETON_HPP
#define SINGLETON_HPP

#include <NonCopyable.hpp>
#include <Mutex.hpp>

/** Singleton
 *
 * Template class for only one instance classes
 *
 */
template <typename T>
class Singleton : private NonCopyable {
 public:
	static T* instance_ptr();

	static T& instance();

  protected:
	Singleton();

	virtual ~Singleton();

	static T* instance_;

	explicit_noncopyable(Singleton);
};

template <typename T>
class SmartSingleton : private NonCopyable {
  public:
	
	// todo	

	explicit_noncopyable(SmartSingleton);
};

template <typename T>
class SafeSingleton : private NonCopyable {
  public:
	
	// todo	

	explicit_noncopyable(SafeSingleton);
};

#include <Singleton.inl>

#endif // SINGLETON_HPP
