/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef CONSOLE_HPP
#define CONSOLE_HPP

/**
 * Headers
 */
#include <Types.hpp>

#include <iostream> // Stream.hpp

enum {
	CMD_MATRIX_INVERSE			= 0x00,
	CMD_MATRIX_TRIANGULATEUP	= 0x01,
	CMD_NUMERICAL_PI			= 0x02,

	CMD_EXIT					= 0xFFUL,
	CMD_ERROR					= 0xFFFFFFFFUL
};

class Console {
  public:
	Console(std::istream& input, std::ostream& output);

	virtual ~Console();

	bool is_open();

	uint32 read_command();

	void close();

	template <typename T>
	inline void get_arguments(T& args) {
		input_stream_ >> args;
	}

  protected:
	bool			is_open_;
	uint32			current_command_;
	std::istream&	input_stream_;
	std::ostream&	output_stream_;
};

#endif // CONSOLE_HPP
