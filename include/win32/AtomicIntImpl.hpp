/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef ATOMICINTIMPL_HPP
#define ATOMICINTIMPL_HPP

/**
 * Headers
 */
#include <Types.hpp>

#include <windows.h>

#if COMPILER_TYPE == COMPILER_MSVC
#	include <intrin.h>
#	pragma intrinsic (_InterlockedIncrement)
#	pragma intrinsic (_InterlockedDecrement)
#	pragma intrinsic (_InterlockedExchangeAdd)
#else
#   define _InterlockedIncrement InterlockedIncrement
#	define _InterlockedDecrement InterlockedDecrement
#	define _InterlockedExchangeAdd InterlockedExchangeAdd
#endif

namespace priv {

class AtomicIntImpl {
  public:

    AtomicIntImpl(int32 value = 0);

    virtual ~AtomicIntImpl();

	bool is_null() const;

	void increment();

	bool decrement();

	int32 get_value() const;

	void set_value(int32 value);

	int32 exchange_add(int32 value);

	void add(int32 value);

  protected:

#if COMPILER_TYPE == COMPILER_MSVC
	volatile LONG value_;
#else
    long int value_;
#endif
};

}

#endif // ATOMICINTIMPL_HPP
