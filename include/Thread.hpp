/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef THREAD_HPP
#define THREAD_HPP

#include <Types.hpp>
#include <NonCopyable.hpp>
#include <Runnable.hpp>

namespace priv {
    class  ThreadImpl;

    template <typename T>
    class ThreadFunctor : public Runnable {
	  public:
        ThreadFunctor(T functor) : functor_(functor) {}
        virtual void run() {functor_();}
        T functor_;
    };

    template <typename T, typename S>
    class ThreadFunctorWithArg : public Runnable {
	  public:
        ThreadFunctorWithArg(T function, S arg) : function_(function), arg_(arg) {}
        virtual void run() {function_(arg_);}
        T function_;
        S arg_;
    };

    template <typename T>
    class ThreadObjectFunc : public Runnable {
	  public:
        ThreadObjectFunc(void (T::*function)(), T* object) : function_(function), object_(object) {}
        virtual void run() {(object_->*function_)();}
        void (T::*function_)();
        T* object_;
    };

	template <typename T, typename S>
    class ThreadObjectFuncWithArg : public Runnable {
	  public:
		ThreadObjectFuncWithArg(void (T::*function)(), T* object, S arg) : function_(function), object_(object), arg_(arg) {}
        virtual void run() {(object_->*function_)(arg_);}
        void (T::*function_)();
        T* object_;
		S arg_;
    };
}


class Thread : public Runnable, private NonCopyable {
  public:
    /** Templated function constructor
     * @param function the pointer to function
     */
    template <typename T> Thread(T function) : impl_(null), func_(new priv::ThreadFunctor<T>(function)) {

    }

    /** Templated function constructor with arguments
     * @param function the pointer to function
     * @param argument the pointer to arguments
     */
    template <typename T, typename S> Thread(T function, S argument) :
    impl_(null), func_(new priv::ThreadFunctorWithArg<T, S>(function, argument)) {

    }

    /** Templated object constructor
     * @param function the pointer to object function
     * @param object the pointer to object
     */
    template <typename T> Thread(void (T::*function)(), T* object) :
    impl_(null), func_(new priv::ThreadObjectFunc<T>(function, object)) {

    }

	/** Templated object constructor with arguments
     * @param function the pointer to object function
	 * @param object the pointer to object
     * @param argument the pointer to arguments
	 */
    template <typename T, typename S> Thread(void (T::*function)(), T* object, S argument) :
    impl_(null), func_(new priv::ThreadObjectFuncWithArg<T, S>(function, object, argument)) {

    }

	/** Destructor
     */
    ~Thread();

    /** Starts the thread
     */
    void launch();

    /** Waits (blocking) until thread is finished
     */
    void wait();

    /** Release the thread
     */
    void terminate();

  protected:
    friend class priv::ThreadImpl;

    /** Internal entry point
     */
    virtual void run();

    // Member data
    priv::ThreadImpl*	impl_;
	Runnable*			func_;

	explicit_noncopyable(Thread);
};

#endif // THREAD_HPP
