/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef CLIENT_HPP
#define CLIENT_HPP

/**
 * Headers
 */
#include <Runnable.hpp>
#include <Thread.hpp>
#include <Lock.hpp>
#include <Protocol.hpp>
#include <Console.hpp>
#include <MatrixManager.hpp>

class Client : public Runnable, ProtocolHandler {
  public:
	Client(const Address address, uint16 port);

	virtual ~Client();

	virtual void run();

	virtual void incoming_data(Packet& packet);

	virtual void outgoing_data(Packet& packet);

  protected:
  	void incoming_request(Packet& packet);

	void incoming_result(Packet& packet);

	void outgoing_request(Packet& packet);

	void outgoing_result(Packet& packet);

	void incoming_thread();

	// void outgoing_thread();

	bool				available_;
	Address				address_;
	uint16				port_;
	Socket				socket_;
	Protocol			protocol_;
	Thread*				incoming_thread_;
	// Thread*				outgoing_thread_;
	Console				console_;
	MatrixfManager		matrix_manager_; // float32 by default
};

#endif // CLIENT_HPP
