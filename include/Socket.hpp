/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef SOCKET_HPP
#define SOCKET_HPP

/**
 * Headers
 */
#include <Types.hpp>
#include <NonCopyable.hpp>
#include <Address.hpp>
#include <Buffer.hpp>

#if defined(PLATFORM_FAMILY_WINDOWS)
#   include <basetsd.h>
#endif

/** Socket
 *
 * A basic cross platform interface to handle sockets.
 *
 * For listening:
 *  listener.listen(port)
 *  listener.accept(client, &address)
 *  sock.disconnect()
 *
 * For connection:
 *  sock.connect(address, port)
 *  sock.receive(buffer, sizeof(buffer), received)
 *  sock.send(buffer, sizeof(buffer))
 *  sock.disconnect()
 *
 * @see SocketImpl
 *
 */

class Socket : private NonCopyable {
  public:

    /**
     * Define socket status
     */
    enum Status {
        STATUS_DONE,
        STATUS_NOTREADY,
        STATUS_DISCONNECTED,
        STATUS_ERROR
    };

    /**
     * Define invalid port type
     */
    enum {
        INVALID_PORT = 0
    };

    /**
     * Define default buffer size
     */
     enum {
        DEFAULT_BUFFER_SIZE = 1024
     };

    /**
     * Define portable socket handle
     */
    #if defined(PLATFORM_FAMILY_WINDOWS)
        typedef UINT_PTR Handle;
    #else
        typedef int      Handle;
    #endif

    /**
     * Socket constructor
     */
    Socket();

    /**
     * Virtual socket destructor
     */
    virtual ~Socket();

    /**
     * Data get & set
     */

    Handle get_handle() const;

    void set_blocking(bool blocking);

    bool get_blocking() const;

    uint16 get_port_local() const;

    uint16 get_port_remote() const;

    Address get_address_remote() const;

    /**
     * Methods
     */

    Status connect(const Address& remote_addr, uint16 remote_port = INVALID_PORT, uint32 timeout = 0);

    void disconnect();

    Status send(const void* buffer, uint32 size);

	Status receive(Buffer& buffer);

    Status receive(void* buffer, uint32 size, uint32& received);

    Status listen(uint16 port = INVALID_PORT);

    Status accept(Socket& socket);

  protected:

    void create();

    void create(Handle handle);

    void close();

  private:
    Handle  handle_;
    bool    blocking_;
};

#endif // SOCKET_HPP
