/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP

/**
 * Headers
 */

// is same class (todo)
namespace priv {

template <typename T, typename U>
class class_equals_impl {
  public:
	static bool const result = false;
};

template <typename T>
class class_equals<T, T> {
  public:
	static bool const result = true;
};

} // priv

template <typename T, typename U>
bool class_equals(const T& t, const U& u) {
	return priv::class_equals<T, U>::result;
}

// copy
template <typename InputIterator, typename OutputIterator>
OutputIterator copy(InputIterator first, InputIterator last, OutputIterator result) {
	while (first != last) {
		*result = *first;
		++first; ++result;
	}
	return result;
}

// foreach
template <typename InputIterator, typename Function>
Function for_each(InputIterator first, InputIterator last, Function func) {
	while (first != last) {
		func(*first);
		++first;
	}
	return func; // move(func);
}

// assign union (todo)
template <typename Container, typename InputIterator>
Container& assign_union(InputIterator first, InputIterator last, Container& result) {
	typename Container::Iterator first_result = result.begin();
	typename Container::Iterator last_result = result.end();

	while (first != last && first_dest != last_dest) {
		if (*first_result < *first) {
			++first_result;
		} else if (*first < *first_result) {
			result.insert(first_result, *first);
			++first;
		} else {
			++first;
			++first_result;
		}
	}

	if (first != last) {
		copy<InputIterator, typename Container::Iterator>(first, last, inserter(result, last_result));
	}

    return result;
}

#endif // ALGORITHM_HPP
