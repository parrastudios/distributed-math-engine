/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef IMUTEX_HPP
#define IMUTEX_HPP

/**
 * Headers
 */
#include <NonCopyable.hpp>

/** IMutex
 *
 * An interface to implement mutex policies
 *
 */

class IMutex : private NonCopyable {
  public:

    /** Lock the mutex
     */
    virtual void lock() = 0;

    /** Try to lock the mutex
	 * @return result of locking operation
     */
	virtual bool trylock() = 0;

    /** Unlock the mutex
     */
    virtual void unlock() = 0;

  protected:

    /** Default inline constructor
     */
    IMutex() {
	
	}

    /** Pure virtual desstructor
     */
	virtual ~IMutex() = 0 {
	
	}

    /** Explicit noncopyable pattern declaration
     */
	explicit_noncopyable(IMutex);
};

/** IMutexRecursive
 *
 * An interface to implement recursive mutex policies
 *
 */
class IMutexRecursive : public IMutex {
  public:
	
};

#endif // IMUTEX_HPP
