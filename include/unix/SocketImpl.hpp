/////////////////////////////////////////////////////////////////////////////
//  Distribuited Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef SOCKETIMPL_HPP
#define SOCKETIMPL_HPP

#include <Socket.hpp>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

namespace priv {

class SocketImpl {
  public:
    typedef socklen_t addr_size_t;

    static sockaddr_in create_addr(uint32 address, uint16 port);

    static Socket::Handle invalid();

    static void close(Socket::Handle handle);

    static void set_blocking(Socket::Handle handle, bool block);

    static Socket::Status get_error();
};

} // priv

#endif // SOCKETIMPL_HPP
