/////////////////////////////////////////////////////////////////////////////
//  Distribuited Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef THREADIMPL_HPP
#define THREADIMPL_HPP

#include <NonCopyable.hpp>
#include <pthread.h>

class Thread;

namespace priv {

class ThreadImpl : private NonCopyable {
  public:
    ThreadImpl(Thread* owner);

    void wait();

    void terminate();

  private:
    static void* function(void* data);

    pthread_t thread_;
    bool	  active_;
};

}

#endif // THREADIMPL_HPP
