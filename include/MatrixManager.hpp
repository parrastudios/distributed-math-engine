/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef MATRIXMANAGER_HPP
#define MATRIXMANAGER_HPP

/**
 * Headers
 */
#include <Vector.hpp>
#include <Matrix.hpp>
#include <ResourceManager.hpp>
#include <Packet.hpp>

template <typename T>
class MatrixManager;

template <typename T>
class MatrixBlock : public Resource {
  public:
	MatrixBlock(uint32 id, String& name);

	MatrixBlock(uint32 id, String& name, uint32 size);

	MatrixBlock(uint32 id, String& name, uint32 size, Matrix<T>* data);

	virtual ~MatrixBlock();

	void destroy();

	void copy(MatrixBlock<T>& matrix_block);

	virtual void create(uint32 count, uint32 size, enum Matrix<T>::Type type);

	Matrix<T>* get_data();

	uint32 get_size();

	void to_packet(Packet& packet);

	void from_packet(Packet& packet);

  protected:
	uint32		size_;
	Matrix<T>*	data_;
};

template <typename T>
class MatrixManager : public ResourceManager< MatrixBlock<T> > {
  public:
    using ResourceManager< MatrixBlock<T> >::add;
    using ResourceManager< MatrixBlock<T> >::remove;
    using ResourceManager< MatrixBlock<T> >::get_element;
    using ResourceManager< MatrixBlock<T> >::resource_list_;

	MatrixManager();

	virtual ~MatrixManager();

	uint32 create(String& name, uint32 count, uint32 size, enum Matrix<T>::Type type = Matrix<T>::TYPE_ZERO);

	uint32 create(Packet& packet);

	Matrix<T>* get(uint32 id);

	Vector<uint32> slice(uint32 id, uint32 size);

  protected:
	Vector<uint32> handles_;
};

#include <MatrixManager.inl>

typedef MatrixBlock<float64>	MatrixdBlock;
typedef MatrixBlock<float32>	MatrixfBlock;
typedef MatrixBlock<int32>		MatrixiBlock;
typedef MatrixBlock<uint32>		MatrixuiBlock;

typedef MatrixManager<float64>	MatrixdManager;
typedef MatrixManager<float32>	MatrixfManager;
typedef MatrixManager<int32>	MatrixiManager;
typedef MatrixManager<uint32>	MatrixuiManager;

#endif // MATRIXMANAGER_HPP
