/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef CONDITION_HPP
#define CONDITION_HPP

/**
 * Headers
 */
#include <Platform.hpp>

#if defined(PLATFORM_FAMILY_WINDOWS)
#   include <Win32/ConditionImpl.hpp>
#elif defined(PLATFORM_FAMILY_UNIX)
#   include <Unix/ConditionImpl.hpp>
#endif

/**
 * Sugar for cross-platform condition
 */
typedef priv::ConditionImpl Condition;

#endif // CONDITION_HPP
