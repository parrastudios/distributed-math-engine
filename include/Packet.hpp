/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef PACKET_HPP
#define PACKET_HPP

#include <Types.hpp>
#include <Buffer.hpp>

class Packet {
  public:
    Packet();

    Packet(uint8 type, uint8 code, uint32 size = 0, const void* data = null);

    Packet(const Packet& packet);

    Packet& operator=(const Packet& packet);

    virtual ~Packet();

	uint8 get_type();

	uint8 get_code();

    void* get_data();

    uint32 get_size();

    void set_type(uint8 type);

    void set_code(uint8 code);

    void set(const void* data);

    void clear();

	void append(Buffer& buffer);

    /** Adds data to the packet to sending it (overwrites the data if it already exists)
     * @param data the input data
     * @param size the size of the input data
     */
    void append(const void* data, uint32 size);

    /** Parses properly the packet for using it
     * @param buffer the socket input buffer
     * @param size the size of the buffer
     * @returns size read
     */
    uint32 parse(void* buffer, uint32 size);

	void buffer_pack(Buffer& buffer);

	void buffer_unpack(Buffer& buffer);

  protected:
    void set_data(const uint8* data, uint32 offset = 0);

    // Allow mutable packet sizes
    enum {
        HEADER_PREAMBLE_UINT8  = 0x73,
        HEADER_MAX_SIZE_UINT8  = 0xFBUL,		//< range_of_uint8 - (preamble + type + code + size + checksum)
												//      255        -           (sizeof(uint8) * 5)
        HEADER_PREAMBLE_UINT16 = 0x74,
        HEADER_MAX_SIZE_UINT16 = 0xFFF9UL,		// range_of_uint16 -  (sizeof(uint8) * 3 + sizeof(uint16) * 2)

        HEADER_PREAMBLE_UINT32 = 0x75,
        HEADER_MAX_SIZE_UINT32 = 0xFFFFFFF5UL	// range_of_uint32 -  (sizeof(uint8) * 3 + sizeof(uint32) * 2)
    };

    uint8  preamble_;
    uint8  type_;
    uint8  code_;
    uint32 size_; //< packet data size

    uint8* data_;
    uint32 read_pos_;
    bool   valid_;

  private:
    void create();

    void preamble_update();

    template <typename T>
    uint32 get_full_size();

    template <typename T>
    void write_size_buffer(Buffer& buffer, uint32& offset);

    enum {
        PARSER_WAITING_PREAMBLE = 0x00000000,
        PARSER_WAITING_TYPE     = 0x00000001,
        PARSER_WAITING_CODE     = 0x00000002,
        PARSER_WAITING_LENGTH   = 0x00000003,
        PARSER_WAITING_CHECKSUM = 0x00000004,
        PARSER_WAITING_DATA     = 0x00000005
    };

    uint32 parser_status_;
};

#endif // PACKET_HPP
