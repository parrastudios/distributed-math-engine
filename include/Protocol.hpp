/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef PROTOCOL_HPP
#define PROTOCOL_HPP

#include <Packet.hpp>
#include <Socket.hpp>

/**
 * Network types
 */
enum {
	PROTOCOL_TYPE_MATRIX		= 0x00,
	PROTOCOL_TYPE_NUMERICAL		= 0x01,
	PROTOCOL_TYPE_CRYPTO		= 0x02,
	PROTOCOL_TYPE_METEO			= 0x03,
	PROTOCOL_TYPE_MOLECULAR		= 0x04,

	PROTOCOL_TYPE_ERROR			= 0xFFFFFFFF
};

/**
 * Network matrix codes
 */
enum {
	PROTOCOL_CODE_INVERSE		= 0x00,
	PROTOCOL_CODE_TRIANGULATEUP	= 0x01
};

/**
 * Network numerical codes
 */
enum {
	PROTOCOL_CODE_PI			= 0x00,
	PROTOCOL_CODE_PRIME			= 0x01
};

/**
 * Network cryptographic codes
 */
enum {
	PROTOCOL_CODE_BRUTEFORCE	= 0x00
};

/**
 * Network meteorology codes
 */
enum {
	PROTOCOL_CODE_CLIMATE		= 0x00,
	PROTOCOL_CODE_HURRICANE		= 0x01
};

/**
 * Network molecular codes
 */
enum {
	PROTOCOL_CODE_PROTEIN		= 0x00
};

/** ProtocolHandler
 *
 * A basic interface to implement an incoming/outgoing
 * packet handler
 */
class ProtocolHandler {
  public:
	virtual ~ProtocolHandler() {}

	virtual void incoming_data(Packet& packet) = 0;

	virtual void outgoing_data(Packet& packet) = 0;
};

/** Protocol
 *
 * A basic class that deals with socket calls and raises events
 * to the protocol handler
 */
class Protocol {
  public:
    Protocol(Socket& socket);

    virtual ~Protocol();

	virtual void handle_incoming_data(ProtocolHandler& handler);

	virtual void handle_outgoing_data(ProtocolHandler& handler, uint8 type, uint8 code);

  protected:
    Socket&	socket_;
};

#endif // PROTOCOL_HPP
