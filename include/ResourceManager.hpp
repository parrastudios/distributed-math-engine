/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef RESOURCEMANAGER_HPP
#define RESOURCEMANAGER_HPP

#include <Resource.hpp>
#include <NonCopyable.hpp>
#include <List.hpp>
#include <Stack.hpp>

template <typename T>
class ResourceManager : private NonCopyable {
  public:
	static const uint32 INVALID_ID;

	ResourceManager();

	virtual ~ResourceManager();

	List<T*>* get_list();

	T* get_element();

	T* get_element(uint32 id);

	T* get_element(String& name);

	void clear();

	void remove(uint32 id);

	uint32 add(T* element);

	uint32 add(String& name);

	T* operator[](uint32 id);

  protected:
    Stack<uint32>   ids_stack_;
    List<T*>*       resource_list_;

    explicit_noncopyable(ResourceManager);
};

#include <ResourceManager.inl>

#endif // RESOURCEMANAGER_HPP
