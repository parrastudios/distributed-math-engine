/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef BUFFER_HPP
#define BUFFER_HPP

/**
 * Headers
 */
#include <Types.hpp>

class Buffer {
  public:
    Buffer();

    explicit Buffer(uint32 size);

    Buffer(const Buffer& buffer);

    virtual ~Buffer();

    Buffer& operator=(const Buffer& buffer);

    void set_data(void* data, uint32 size, uint32 offset);

    uint8* get_data();

    uint32 get_size();

    void create(uint32 size);

    void clear();
  protected:
    uint8*  data_;
    uint32  size_;
};

#endif // BUFFER_HPP
