/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#if !defined STRING_HPP || defined STRING_INL
#	error String.hpp included directly
#endif

#define STRING_INL

template <typename T>
const T StringBase<T>::null_char = T('\0');

template <typename T>
StringBase<T>::StringBase() : Vector<T>() {
	
}

template <typename T>
StringBase<T>::StringBase(const T* string) : Vector<T>() {
	copy(string);
}

template <typename T>
StringBase<T>::StringBase(const StringBase<T>& string) : Vector<T>(string) {
	
}

template <typename T>
StringBase<T>::~StringBase() {
	
}

template <typename T>
StringBase<T> StringBase<T>::substr(uint32 length, uint32 offset) {
	// todo
	return StringBase<T>();
}

template <typename T>
bool StringBase<T>::readfield(StringBase<T>& result, T delimiter, uint32 position) const {
	uint32 del_count = 0;
	uint32 i, j = 0;
	bool reading = false;

	// if the position is at beginning, start reading
	if (position == 0) {
		reading = true;
	}

	// iterate through source string
	for (i = 0; i < size_; ++i) {
		// delimiter found
		if (data_[i] == delimiter) {
			++del_count;

			if (del_count == position) {
				// position found, read
				reading = true;

				// skip delimiter;
				++i;
			}

			if (del_count == (position + 1)) {
				// put nullchar at the end of string
				result.push_back(null_char);

				// parsing finished, return
				return true;
			}
		}

		// if reading, copy the word
		if (reading == true) {
			// copy word
			result.push_back(data_[i]);
		}
	}

	// found but it is the last one
	if (result.size() > 0) {
		// put nullchar at the end of string
		result.push_back(null_char);
		return true;
	}
	
	// not found
	return false;
}

template <typename T>
StringBase<T> StringBase<T>::readfield(T delimiter, uint32 position) const {
	StringBase<T> result;

	if (readfield(result, delimiter, position)) {
		return result;
	}

	return StringBase<T>();
}

template <typename T>
Vector<StringBase<T>>& StringBase<T>::split(Vector<StringBase<T>>& vector, T delimiter) const {
	uint32 del_count = 0;

	for (uint32 i = 0; i < size_; ++i) {
		
		if (data_[i] == delimiter) {
			vector[del_count].push_back(null_char);
			++i; // skip delimiter
			++del_count;
		} else if (data_[i] == null_char) {
			vector[del_count].push_back(data_[i]);
			return vector;
		}

		vector[del_count].push_back(data_[i]);
	}

	return vector;
}

template <typename T>
Vector<StringBase<T>> StringBase<T>::split(T delimiter) const {
	Vector<StringBase<T>> vector;

	split(vector, delimiter);

	return vector;
}

template <typename T>
void StringBase<T>::copy(const T* string) {
	if (string != data_) {
		uint32 count;

		for (count = 0; string[count] != null_char; ++count);

		size_ = capacity_ = count;
		data_ = new T[capacity_];
				
		for (count = 0; count < size_; ++count) {
			data_[count] = string[count];
		}
	}
}

template <typename T>
StringBase<T>& StringBase<T>::operator=(const T* string) {
	// prevent self-assignement
	if (string != data_) {
		// clear fist current string
		clear();

		// copy the string
		copy(string);
	}

	return *this;
}

template <typename T>
StringBase<T> StringBase<T>::operator+(const StringBase<T>& string) {
	StringBase<T> result(*this);

	// append to the copy
	result.append(string);

	return result;
}

template <typename T>
StringBase<T>& StringBase<T>::operator+=(const StringBase<T>& string) {
	// append it to this string
	append(string);

	return *this;
}

template <typename T>
T* StringBase<T>::operator*() {
	return data_;
}

template <typename T>
const T* StringBase<T>::operator*() const {
	return data_;
}

template <typename T>
bool StringBase<T>::operator==(const StringBase<T>& string) {
	if (&string == this) {
		return true;
	}

	if (size_ == string.size_) {
		Iterator self_it = data_;
		Iterator string_it = string.data_;

		while (*self_it != null_char && *string_it != null_char) {
			if (*self_it != *string_it) {
				return false;
			}

			self_it++; string_it++;
		}

		return true;
	}

	return false;
}

template <typename T>
bool StringBase<T>::operator==(const T* string) {
	if (string == data_) {
		return true;
	} else {
		uint32 count = 0;

		while (data_[count] != null_char && string[count] != null_char) {
			if (data_[count] != string[count]) {
				return false;
			}

			++count;
		}
		
		return (data_[count] == string[count]);
	}
}

template <typename T>
bool StringBase<T>::operator!=(const StringBase<T>& string) {
	return !(*this == string);
}

template <typename T>
bool StringBase<T>::operator!=(const T* string) {
	return !(*this == string);
}

template <typename T>
bool StringBase<T>::operator<(const StringBase<T>& string) {
	Iterator self_it = data_;
	Iterator string_it = string.data_;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it < *string_it) {
			return true;
		} else if (*self_it > *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return (size_ < string.size_);
}

template <typename T>
bool StringBase<T>::operator<(const T* string) {
	Iterator self_it = data_;
	Iterator string_it = string;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it < *string_it) {
			return true;
		} else if (*self_it > *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return false; // equal
}

template <typename T>
bool StringBase<T>::operator>(const StringBase<T>& string) {
	Iterator self_it = data_;
	Iterator string_it = string.data_;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it > *string_it) {
			return true;
		} else if (*self_it < *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return (size_ > string.size_);
}

template <typename T>
bool StringBase<T>::operator>(const T* string) {
	Iterator self_it = data_;
	Iterator string_it = string;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it > *string_it) {
			return true;
		} else if (*self_it < *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return false; // equal
}

template <typename T>
bool StringBase<T>::operator<=(const StringBase<T>& string) {
	Iterator self_it = data_;
	Iterator string_it = string.data_;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it < *string_it) {
			return true;
		} else if (*self_it > *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return (size_ <= string.size_);
}

template <typename T>
bool StringBase<T>::operator<=(const T* string) {
	Iterator self_it = data_;
	Iterator string_it = string;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it < *string_it) {
			return true;
		} else if (*self_it > *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return true;
}

template <typename T>
bool StringBase<T>::operator>=(const StringBase<T>& string) {
	Iterator self_it = data_;
	Iterator string_it = string.data_;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it > *string_it) {
			return true;
		} else if (*self_it < *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return (size_ >= string.size_);
}
template <typename T>
bool StringBase<T>::operator>=(const T* string) {
	Iterator self_it = data_;
	Iterator string_it = string;

	while (*self_it != null_char && *string_it != null_char) {
		if (*self_it > *string_it) {
			return true;
		} else if (*self_it < *string_it) {
			return false;
		}

		self_it++; string_it++;
	}

	return true;
}

template <typename T>
std::ostream& operator<<(std::ostream& stream, const StringBase<T>& string) {
	for (uint32 i = 0; i < string.size_; ++i) {
		stream << string.data_[i];
	}

	return stream;
}

template <typename T>
std::istream& operator>>(std::istream& stream, const StringBase<T>& string) {
	for (uint32 i = 0; i < string.size_; ++i) {
		stream >> string.data_[i];
	}

	return stream;
}
