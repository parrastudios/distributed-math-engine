/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef ADDRESS_HPP
#define ADDRESS_HPP

/**
 * Headers
 */
#include <Types.hpp>
#include <String.hpp>

/** Address
 *
 * IPv4 - IPv6 network address handler
 *
 */

class Address {
  public:
    static const Address ADDR_NONE;
    static const Address ADDR_LOCALHOST;
    static const Address ADDR_BROADCAST;

    Address();

    Address(const String& address);

    Address(const char* address);

    Address(uint8 byte0, uint8 byte1, uint8 byte2, uint8 byte3);

    explicit Address(uint32 address);

	Address(const Address& addresss);

    String to_string() const;

    uint32 to_uint32() const;

    static Address get_address_local();

  protected:
    uint32 address_;
};

bool operator==(const Address& left, const Address& right);

bool operator!=(const Address& left, const Address& right);

#endif // ADDRESS_HPP
