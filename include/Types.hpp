/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef TYPES_HPP
#define TYPES_HPP

/**
 * Headers
 */
#include <Debug.hpp>
#include <Platform.hpp>

/**
 * Standard C++ library includes
 */
#include <climits>
#include <cfloat>

/**
 * Null definition
 */
#ifndef null
#	ifdef __cplusplus
#		define null 0L
#	else
#		define null (void*)0
#	endif
#endif

/**
 * Null type class definition
 */
namespace priv {
	class NullType {};
} // priv

typedef priv::NullType null_type;	//< Syntactic sugar for null type class

/**
 * Constness type definitions
 */
template <typename T>
struct NonPropagateConst {
	typedef T type;
};

template <typename T>
struct PropagateConst {
	typedef const T type;
};

/**
 * Empty type definitions
 */
class EmptyType {};

inline bool operator==(const EmptyType&, const EmptyType&) {
	return true;
}   

inline bool operator<(const EmptyType&, const EmptyType&) {
	return false;
}

inline bool operator>(const EmptyType&, const EmptyType&) {
	return false;
}

/**
 * Define portable fixed-size types
 */

/**
 * 8 bits integer types
 */
#if UCHAR_MAX == 0xFF
    typedef signed   char int8;
    typedef unsigned char uint8;
#else
#	error No 8 bits integer type for this platform
#endif

static_assert(sizeof(int8) == 1);

/**
 * 16 bits integer types
 */
#if USHRT_MAX == 0xFFFF
    typedef signed   short int16;
    typedef unsigned short uint16;
#elif UINT_MAX == 0xFFFF
    typedef signed   int int16;
    typedef unsigned int uint16;
#elif ULONG_MAX == 0xFFFF
    typedef signed   long int16;
    typedef unsigned long uint16;
#else
#	error No 16 bits integer type for this platform
#endif

static_assert(sizeof(int16) == 2);

/**
 * 32 bits integer types
 */
#ifdef __palmos__
  typedef signed   long int32;
  typedef unsigned long uint32;
#else
#	if USHRT_MAX == 0xFFFFFFFF
		typedef signed   short int32;
		typedef unsigned short uint32;
#	elif UINT_MAX == 0xFFFFFFFF
		typedef signed   int int32;
		typedef unsigned int uint32;
#	elif ULONG_MAX == 0xFFFFFFFF
		typedef signed   long int32;
		typedef unsigned long uint32;
#	else
#		error No 32 bits integer type for this platform
#	endif
#endif

static_assert(sizeof(int32) == 4);

/**
 * 64 bits integer types
 */
#if COMPILER_TYPE == COMPILER_MSVC || COMPILER_TYPE == COMPILER_WATCOM || COMPILER_TYPE == COMPILER_BORLAND
	typedef signed   __int64 int64;
	typedef unsigned __int64 uint64;
#elif defined(__LP64__) || defined(__powerpc64__) || defined(__sparc__)
	typedef signed	 long int64;
	typedef unsigned long uint64;
#else
	typedef signed	 long long int64;
	typedef unsigned long long uint64;
#endif

static_assert(sizeof(int64) == 8);

#ifndef float32
    typedef float float32;
#endif
#ifndef float64
    typedef double float64;
#endif

#endif // TYPES_HPP
