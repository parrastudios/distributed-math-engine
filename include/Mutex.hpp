/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef MUTEX_HPP
#define MUTEX_HPP

// todo: Implement RecursiveMutex, Benaphore, RecursiveBenaphore, Futex

/**
 * Headers
 */
#include <Platform.hpp>

#if defined(PLATFORM_FAMILY_WINDOWS)
#   include <Win32/MutexImpl.hpp>
//#	include <Win32/Benaphore.hpp>
#elif defined(PLATFORM_FAMILY_UNIX)
#   include <Unix/MutexImpl.hpp>
#	include <Unix/Futex.hpp>
#endif

/**
 * Define spins before yielding, 200 (Linux) - 4000 (Windows)
 */
#if defined(PLATFORM_FAMILY_WINDOWS)
#	define MUTEX_ADAPTIVE_SPINCOUNT	4000
#elif defined(PLATFORM_FAMILY_UNIX)
#	define MUTEX_ADAPTIVE_SPINCOUNT	200
#endif

/**
 * Sugar for cross-platform mutex
 */
typedef priv::MutexImpl Mutex;

/**
 * Sugar for cross-platform recursive mutex
 */
typedef priv::MutexRecursiveImpl MutexRecursive;

/** MutexSingleThreaded
 *
 * Single threaded policy for mutex (null mutex)
 *
 */

class MutexSingleThreaded : public IMutex {
  public:
    MutexSingleThreaded();

    ~MutexSingleThreaded();

    virtual void lock();

	virtual bool trylock();

    virtual void unlock();

  protected:
	bool locked_;

	explicit_noncopyable(MutexSingleThreaded);
};

#endif // MUTEX_HPP
