/////////////////////////////////////////////////////////////////////////////
//  Distributed Math Engine
//  A new approach to improve the efficiency in mathematical calculus through
//  its distribution on network.
//  Copyright (C) 2012-2013 Vicente Ferrer Garcia (parra_real_93@hotmail.com)
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Affero General Public License as
//  published by the Free Software Foundation, either version 3 of the
//  License, or (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Affero General Public License for more details.
//
//  You should have received a copy of the GNU Affero General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>
/////////////////////////////////////////////////////////////////////////////

#ifndef TRACKPOINT_HPP
#define TRACKPOINT_HPP

/**
 * Headers
 */
#include <Types.hpp>
#include <iostream> // Stream.hpp

/** TrackPoint
 *
 * A class useful for tracking down where some event occurs
 *
 */

class TrackPoint {
  public:
    /** Default constructor
     */
    TrackPoint();

	/** Constructor
	 * @param file file string
	 * @param line line number
     */
	TrackPoint(const char* file, uint32 line);

    /** Copy constructor
     */
    TrackPoint(const TrackPoint& track_point);

    /** Destructor
     */
    virtual ~TrackPoint();

    /** The current file
     * @return the internal file string
     */
	const char* get_file() const;

    /** The current line
     * @return the line number in the file
     */
	uint32 get_line() const;

    /** Marks the specified file and line
	 * @param file file string
	 * @param line line number
	 * @return a reference to itself
     */
	const TrackPoint& mark_impl(const char* file, uint32 line);

    /** Check if the track point has been marked
	 * @return true if it is marked, false otherwise
     */
	bool marked() const;

    /** Overloaded assignment opearator
     * @param track_point the track point to be copied
     * @return the reference to itself
     */
    TrackPoint& operator=(const TrackPoint& track_point);

  protected:
    /** Wrapper for ostream opearator
     * @param stream the output stream
     * @return the reference to ostream object
     */
	std::ostream& print(std::ostream& stream) const;

    /** Overloaded ostream opearator
     * @param stream the output stream
     * @param track_point the track point to be streamed
     * @return the reference to ostream object
     */
    friend std::ostream& operator<<(std::ostream& stream, const TrackPoint& track_point);

	/** Member data
	 */
	const char* file_;	//< file register
	uint32		line_;	//< line register
};

/** Syntactic sugar for mark function
 */
#define mark() mark_impl(__FILE__, __LINE__)

/** Syntactic sugar for templated track point construction
 */
template <const char* message, uint32 line>
TrackPoint TrackPointWrapper() {
	return TrackPoint(message, line);

}

#endif // TRACKPOINT_HPP