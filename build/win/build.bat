SETLOCAL ENABLEEXTENSIONS
SETLOCAL ENABLEDELAYEDEXPANSION
REM COLOR 0A
TITLE Distributed Math Engine Win32 Builder

:START

REM define local variables
SET "current_path = %CD%"

REM go to project path and set all paths
cd ..\..
SET "project_path = %CD%"
REM include_path = %project_path%"include\"
REM  source_path = %project_path%"source\"
REM  output_file = %project_path%"build\bin\dme.exe"
 
echo %current_path% 
echo %project_path%
REM echo %include_path% 
REM echo %source_path% 
REM echo %output_file%


REM check compilation mode
REM if "%2" == "debug" (
REM 	set compile_mode=debug
REM ) else if "%2" == "release" (
REM 	set compile_mode=release
REM ) else (
REM 	goto argerr
REM )

REM check compiler type
REM if "%1" == "mingw" (
REM 	if "%compile_mode%" == "debug" (
REM	%mingw_path%g++ -o %output_file% -I ../include MemoryBlock.cpp MemoryChunk.cpp MemoryPool.cpp main.cpp
	REM %mingw_path%g++ -o %output_file% -I ../include Exception.cpp VideoMode.cpp ./win32/VideoModeImpl.cpp ./win32/ClockImpl.cpp Clock.cpp main.cpp
	REM del %project_path%*.o
	REM -o %output_path%aoc.exe
	REM Observable.cpp Observer.cpp VideoMode.cpp ./win32/VideoModeImpl.hpp Resource.cpp Lock.cpp Window.cpp
REM 	) else (
REM 		echo not implemented
REM 	)
 	goto end
REM ) else if "%1" == "msvc08" (
REM 	echo msvc08: not implemented yet
REM 	goto end
REM ) else if "%1" == "msvc10" (
REM 	echo msvc10: not implemented yet
REM 	goto end
REM ) else if "%1" == "help" (
REM 	echo build compiles all sources from aoc project
REM 	echo syntax		build [compiler] [mode]
REM 	echo compiler	compiler type [mingw msvc08 msvc10]
REM 	echo mode		compilation mode [debug release]
REM 	goto end
REM ) else (
REM 	goto argerr
REM )

:argerr
echo Invalid command syntax. Type build help for more info.
goto end

:end
endlocal
REM pause
REM exit
