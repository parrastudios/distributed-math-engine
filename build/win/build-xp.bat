@echo off
setlocal ENABLEEXTENSIONS
setlocal ENABLEDELAYEDEXPANSION
title	 Distributed Math Engine Win32 Builder

:start

rem set default compiler paths
set   mingw_path=C:\Archivos de programa\CodeBlocks\MinGW\bin\
set  msvc08_path=C:\Archivos de programa\Visual Studio\VC\bin\
set  msvc10_path=C:\Archivos de programa\Visual Studio 2010\VC\bin\

rem set project paths
set current_path=%cd%

cd ..\..

set project_path=%cd%
set include_path=%project_path%\include
set  source_path=%project_path%\src
set  output_file=%project_path%\build\bin\dme.exe

rem ---------------------------------------------------
set mypath=
call :treeProcess
goto :eof

:treeProcess
setlocal
for %%f in (*.cpp) do echo %mypath%%%f
for /D %%d in (*) do (
    set mypath=%mypath%%%d\
    cd %%d
    call :treeProcess
    cd ..
)
endlocal
exit /b

goto end
rem ---------------------------------------------------

rem set build arguments
set compiler_type=%1
set compile_mode=%2

rem check compilation mode
if not "%compile_mode%"=="debug" (
	if not "%compile_mode%"=="release" (
		if not "%compiler_type%"=="help" (
			goto args_error
		) else (
			goto help_show
		)
	)
)

rem check compiler type
if "%compiler_type%"=="mingw" (
	goto compile_mingw
) else if "%compiler_type%"=="msvc08" (
	goto compile_msvc08
) else if "%compiler_type%"=="msvc10" (
	goto compile_msvc10
) else (
	goto args_error
)

rem compile with mingw
:compile_mingw
echo Compiling with MinGW
cd %mingw_path%
g++ -o %output_file% -I %include_path% %source_path%\main.cpp 
goto end

rem compile with msvc08
:compile_msvc08

goto end

rem compile with msvc10
:compile_msvc10

goto end

rem show help info
:help_show
echo build compiles all sources from Distributed Math Engine project
echo syntax		build [compiler] [mode]
echo  compiler	compiler type [mingw msvc08 msvc10]
echo  mode		compilation mode [debug release]
goto end

rem arguments error handler
:args_error
echo Invalid command syntax. Type build help for more info.
goto end

rem end of batch
:end
endlocal
