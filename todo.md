## __TODO LIST__
### __v.0.1.b__

* Extend Randomizer (normal, binomial, ...)
* __Exception System *(Parra: Implemented & Tested)*__
* __Sleep, SleepImpl *(Parra: Implemented & Tested)*__
* __Time, TimeImpl *(Parra: Implemented & Tested)*__
* Check / Test / Benchmark
* Barrier, AtomicImpl, Thread / ThreadPool, Condition, Monitor, SpinLock
* Allocator, MemoryManager
* Task, Scheduler
* Event, EventManager
* ADT:
	+ Map
	+ Set
	+ Tree
	+ Deque (circular vector)
	+ Heap (vector implementation)
	+ Priority Queue
* Patterns:
	+ __Callbacks *(Parra: Implemented & Tested)*__
	+ Observer
* Internet Statics: Geolocalization, Bandwith, Ping
* System & CPU Statics: Memory, CPU usage, ...
* Stream
* Node, SuperNode, RemoteNode, LoginNode, ProxyNode
	+ Ensure Data Consistency, Integrity & Control, Elasticity & Scalability
* String, Lexer, Parser
* String System, Plugins, Packages
* Clustering, k-means, Quality Treshold Cluster
* Inline Assembly Optimizations with CPU Extensions: SSE, SSE2, SSE3, ...
* Optimized Math Engine
* Complete System Monitoring via Web Server / RemoteNode
* Dynamic Abstract Protocol, Protocol Error Control, Protocol Data Recovery & Replication
* Pipelined Protocol, Data Compression (for slow connections)
* Packets: Concurrent Dijkstra, Concurrent Pi Decimal Calculator, ...
* Miscelaneous:
	+ Rewrite properly all templates in its inl files
	+ Generate a cross-platform build script for all project
	+ Write all tests
